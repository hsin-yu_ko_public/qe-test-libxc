
     Program CP v.6.7GPU starts on  6Jun2021 at  2:12:58 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
         "P. Giannozzi et al., J. Chem. Phys. 152 154105 (2020);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
     Message from routine scan_end:
     No INFO block end statement, possibly corrupted file
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Number of Electrons=     8, of States =     4
   Occupation numbers :
   2.00 2.00 2.00 2.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   internal stress tensor calculated
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249

     Using Pencil Decomposition


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

     Subspace diagonalization in iterative solution of the eigenvalue problem:
     a serial algorithm will be used


   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   nbnd =     4
    2.00 2.00 2.00 2.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)      134.62224      0

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.976566965109257    0.0    0.00      10.642034842665      10.642034842665      10.642034842665      11.618601807774   0.0000   0.0000   0.0000   0.0000
      2    2.768909244405163    0.0    0.00       8.091717363581       8.091717363581       8.091717363581      10.860626607987   0.0000   0.0000   0.0000   0.0000
      3    5.066052758181264    0.0    0.00       4.287968920118       4.287968920118       4.287968920118       9.354021678299   0.0000   0.0000   0.0000   0.0000
      4    7.567941515095341    0.0    0.00      -0.677739499461      -0.677739499461      -0.677739499461       6.890202015634   0.0000   0.0000   0.0000   0.0000
      5    8.977433068182705    0.0    0.00      -5.692814830149      -5.692814830149      -5.692814830149       3.284618238033   0.0000   0.0000   0.0000   0.0000
      6    7.554421251647222    0.0    0.00      -7.503536445804      -7.503536445804      -7.503536445804       0.050884805843   0.0000   0.0000   0.0000   0.0000
      7    4.658184310394582    0.0    0.00      -6.735444236504      -6.735444236504      -6.735444236504      -2.077259926110   0.0000   0.0000   0.0000   0.0000
      8    2.924499481938872    0.0    0.00      -6.447647790253      -6.447647790253      -6.447647790253      -3.523148308315   0.0000   0.0000   0.0000   0.0000
      9    2.653259524457532    0.0    0.00      -7.344624632927      -7.344624632927      -7.344624632927      -4.691365108470   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       37.19795     10
     Pressure of Nuclei (GPa)             0.00000     10
     Pressure Total (GPa)            37.19795     10


                total energy =       -8.82441279441 Hartree a.u.
              kinetic energy =        8.00419 Hartree a.u.
        electrostatic energy =      -15.96150 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.36136 Hartree a.u.
  n-l pseudopotential energy =        3.94856 Hartree a.u.
 exchange-correlation energy =       -2.45431 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -78.97  -17.43   -7.60   -1.26


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       49.55626897        -3.78803099        -1.13911100
       -3.78803099        42.25160520        -0.61837294
       -1.13911100        -0.61837294        19.78598277
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.79060086373037E+00     0.89196972743263E+00     0.21854172685068E+00
   H       0.96502993051912E+00    -0.18157631185234E+00     0.40982315498421E-02
   H      -0.23142632745509E+00     0.95623541655594E+00     0.28179857059205E+00



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.921745780306869    0.0    0.00      -8.824412794409      -8.824412794409      -8.824412794409      -5.902667014103   0.0000   0.0000   0.0000   0.0000
     11    2.951109137111763    0.0    0.00     -10.066213220536     -10.066213220536     -10.066213220536      -7.115104083424   0.0000   0.0000   0.0000   0.0000
     12    2.604414114663909    0.0    0.00     -10.804063602365     -10.804063602365     -10.804063602365      -8.199649487701   0.0000   0.0000   0.0000   0.0000
     13    2.166325509887246    0.0    0.00     -11.289389137992     -11.289389137992     -11.289389137992      -9.123063628105   0.0000   0.0000   0.0000   0.0000
     14    1.842508292997843    0.0    0.00     -11.757373183433     -11.757373183433     -11.757373183433      -9.914864890435   0.0000   0.0000   0.0000   0.0000
     15    1.637426583811155    0.0    0.00     -12.251073725782     -12.251073725782     -12.251073725782     -10.613647141971   0.0000   0.0000   0.0000   0.0000
     16    1.483825867030851    0.0    0.00     -12.725357997674     -12.725357997674     -12.725357997674     -11.241532130643   0.0000   0.0000   0.0000   0.0000
     17    1.340521529252746    0.0    0.00     -13.146690984385     -13.146690984385     -13.146690984385     -11.806169455132   0.0000   0.0000   0.0000   0.0000
     18    1.206185282668607    0.0    0.00     -13.519018913300     -13.519018913300     -13.519018913300     -12.312833630631   0.0000   0.0000   0.0000   0.0000
     19    1.092407502604325    0.0    0.00     -13.862686101102     -13.862686101102     -13.862686101102     -12.770278598498   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)        7.16359     20
     Pressure of Nuclei (GPa)             0.00000     20
     Pressure Total (GPa)             7.16359     20


                total energy =      -14.18999142011 Hartree a.u.
              kinetic energy =        9.87237 Hartree a.u.
        electrostatic energy =      -20.72792 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.07474 Hartree a.u.
  n-l pseudopotential energy =        3.13050 Hartree a.u.
 exchange-correlation energy =       -3.39020 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -43.03  -26.72   -8.54   -3.59


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       14.75987422       -12.77715284         1.93396136
      -12.77715284        10.11895381         3.45620540
        1.93396136         3.45620540        -3.38804509
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18597853645819E+00     0.17980839480207E+00     0.39778613434951E+00
   H       0.46628870771826E+00    -0.49226215907913E+00    -0.85162821792677E-01
   H      -0.27132083848555E+00     0.24632311985514E+00    -0.44014981170299E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.001999542670544    0.0    0.00     -14.189991420113     -14.189991420113     -14.189991420113     -13.187991877442   0.0000   0.0000   0.0000   0.0000
     21    0.927768452630518    0.0    0.00     -14.501492519225     -14.501492519225     -14.501492519225     -13.573724066595   0.0000   0.0000   0.0000   0.0000
     22    0.859467293871489    0.0    0.00     -14.790700735743     -14.790700735743     -14.790700735743     -13.931233441872   0.0000   0.0000   0.0000   0.0000
     23    0.789431278506857    0.0    0.00     -15.050457148755     -15.050457148755     -15.050457148755     -14.261025870248   0.0000   0.0000   0.0000   0.0000
     24    0.714663554494174    0.0    0.00     -15.276340359276     -15.276340359276     -15.276340359276     -14.561676804782   0.0000   0.0000   0.0000   0.0000
     25    0.635832221019282    0.0    0.00     -15.467183461199     -15.467183461199     -15.467183461199     -14.831351240180   0.0000   0.0000   0.0000   0.0000
     26    0.555535685142799    0.0    0.00     -15.624550602173     -15.624550602173     -15.624550602173     -15.069014917030   0.0000   0.0000   0.0000   0.0000
     27    0.477391393522829    0.0    0.00     -15.751953030699     -15.751953030699     -15.751953030699     -15.274561637176   0.0000   0.0000   0.0000   0.0000
     28    0.405934152411218    0.0    0.00     -15.856060576762     -15.856060576762     -15.856060576762     -15.450126424351   0.0000   0.0000   0.0000   0.0000
     29    0.344750138982155    0.0    0.00     -15.943843609456     -15.943843609456     -15.943843609456     -15.599093470474   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -0.34869     30
     Pressure of Nuclei (GPa)             0.00000     30
     Pressure Total (GPa)            -0.34869     30


                total energy =      -16.02199117345 Hartree a.u.
              kinetic energy =       12.11283 Hartree a.u.
        electrostatic energy =      -22.71589 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.65252 Hartree a.u.
  n-l pseudopotential energy =        2.12735 Hartree a.u.
 exchange-correlation energy =       -3.89376 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -32.47  -19.20  -12.84   -4.42


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
        5.55592360        -6.97875947        -0.90468288
       -6.97875947         1.24222767        -0.19481661
       -0.90468288        -0.19481661        -7.84421723
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.52262967661254E+00    -0.33971097966693E+00    -0.11747775574641E+00
   H       0.21328404162335E+00    -0.14714284479953E+00    -0.20435689688402E-01
   H      -0.10963352731274E+00     0.13679990819105E+00    -0.49499021078394E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.295909754380650    0.0    0.00     -16.021991173452     -16.021991173452     -16.021991173452     -15.726081419071   0.0000   0.0000   0.0000   0.0000
     31    0.260234515353002    0.0    0.00     -16.096627668559     -16.096627668559     -16.096627668559     -15.836393153206   0.0000   0.0000   0.0000   0.0000
     32    0.236837775910967    0.0    0.00     -16.171916043263     -16.171916043263     -16.171916043263     -15.935078267352   0.0000   0.0000   0.0000   0.0000
     33    0.223915705885948    0.0    0.00     -16.250685176699     -16.250685176699     -16.250685176699     -16.026769470813   0.0000   0.0000   0.0000   0.0000
     34    0.218871047211869    0.0    0.00     -16.334035249602     -16.334035249602     -16.334035249602     -16.115164202390   0.0000   0.0000   0.0000   0.0000
     35    0.218114100914605    0.0    0.00     -16.420827075242     -16.420827075242     -16.420827075242     -16.202712974328   0.0000   0.0000   0.0000   0.0000
     36    0.217959397707295    0.0    0.00     -16.508083599018     -16.508083599018     -16.508083599018     -16.290124201310   0.0000   0.0000   0.0000   0.0000
     37    0.215542222852695    0.0    0.00     -16.592708602447     -16.592708602447     -16.592708602447     -16.377166379594   0.0000   0.0000   0.0000   0.0000
     38    0.208870605692782    0.0    0.00     -16.671277665355     -16.671277665355     -16.671277665355     -16.462407059663   0.0000   0.0000   0.0000   0.0000
     39    0.196790870167323    0.0    0.00     -16.740639223326     -16.740639223326     -16.740639223326     -16.543848353159   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -7.47157     40
     Pressure of Nuclei (GPa)             0.00000     40
     Pressure Total (GPa)            -7.47157     40


                total energy =      -16.79853401768 Hartree a.u.
              kinetic energy =       12.04304 Hartree a.u.
        electrostatic energy =      -23.02989 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63042 Hartree a.u.
  n-l pseudopotential energy =        1.97135 Hartree a.u.
 exchange-correlation energy =       -4.15262 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.67  -13.64  -10.08   -7.93


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -8.16987808        -0.38095857        -0.00020577
       -0.38095857        -7.07725309        -0.21729946
       -0.00020577        -0.21729946        -7.16756738
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13855903321731E+00    -0.99152916118306E-01     0.86082674114996E-02
   H      -0.59789550667675E-01    -0.17329193341612E-01    -0.61324951271475E-02
   H      -0.23616853134569E-01    -0.45405645418286E-01    -0.30851643375776E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.179324424508036    0.0    0.00     -16.798534017676     -16.798534017676     -16.798534017676     -16.619209593168   0.0000   0.0000   0.0000   0.0000
     41    0.157527830596749    0.0    0.00     -16.844088391729     -16.844088391729     -16.844088391729     -16.686560561132   0.0000   0.0000   0.0000   0.0000
     42    0.133099023075669    0.0    0.00     -16.877611189147     -16.877611189147     -16.877611189147     -16.744512166071   0.0000   0.0000   0.0000   0.0000
     43    0.108063940035057    0.0    0.00     -16.900475946885     -16.900475946885     -16.900475946885     -16.792412006850   0.0000   0.0000   0.0000   0.0000
     44    0.084465744981373    0.0    0.00     -16.914973047868     -16.914973047868     -16.914973047868     -16.830507302887   0.0000   0.0000   0.0000   0.0000
     45    0.063962620910485    0.0    0.00     -16.923773695370     -16.923773695370     -16.923773695370     -16.859811074460   0.0000   0.0000   0.0000   0.0000
     46    0.047373680849961    0.0    0.00     -16.929154768772     -16.929154768772     -16.929154768772     -16.881781087922   0.0000   0.0000   0.0000   0.0000
     47    0.034692306629180    0.0    0.00     -16.932688263282     -16.932688263282     -16.932688263282     -16.897995956652   0.0000   0.0000   0.0000   0.0000
     48    0.025363668913313    0.0    0.00     -16.935253546683     -16.935253546683     -16.935253546683     -16.909889877770   0.0000   0.0000   0.0000   0.0000
     49    0.018616085857168    0.0    0.00     -16.937232419395     -16.937232419395     -16.937232419395     -16.918616333538   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -7.65278     50
     Pressure of Nuclei (GPa)             0.00000     50
     Pressure Total (GPa)            -7.65278     50


                total energy =      -16.93877316314 Hartree a.u.
              kinetic energy =       11.61676 Hartree a.u.
        electrostatic energy =      -22.98751 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63674 Hartree a.u.
  n-l pseudopotential energy =        2.22039 Hartree a.u.
 exchange-correlation energy =       -4.15167 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.80  -13.29  -10.07   -7.32


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -6.38838346        -0.04814252         0.07377813
       -0.04814252        -7.00458109         0.06143892
        0.07377813         0.06143892        -9.56537175
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.66026988692742E-01     0.35902016604248E-01     0.85716802975390E-02
   H       0.48634940735117E-01    -0.96291858059193E-02    -0.10575270128258E-02
   H       0.54919833667315E-03     0.40958932379326E-01    -0.23166865466320E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.013735787740613    0.0    0.00     -16.938773163143     -16.938773163143     -16.938773163143     -16.925037375402   0.0000   0.0000   0.0000   0.0000
     51    0.010196938203195    0.0    0.00     -16.939977483177     -16.939977483177     -16.939977483177     -16.929780544974   0.0000   0.0000   0.0000   0.0000
     52    0.007661052542696    0.0    0.00     -16.940974248396     -16.940974248396     -16.940974248396     -16.933313195854   0.0000   0.0000   0.0000   0.0000
     53    0.005907615187303    0.0    0.00     -16.941904694749     -16.941904694749     -16.941904694749     -16.935997079562   0.0000   0.0000   0.0000   0.0000
     54    0.004759852280475    0.0    0.00     -16.942873315408     -16.942873315408     -16.942873315408     -16.938113463128   0.0000   0.0000   0.0000   0.0000
     55    0.004047031712017    0.0    0.00     -16.943916822937     -16.943916822937     -16.943916822937     -16.939869791225   0.0000   0.0000   0.0000   0.0000
     56    0.003606001708215    0.0    0.00     -16.945009272648     -16.945009272648     -16.945009272648     -16.941403270939   0.0000   0.0000   0.0000   0.0000
     57    0.003300876890973    0.0    0.00     -16.946091136557     -16.946091136557     -16.946091136557     -16.942790259666   0.0000   0.0000   0.0000   0.0000
     58    0.003039220110402    0.0    0.00     -16.947102053012     -16.947102053012     -16.947102053012     -16.944062832902   0.0000   0.0000   0.0000   0.0000
     59    0.002774311844607    0.0    0.00     -16.948001778883     -16.948001778883     -16.948001778883     -16.945227467038   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.40032     60
     Pressure of Nuclei (GPa)             0.00000     60
     Pressure Total (GPa)            -8.40032     60


                total energy =      -16.94877477313 Hartree a.u.
              kinetic energy =       11.67542 Hartree a.u.
        electrostatic energy =      -22.97994 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60909 Hartree a.u.
  n-l pseudopotential energy =        2.13088 Hartree a.u.
 exchange-correlation energy =       -4.16605 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.38  -13.22   -9.33   -7.11


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.88036467        -0.41147292        -0.03974767
       -0.41147292        -7.71255323         0.01211963
       -0.03974767         0.01211963        -9.60804151
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.44925597216259E-01    -0.29728856500333E-01    -0.44161945153395E-02
   H       0.14064672658039E-01     0.65110781954861E-02    -0.24906888588789E-03
   H       0.40066271116628E-02     0.14728895271321E-01    -0.22263444222817E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.002493891257380    0.0    0.00     -16.948774773134     -16.948774773134     -16.948774773134     -16.946280881877   0.0000   0.0000   0.0000   0.0000
     61    0.002203258929227    0.0    0.00     -16.949422330972     -16.949422330972     -16.949422330972     -16.947219072043   0.0000   0.0000   0.0000   0.0000
     62    0.001911638903851    0.0    0.00     -16.949952268202     -16.949952268202     -16.949952268202     -16.948040629298   0.0000   0.0000   0.0000   0.0000
     63    0.001626118718694    0.0    0.00     -16.950372788910     -16.950372788910     -16.950372788910     -16.948746670192   0.0000   0.0000   0.0000   0.0000
     64    0.001351964340860    0.0    0.00     -16.950692251506     -16.950692251506     -16.950692251506     -16.949340287165   0.0000   0.0000   0.0000   0.0000
     65    0.001095004574345    0.0    0.00     -16.950921893315     -16.950921893315     -16.950921893315     -16.949826888741   0.0000   0.0000   0.0000   0.0000
     66    0.000862479837365    0.0    0.00     -16.951077479926     -16.951077479926     -16.951077479926     -16.950215000089   0.0000   0.0000   0.0000   0.0000
     67    0.000661549396299    0.0    0.00     -16.951177943884     -16.951177943884     -16.951177943884     -16.950516394488   0.0000   0.0000   0.0000   0.0000
     68    0.000496713598501    0.0    0.00     -16.951241837108     -16.951241837108     -16.951241837108     -16.950745123510   0.0000   0.0000   0.0000   0.0000
     69    0.000368038261228    0.0    0.00     -16.951283980044     -16.951283980044     -16.951283980044     -16.950915941783   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.10246     70
     Pressure of Nuclei (GPa)             0.00000     70
     Pressure Total (GPa)            -8.10246     70


                total energy =      -16.95131390560 Hartree a.u.
              kinetic energy =       11.73442 Hartree a.u.
        electrostatic energy =      -23.01057 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62878 Hartree a.u.
  n-l pseudopotential energy =        2.13030 Hartree a.u.
 exchange-correlation energy =       -4.17669 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.21  -13.02   -9.35   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.52892547        -0.21611727         0.01663623
       -0.21611727        -7.55321100        -0.00585370
        0.01663623        -0.00585370        -9.22525106
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.88488143355629E-02    -0.12622302421829E-01     0.16959776862269E-02
   H       0.11645030853614E-01     0.29981633469339E-02     0.34599775665477E-04
   H       0.27323463215581E-02     0.11853278849151E-01     0.22630662642151E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.000271447399381    0.0    0.00     -16.951313905603     -16.951313905603     -16.951313905603     -16.951042458204   0.0000   0.0000   0.0000   0.0000
     71    0.000200647918694    0.0    0.00     -16.951336654824     -16.951336654824     -16.951336654824     -16.951136006906   0.0000   0.0000   0.0000   0.0000
     72    0.000149342702200    0.0    0.00     -16.951354722857     -16.951354722857     -16.951354722857     -16.951205380155   0.0000   0.0000   0.0000   0.0000
     73    0.000112642566948    0.0    0.00     -16.951369881035     -16.951369881035     -16.951369881035     -16.951257238468   0.0000   0.0000   0.0000   0.0000
     74    0.000087241884737    0.0    0.00     -16.951383991177     -16.951383991177     -16.951383991177     -16.951296749292   0.0000   0.0000   0.0000   0.0000
     75    0.000070796302455    0.0    0.00     -16.951398820197     -16.951398820197     -16.951398820197     -16.951328023894   0.0000   0.0000   0.0000   0.0000
     76    0.000061176842203    0.0    0.00     -16.951415447740     -16.951415447740     -16.951415447740     -16.951354270898   0.0000   0.0000   0.0000   0.0000
     77    0.000056108433520    0.0    0.00     -16.951433869109     -16.951433869109     -16.951433869109     -16.951377760675   0.0000   0.0000   0.0000   0.0000
     78    0.000053317827987    0.0    0.00     -16.951453104530     -16.951453104530     -16.951453104530     -16.951399786702   0.0000   0.0000   0.0000   0.0000
     79    0.000050913004145    0.0    0.00     -16.951471708650     -16.951471708650     -16.951471708650     -16.951420795646   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.22827     80
     Pressure of Nuclei (GPa)             0.00000     80
     Pressure Total (GPa)            -8.22827     80


                total energy =      -16.95148833575 Hartree a.u.
              kinetic energy =       11.71086 Hartree a.u.
        electrostatic energy =      -22.99819 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62103 Hartree a.u.
  n-l pseudopotential energy =        2.13055 Hartree a.u.
 exchange-correlation energy =       -4.17368 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.04   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.63653731        -0.23608215        -0.00197750
       -0.23608215        -7.64056597         0.00321172
       -0.00197750         0.00321172        -9.40770029
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.21359359769107E-01    -0.20940420906589E-01     0.54611145095145E-05
   H       0.13118109765348E-01     0.24998920582875E-02     0.14880110724548E-03
   H       0.28141516924611E-02     0.12986731870772E-01     0.12865121556207E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.000047699599680    0.0    0.00     -16.951488335753     -16.951488335753     -16.951488335753     -16.951440636153   0.0000   0.0000   0.0000   0.0000
     81    0.000043255702927    0.0    0.00     -16.951502138087     -16.951502138087     -16.951502138087     -16.951458882384   0.0000   0.0000   0.0000   0.0000
     82    0.000037760416840    0.0    0.00     -16.951512839132     -16.951512839132     -16.951512839132     -16.951475078715   0.0000   0.0000   0.0000   0.0000
     83    0.000031702144135    0.0    0.00     -16.951520624906     -16.951520624906     -16.951520624906     -16.951488922762   0.0000   0.0000   0.0000   0.0000
     84    0.000025616630241    0.0    0.00     -16.951525934051     -16.951525934051     -16.951525934051     -16.951500317421   0.0000   0.0000   0.0000   0.0000
     85    0.000019938421808    0.0    0.00     -16.951529292568     -16.951529292568     -16.951529292568     -16.951509354146   0.0000   0.0000   0.0000   0.0000
     86    0.000014952184766    0.0    0.00     -16.951531215440     -16.951531215440     -16.951531215440     -16.951516263255   0.0000   0.0000   0.0000   0.0000
     87    0.000010812209245    0.0    0.00     -16.951532164471     -16.951532164471     -16.951532164471     -16.951521352261   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.108122D-04  0.1D-03  0.949031D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:    88
     Pressure of Nuclei (GPa)             0.00000     88
     Pressure Total (GPa)            -8.19676     88


                total energy =      -16.95153254751 Hartree a.u.
              kinetic energy =       11.71517 Hartree a.u.
        electrostatic energy =      -23.00099 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62300 Hartree a.u.
  n-l pseudopotential energy =        2.13113 Hartree a.u.
 exchange-correlation energy =       -4.17384 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.38   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.61273019        -0.23784311         0.00078769
       -0.23784311        -7.60703917        -0.00267935
        0.00078769        -0.00267935        -9.37050193
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.16925293745574E-01    -0.17615486247655E-01    -0.29702083584581E-03
   H       0.13072060378886E-01     0.28819549435356E-02    -0.21943885940109E-04
   H       0.29133002896128E-02     0.13060399786043E-01    -0.40433924764833E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
     88    0.000007574999046    0.0    0.00     -16.951532547508     -16.951532547508     -16.951532547508     -16.951524972509   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.757500D-05  0.1D-03  0.383038D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.01s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.82188       0.82188 (AU)
   ekin          :       10.84397      10.84397 (AU)
   epot          :      -27.62141     -27.62141 (AU)
   total energy  :      -14.49885     -14.49885 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :      -14.49885     -14.49885 (AU)
   econs         :      -14.49885     -14.49885 (AU)
   pressure      :        2.56037       2.56037 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      1.59s CPU      1.61s WALL (       1 calls)
     main_loop    :     11.93s CPU     12.33s WALL (      88 calls)
     cpr_total    :     11.94s CPU     12.34s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     11.94s CPU     12.34s WALL (      88 calls)
     move_electro :     11.85s CPU     12.23s WALL (      88 calls)

     Called by move_electrons:
     rhoofr       :      3.96s CPU      3.99s WALL (      89 calls)
     vofrho       :      6.50s CPU      6.86s WALL (      89 calls)
     dforce       :      1.49s CPU      1.50s WALL (     178 calls)
     calphi       :      0.01s CPU      0.01s WALL (      89 calls)
     nlfl         :      0.00s CPU      0.00s WALL (      89 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (      89 calls)
     rsg          :      0.01s CPU      0.01s WALL (      89 calls)
     rhoset       :      0.01s CPU      0.01s WALL (      89 calls)
     sigset       :      0.00s CPU      0.00s WALL (      89 calls)
     tauset       :      0.00s CPU      0.00s WALL (      89 calls)
     ortho        :      0.02s CPU      0.02s WALL (      89 calls)
     updatc       :      0.00s CPU      0.00s WALL (      89 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (      89 calls)
     nlfq         :      0.01s CPU      0.01s WALL (      89 calls)
     nlsm1        :      0.00s CPU      0.00s WALL (      90 calls)
     nlsm2        :      0.01s CPU      0.01s WALL (      89 calls)
     fft          :      1.54s CPU      1.63s WALL (     891 calls)
     ffts         :      0.66s CPU      0.66s WALL (     356 calls)
     fftw         :      1.91s CPU      1.92s WALL (    2136 calls)
     fft_scatt_xy :      0.90s CPU      0.92s WALL (    3383 calls)
     fft_scatt_yz :      1.26s CPU      1.28s WALL (    3383 calls)
     betagx       :      0.67s CPU      0.67s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      1.44s CPU      1.44s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.03s CPU      0.03s WALL (       1 calls)
     from_scratch :      0.13s CPU      0.15s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.00s CPU      0.00s WALL (      90 calls)
     exch_corr    :      4.69s CPU      5.00s WALL (      89 calls)


     CP           :     13.62s CPU     14.03s WALL


   This run was terminated on:   2:13:12   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
