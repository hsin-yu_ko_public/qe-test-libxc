#!/bin/bash
mpirun -n 2 ./cp.x < ./input.nspin1.cp > output.nspin1.cp
mpirun -n 2 ./cp.x < ./input.nspin2.cp > output.nspin2.cp

mpirun -n 2 ./pw.x < ./input.nspin1.pw > output.nspin1.pw
mpirun -n 2 ./pw.x < ./input.nspin2.pw > output.nspin2.pw
