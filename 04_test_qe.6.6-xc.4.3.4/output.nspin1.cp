
     Program CP v.6.6 starts on  6Jun2021 at  2: 9:43 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Fft bands division:     nmany     =       1
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
     Message from routine scan_end:
     No INFO block end statement, possibly corrupted file
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Number of Electrons=     8, of States =     4
   Occupation numbers :
   2.00 2.00 2.00 2.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   internal stress tensor calculated
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   nbnd =     4
    2.00 2.00 2.00 2.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       91.24618      0

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.976566965109257    0.0    0.00      13.177390871541      13.177390871541      13.177390871541      14.153957836651   0.0000   0.0000   0.0000   0.0000
      2    2.768909244405164    0.0    0.00      10.600058166398      10.600058166398      10.600058166398      13.368967410803   0.0000   0.0000   0.0000   0.0000
      3    5.066052758181266    0.0    0.00       6.792030275459       6.792030275459       6.792030275459      11.858083033641   0.0000   0.0000   0.0000   0.0000
      4    7.567941515095341    0.0    0.00       1.992403122983       1.992403122983       1.992403122983       9.560344638078   0.0000   0.0000   0.0000   0.0000
      5    8.977433068182705    0.0    0.00      -2.487286622742      -2.487286622742      -2.487286622742       6.490146445441   0.0000   0.0000   0.0000   0.0000
      6    7.554421251647220    0.0    0.00      -3.877474002482      -3.877474002482      -3.877474002482       3.676947249165   0.0000   0.0000   0.0000   0.0000
      7    4.658184310394578    0.0    0.00      -3.068974415938      -3.068974415938      -3.068974415938       1.589209894457   0.0000   0.0000   0.0000   0.0000
      8    2.924499481938871    0.0    0.00      -2.607860454594      -2.607860454594      -2.607860454594       0.316639027345   0.0000   0.0000   0.0000   0.0000
      9    2.653259524457531    0.0    0.00      -3.056457198511      -3.056457198511      -3.056457198511      -0.403197674053   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)      -46.37614     10
     Pressure of Nuclei (GPa)             0.00000     10
     Pressure Total (GPa)           -46.37614     10


                total energy =       -3.91581154271 Hartree a.u.
              kinetic energy =        8.00419 Hartree a.u.
        electrostatic energy =      -15.96150 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.36136 Hartree a.u.
  n-l pseudopotential energy =        3.94856 Hartree a.u.
 exchange-correlation energy =        2.45430 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -78.97  -17.43   -7.60   -1.26


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
      -34.01782505        -3.78803099        -1.13911100
       -3.78803099       -41.32248882        -0.61837294
       -1.13911100        -0.61837294       -63.78811125
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.79060086373037E+00     0.89196972743263E+00     0.21854172685068E+00
   H       0.96502993051912E+00    -0.18157631185234E+00     0.40982315498422E-02
   H      -0.23142632745509E+00     0.95623541655594E+00     0.28179857059205E+00



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.921745780306866    0.0    0.00      -3.915811542706      -3.915811542706      -3.915811542706      -0.994065762399   0.0000   0.0000   0.0000   0.0000
     11    2.951109137111760    0.0    0.00      -4.650147178183      -4.650147178183      -4.650147178183      -1.699038041071   0.0000   0.0000   0.0000   0.0000
     12    2.604414114663908    0.0    0.00      -5.150633379894      -5.150633379894      -5.150633379894      -2.546219265230   0.0000   0.0000   0.0000   0.0000
     13    2.166325509887245    0.0    0.00      -5.534136849890      -5.534136849890      -5.534136849890      -3.367811340003   0.0000   0.0000   0.0000   0.0000
     14    1.842508292997843    0.0    0.00      -5.898611139290      -5.898611139290      -5.898611139290      -4.056102846292   0.0000   0.0000   0.0000   0.0000
     15    1.637426583811155    0.0    0.00      -6.254115896137      -6.254115896137      -6.254115896137      -4.616689312326   0.0000   0.0000   0.0000   0.0000
     16    1.483825867030850    0.0    0.00      -6.574091051032      -6.574091051032      -6.574091051032      -5.090265184001   0.0000   0.0000   0.0000   0.0000
     17    1.340521529252745    0.0    0.00      -6.849322721721      -6.849322721721      -6.849322721721      -5.508801192468   0.0000   0.0000   0.0000   0.0000
     18    1.206185282668607    0.0    0.00      -7.082827649034      -7.082827649034      -7.082827649034      -5.876642366365   0.0000   0.0000   0.0000   0.0000
     19    1.092407502604324    0.0    0.00      -7.271322230101      -7.271322230101      -7.271322230101      -6.178914727497   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -108.27975     20
     Pressure of Nuclei (GPa)             0.00000     20
     Pressure Total (GPa)          -108.27975     20


                total energy =       -7.40959645675 Hartree a.u.
              kinetic energy =        9.87237 Hartree a.u.
        electrostatic energy =      -20.72792 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.07474 Hartree a.u.
  n-l pseudopotential energy =        3.13050 Hartree a.u.
 exchange-correlation energy =        3.39020 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -43.03  -26.72   -8.54   -3.59


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -100.68347453       -12.77715284         1.93396136
      -12.77715284      -105.32439495         3.45620540
        1.93396136         3.45620540      -118.83139385
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18597853645820E+00     0.17980839480207E+00     0.39778613434950E+00
   H       0.46628870771826E+00    -0.49226215907913E+00    -0.85162821792677E-01
   H      -0.27132083848555E+00     0.24632311985514E+00    -0.44014981170299E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.001999542670543    0.0    0.00      -7.409596456754      -7.409596456754      -7.409596456754      -6.407596914084   0.0000   0.0000   0.0000   0.0000
     21    0.927768452630517    0.0    0.00      -7.499429474371      -7.499429474371      -7.499429474371      -6.571661021741   0.0000   0.0000   0.0000   0.0000
     22    0.859467293871489    0.0    0.00      -7.555039144312      -7.555039144312      -7.555039144312      -6.695571850441   0.0000   0.0000   0.0000   0.0000
     23    0.789431278506857    0.0    0.00      -7.598073880181      -7.598073880181      -7.598073880181      -6.808642601675   0.0000   0.0000   0.0000   0.0000
     24    0.714663554494174    0.0    0.00      -7.648931928738      -7.648931928738      -7.648931928738      -6.934268374244   0.0000   0.0000   0.0000   0.0000
     25    0.635832221019282    0.0    0.00      -7.719496398871      -7.719496398871      -7.719496398871      -7.083664177852   0.0000   0.0000   0.0000   0.0000
     26    0.555535685142800    0.0    0.00      -7.811444063914      -7.811444063914      -7.811444063914      -7.255908378772   0.0000   0.0000   0.0000   0.0000
     27    0.477391393522829    0.0    0.00      -7.918782950689      -7.918782950689      -7.918782950689      -7.441391557166   0.0000   0.0000   0.0000   0.0000
     28    0.405934152411218    0.0    0.00      -8.030872652156      -8.030872652156      -8.030872652156      -7.624938499745   0.0000   0.0000   0.0000   0.0000
     29    0.344750138982155    0.0    0.00      -8.138485364099      -8.138485364099      -8.138485364099      -7.793735225117   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -132.93643     30
     Pressure of Nuclei (GPa)             0.00000     30
     Pressure Total (GPa)          -132.93643     30


                total energy =       -8.23464585041 Hartree a.u.
              kinetic energy =       12.11283 Hartree a.u.
        electrostatic energy =      -22.71589 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.65252 Hartree a.u.
  n-l pseudopotential energy =        2.12735 Hartree a.u.
 exchange-correlation energy =        3.89359 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -32.47  -19.20  -12.84   -4.42


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -127.03181308        -6.97875947        -0.90468288
       -6.97875947      -131.34550901        -0.19481661
       -0.90468288        -0.19481661      -140.43195391
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.52262967661253E+00    -0.33971097966693E+00    -0.11747775574641E+00
   H       0.21328404162335E+00    -0.14714284479953E+00    -0.20435689688401E-01
   H      -0.10963352731274E+00     0.13679990819105E+00    -0.49499021078393E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.295909754380649    0.0    0.00      -8.234645850408      -8.234645850408      -8.234645850408      -7.938736096028   0.0000   0.0000   0.0000   0.0000
     31    0.260234515353002    0.0    0.00      -8.315049322056      -8.315049322056      -8.315049322056      -8.054814806703   0.0000   0.0000   0.0000   0.0000
     32    0.236837775910967    0.0    0.00      -8.378029166897      -8.378029166897      -8.378029166897      -8.141191390986   0.0000   0.0000   0.0000   0.0000
     33    0.223915705885947    0.0    0.00      -8.422822156684      -8.422822156684      -8.422822156684      -8.198906450798   0.0000   0.0000   0.0000   0.0000
     34    0.218871047211868    0.0    0.00      -8.450263568871      -8.450263568871      -8.450263568871      -8.231392521659   0.0000   0.0000   0.0000   0.0000
     35    0.218114100914605    0.0    0.00      -8.463205723339      -8.463205723339      -8.463205723339      -8.245091622424   0.0000   0.0000   0.0000   0.0000
     36    0.217959397707294    0.0    0.00      -8.466800555256      -8.466800555256      -8.466800555256      -8.248841157549   0.0000   0.0000   0.0000   0.0000
     37    0.215542222852695    0.0    0.00      -8.466743280435      -8.466743280435      -8.466743280435      -8.251201057582   0.0000   0.0000   0.0000   0.0000
     38    0.208870605692781    0.0    0.00      -8.468900264749      -8.468900264749      -8.468900264749      -8.260029659056   0.0000   0.0000   0.0000   0.0000
     39    0.196790870167323    0.0    0.00      -8.477299979827      -8.477299979827      -8.477299979827      -8.280509109660   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -148.87643     40
     Pressure of Nuclei (GPa)             0.00000     40
     Pressure Total (GPa)          -148.87643     40


                total energy =       -8.49332801255 Hartree a.u.
              kinetic energy =       12.04304 Hartree a.u.
        electrostatic energy =      -23.02989 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63042 Hartree a.u.
  n-l pseudopotential energy =        1.97135 Hartree a.u.
 exchange-correlation energy =        4.15258 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.67  -13.64  -10.08   -7.93


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.57473702        -0.38095857        -0.00020577
       -0.38095857      -148.48211202        -0.21729946
       -0.00020577        -0.21729946      -148.57242632
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13855903321731E+00    -0.99152916118306E-01     0.86082674115035E-02
   H      -0.59789550667674E-01    -0.17329193341612E-01    -0.61324951271476E-02
   H      -0.23616853134570E-01    -0.45405645418286E-01    -0.30851643375776E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.179324424508037    0.0    0.00      -8.493328012553      -8.493328012553      -8.493328012553      -8.314003588045   0.0000   0.0000   0.0000   0.0000
     41    0.157527830596750    0.0    0.00      -8.515591855485      -8.515591855485      -8.515591855485      -8.358064024888   0.0000   0.0000   0.0000   0.0000
     42    0.133099023075669    0.0    0.00      -8.541299580081      -8.541299580081      -8.541299580081      -8.408200557005   0.0000   0.0000   0.0000   0.0000
     43    0.108063940035057    0.0    0.00      -8.567616822389      -8.567616822389      -8.567616822389      -8.459552882354   0.0000   0.0000   0.0000   0.0000
     44    0.084465744981374    0.0    0.00      -8.591955875784      -8.591955875784      -8.591955875784      -8.507490130803   0.0000   0.0000   0.0000   0.0000
     45    0.063962620910485    0.0    0.00      -8.612571110940      -8.612571110940      -8.612571110940      -8.548608490029   0.0000   0.0000   0.0000   0.0000
     46    0.047373680849961    0.0    0.00      -8.627809839184      -8.627809839184      -8.627809839184      -8.580436158334   0.0000   0.0000   0.0000   0.0000
     47    0.034692306629181    0.0    0.00      -8.637380836161      -8.637380836161      -8.637380836161      -8.602688529532   0.0000   0.0000   0.0000   0.0000
     48    0.025363668913314    0.0    0.00      -8.641226128337      -8.641226128337      -8.641226128337      -8.615862459423   0.0000   0.0000   0.0000   0.0000
     49    0.018616085857169    0.0    0.00      -8.640259665633      -8.640259665633      -8.640259665633      -8.621643579776   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -149.01674     50
     Pressure of Nuclei (GPa)             0.00000     50
     Pressure Total (GPa)          -149.01674     50


                total energy =       -8.63596920519 Hartree a.u.
              kinetic energy =       11.61676 Hartree a.u.
        electrostatic energy =      -22.98751 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63674 Hartree a.u.
  n-l pseudopotential energy =        2.22039 Hartree a.u.
 exchange-correlation energy =        4.15114 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.80  -13.29  -10.07   -7.32


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -147.75234502        -0.04814252         0.07377813
       -0.04814252      -148.36854265         0.06143892
        0.07377813         0.06143892      -150.92933331
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.66026988692719E-01     0.35902016604265E-01     0.85716802975705E-02
   H       0.48634940735119E-01    -0.96291858059191E-02    -0.10575270128265E-02
   H       0.54919833667253E-03     0.40958932379327E-01    -0.23166865466327E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.013735787740581    0.0    0.00      -8.635969205194      -8.635969205194      -8.635969205194      -8.622233417454   0.0000   0.0000   0.0000   0.0000
     51    0.010196938203141    0.0    0.00      -8.629890573475      -8.629890573475      -8.629890573475      -8.619693635272   0.0000   0.0000   0.0000   0.0000
     52    0.007661052542648    0.0    0.00      -8.623642071497      -8.623642071497      -8.623642071497      -8.615981018954   0.0000   0.0000   0.0000   0.0000
     53    0.005907615187263    0.0    0.00      -8.618545180560      -8.618545180560      -8.618545180560      -8.612637565373   0.0000   0.0000   0.0000   0.0000
     54    0.004759852280456    0.0    0.00      -8.615341000942      -8.615341000942      -8.615341000942      -8.610581148662   0.0000   0.0000   0.0000   0.0000
     55    0.004047031712014    0.0    0.00      -8.614160130642      -8.614160130642      -8.614160130642      -8.610113098930   0.0000   0.0000   0.0000   0.0000
     56    0.003606001708224    0.0    0.00      -8.614628042963      -8.614628042963      -8.614628042963      -8.611022041255   0.0000   0.0000   0.0000   0.0000
     57    0.003300876890986    0.0    0.00      -8.615929156510      -8.615929156510      -8.615929156510      -8.612628279619   0.0000   0.0000   0.0000   0.0000
     58    0.003039220110412    0.0    0.00      -8.617240160895      -8.617240160895      -8.617240160895      -8.614200940785   0.0000   0.0000   0.0000   0.0000
     59    0.002774311844582    0.0    0.00      -8.618054768343      -8.618054768343      -8.618054768343      -8.615280456498   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.24346     60
     Pressure of Nuclei (GPa)             0.00000     60
     Pressure Total (GPa)          -150.24346     60


                total energy =       -8.61782723457 Hartree a.u.
              kinetic energy =       11.67542 Hartree a.u.
        electrostatic energy =      -22.97994 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60909 Hartree a.u.
  n-l pseudopotential energy =        2.13088 Hartree a.u.
 exchange-correlation energy =        4.16489 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.38  -13.22   -9.33   -7.11


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72350026        -0.41147292        -0.03974767
       -0.41147292      -149.55568882         0.01211963
       -0.03974767         0.01211963      -151.45117710
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.44925597216311E-01    -0.29728856500444E-01    -0.44161945153448E-02
   H       0.14064672658043E-01     0.65110781954664E-02    -0.24906888592039E-03
   H       0.40066271116853E-02     0.14728895271355E-01    -0.22263444223956E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.002493891257329    0.0    0.00      -8.617827234565      -8.617827234565      -8.617827234565      -8.615333343308   0.0000   0.0000   0.0000   0.0000
     61    0.002203258929179    0.0    0.00      -8.616496823625      -8.616496823625      -8.616496823625      -8.614293564695   0.0000   0.0000   0.0000   0.0000
     62    0.001911638903848    0.0    0.00      -8.614087524999      -8.614087524999      -8.614087524999      -8.612175886096   0.0000   0.0000   0.0000   0.0000
     63    0.001626118718718    0.0    0.00      -8.610924953910      -8.610924953910      -8.610924953910      -8.609298835192   0.0000   0.0000   0.0000   0.0000
     64    0.001351964341712    0.0    0.00      -8.607527558669      -8.607527558669      -8.607527558669      -8.606175594328   0.0000   0.0000   0.0000   0.0000
     65    0.001095004575908    0.0    0.00      -8.604271458342      -8.604271458342      -8.604271458342      -8.603176453766   0.0000   0.0000   0.0000   0.0000
     66    0.000862479838953    0.0    0.00      -8.601558496138      -8.601558496138      -8.601558496138      -8.600696016299   0.0000   0.0000   0.0000   0.0000
     67    0.000661549396849    0.0    0.00      -8.599663437281      -8.599663437281      -8.599663437281      -8.599001887884   0.0000   0.0000   0.0000   0.0000
     68    0.000496713597634    0.0    0.00      -8.598687522969      -8.598687522969      -8.598687522969      -8.598190809372   0.0000   0.0000   0.0000   0.0000
     69    0.000368038260550    0.0    0.00      -8.598583351768      -8.598583351768      -8.598583351768      -8.598215313507   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30510     70
     Pressure of Nuclei (GPa)             0.00000     70
     Pressure Total (GPa)          -150.30510     70


                total energy =       -8.59925136836 Hartree a.u.
              kinetic energy =       11.73442 Hartree a.u.
        electrostatic energy =      -23.01057 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62878 Hartree a.u.
  n-l pseudopotential energy =        2.13030 Hartree a.u.
 exchange-correlation energy =        4.17537 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.21  -13.02   -9.35   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.73156611        -0.21611728         0.01663623
       -0.21611728      -149.75585163        -0.00585370
        0.01663623        -0.00585370      -151.42789169
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.88488143606766E-02    -0.12622302420568E-01     0.16959776909544E-02
   H       0.11645030854418E-01     0.29981633457739E-02     0.34599779220851E-04
   H       0.27323463197874E-02     0.11853278848794E-01     0.22630662556334E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.000271447385651    0.0    0.00      -8.599251368360      -8.599251368360      -8.599251368360      -8.598979920974   0.0000   0.0000   0.0000   0.0000
     71    0.000200647901617    0.0    0.00      -8.600414480829      -8.600414480829      -8.600414480829      -8.600213832927   0.0000   0.0000   0.0000   0.0000
     72    0.000149342703732    0.0    0.00      -8.601821860737      -8.601821860737      -8.601821860737      -8.601672518033   0.0000   0.0000   0.0000   0.0000
     73    0.000112642583120    0.0    0.00      -8.603244776756      -8.603244776756      -8.603244776756      -8.603132134173   0.0000   0.0000   0.0000   0.0000
     74    0.000087241904723    0.0    0.00      -8.604485597178      -8.604485597178      -8.604485597178      -8.604398355274   0.0000   0.0000   0.0000   0.0000
     75    0.000070796314029    0.0    0.00      -8.605396196803      -8.605396196803      -8.605396196803      -8.605325400489   0.0000   0.0000   0.0000   0.0000
     76    0.000061176840822    0.0    0.00      -8.605976734493      -8.605976734493      -8.605976734493      -8.605915557652   0.0000   0.0000   0.0000   0.0000
     77    0.000056108429281    0.0    0.00      -8.606192056299      -8.606192056299      -8.606192056299      -8.606135947870   0.0000   0.0000   0.0000   0.0000
     78    0.000053317753745    0.0    0.00      -8.606129145622      -8.606129145622      -8.606129145622      -8.606075827868   0.0000   0.0000   0.0000   0.0000
     79    0.000050912875275    0.0    0.00      -8.605877305654      -8.605877305654      -8.605877305654      -8.605826392779   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.32663     80
     Pressure of Nuclei (GPa)             0.00000     80
     Pressure Total (GPa)          -150.32663     80


                total energy =       -8.60555020604 Hartree a.u.
              kinetic energy =       11.71086 Hartree a.u.
        electrostatic energy =      -22.99819 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62103 Hartree a.u.
  n-l pseudopotential energy =        2.13055 Hartree a.u.
 exchange-correlation energy =        4.17226 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.04   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.73490344        -0.23608215        -0.00197750
       -0.23608215      -149.73893209         0.00321169
       -0.00197750         0.00321169      -151.50606639
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.21359357278283E-01    -0.20940419871096E-01     0.54588893774721E-05
   H       0.13118109643929E-01     0.24998921461698E-02     0.14880121118898E-03
   H       0.28141518232732E-02     0.12986731933541E-01     0.12865128326890E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.000047699508905    0.0    0.00      -8.605550206038      -8.605550206038      -8.605550206038      -8.605502506529   0.0000   0.0000   0.0000   0.0000
     81    0.000043255546966    0.0    0.00      -8.605204499883      -8.605204499883      -8.605204499883      -8.605161244336   0.0000   0.0000   0.0000   0.0000
     82    0.000037760198660    0.0    0.00      -8.604929710428      -8.604929710428      -8.604929710428      -8.604891950229   0.0000   0.0000   0.0000   0.0000
     83    0.000031702015984    0.0    0.00      -8.604764671104      -8.604764671104      -8.604764671104      -8.604732969088   0.0000   0.0000   0.0000   0.0000
     84    0.000025616591900    0.0    0.00      -8.604717422650      -8.604717422650      -8.604717422650      -8.604691806058   0.0000   0.0000   0.0000   0.0000
     85    0.000019938394001    0.0    0.00      -8.604758580127      -8.604758580127      -8.604758580127      -8.604738641733   0.0000   0.0000   0.0000   0.0000
     86    0.000014952206099    0.0    0.00      -8.604876264005      -8.604876264005      -8.604876264005      -8.604861311799   0.0000   0.0000   0.0000   0.0000
     87    0.000010811870623    0.0    0.00      -8.605043233718      -8.605043233718      -8.605043233718      -8.605032421848   0.0000   0.0000   0.0000   0.0000
     88    0.000007574238348    0.0    0.00      -8.605219370073      -8.605219370073      -8.605219370073      -8.605211795835   0.0000   0.0000   0.0000   0.0000
     89    0.000005213729314    0.0    0.00      -8.605369623305      -8.605369623305      -8.605369623305      -8.605364409576   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30485     90
     Pressure of Nuclei (GPa)             0.00000     90
     Pressure Total (GPa)          -150.30485     90


                total energy =       -8.60545481037 Hartree a.u.
              kinetic energy =       11.71461 Hartree a.u.
        electrostatic energy =      -23.00048 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62264 Hartree a.u.
  n-l pseudopotential energy =        2.13069 Hartree a.u.
 exchange-correlation energy =        4.17236 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72229436        -0.24086206        -0.00195098
       -0.24086206      -149.71747857        -0.00183422
       -0.00195098        -0.00183422      -151.47476976
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17149222786208E-01    -0.17128915592236E-01    -0.19071645517609E-03
   H       0.13057404610566E-01     0.29304384661924E-02    -0.35294077531247E-04
   H       0.28744770913296E-02     0.13091702356921E-01    -0.61315520843385E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.000003633193029    0.0    0.00      -8.605454810372      -8.605454810372      -8.605454810372      -8.605451177179   0.0000   0.0000   0.0000   0.0000
     91    0.000002683889106    0.0    0.00      -8.605498070454      -8.605498070454      -8.605498070454      -8.605495386565   0.0000   0.0000   0.0000   0.0000
     92    0.000002186734824    0.0    0.00      -8.605494377087      -8.605494377087      -8.605494377087      -8.605492190353   0.0000   0.0000   0.0000   0.0000
     93    0.000001963565468    0.0    0.00      -8.605450556294      -8.605450556294      -8.605450556294      -8.605448592729   0.0000   0.0000   0.0000   0.0000
     94    0.000001868146760    0.0    0.00      -8.605381094073      -8.605381094073      -8.605381094073      -8.605379225926   0.0000   0.0000   0.0000   0.0000
     95    0.000001797079586    0.0    0.00      -8.605310193321      -8.605310193321      -8.605310193321      -8.605308396242   0.0000   0.0000   0.0000   0.0000
     96    0.000001694593645    0.0    0.00      -8.605245713397      -8.605245713397      -8.605245713397      -8.605244018803   0.0000   0.0000   0.0000   0.0000
     97    0.000001545039992    0.0    0.00      -8.605199795316      -8.605199795316      -8.605199795316      -8.605198250276   0.0000   0.0000   0.0000   0.0000
     98    0.000001354045090    0.0    0.00      -8.605169741318      -8.605169741318      -8.605169741318      -8.605168387273   0.0000   0.0000   0.0000   0.0000
     99    0.000001139741421    0.0    0.00      -8.605162912575      -8.605162912575      -8.605162912575      -8.605161772834   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   100

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30470    100
     Pressure of Nuclei (GPa)             0.00000    100
     Pressure Total (GPa)          -150.30470    100


                total energy =       -8.60517062744 Hartree a.u.
              kinetic energy =       11.71349 Hartree a.u.
        electrostatic energy =      -23.00047 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62258 Hartree a.u.
  n-l pseudopotential energy =        2.13188 Hartree a.u.
 exchange-correlation energy =        4.17252 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.71612467        -0.24340790         0.00104999
       -0.24340790      -149.71780996         0.00072462
        0.00104999         0.00072462      -151.48016231
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18089582372139E-01    -0.18115851431653E-01     0.46449057720755E-04
   H       0.13155652236953E-01     0.29509160944247E-02    -0.16931026387749E-04
   H       0.29459822676780E-02     0.13176798497028E-01    -0.15191715714270E-05



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    100    0.000000924385343    0.0    0.00      -8.605170627443      -8.605170627443      -8.605170627443      -8.605169703058   0.0000   0.0000   0.0000   0.0000
    101    0.000000724836712    0.0    0.00      -8.605191774357      -8.605191774357      -8.605191774357      -8.605191049520   0.0000   0.0000   0.0000   0.0000
    102    0.000000553006760    0.0    0.00      -8.605212836892      -8.605212836892      -8.605212836892      -8.605212283885   0.0000   0.0000   0.0000   0.0000
    103    0.000000414779958    0.0    0.00      -8.605238189331      -8.605238189331      -8.605238189331      -8.605237774551   0.0000   0.0000   0.0000   0.0000
    104    0.000000310971872    0.0    0.00      -8.605245266616      -8.605245266616      -8.605245266616      -8.605244955644   0.0000   0.0000   0.0000   0.0000
    105    0.000000238509245    0.0    0.00      -8.605239902921      -8.605239902921      -8.605239902921      -8.605239664411   0.0000   0.0000   0.0000   0.0000
    106    0.000000190728232    0.0    0.00      -8.605225476520      -8.605225476520      -8.605225476520      -8.605225285792   0.0000   0.0000   0.0000   0.0000
    107    0.000000159739961    0.0    0.00      -8.605205440342      -8.605205440342      -8.605205440342      -8.605205280602   0.0000   0.0000   0.0000   0.0000
    108    0.000000138414266    0.0    0.00      -8.605177565340      -8.605177565340      -8.605177565340      -8.605177426925   0.0000   0.0000   0.0000   0.0000
    109    0.000000121178385    0.0    0.00      -8.605146668851      -8.605146668851      -8.605146668851      -8.605146547673   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   110

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30772    110
     Pressure of Nuclei (GPa)             0.00000    110
     Pressure Total (GPa)          -150.30772    110


                total energy =       -8.60512033806 Hartree a.u.
              kinetic energy =       11.71399 Hartree a.u.
        electrostatic energy =      -23.00044 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62259 Hartree a.u.
  n-l pseudopotential energy =        2.13137 Hartree a.u.
 exchange-correlation energy =        4.17254 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72180578        -0.24096619        -0.00049069
       -0.24096619      -149.72174330         0.00025094
       -0.00049069         0.00025094      -151.47960468
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18078458179466E-01    -0.18059669634319E-01    -0.22783245187375E-04
   H       0.13141175439783E-01     0.29075001722764E-02     0.21483187725515E-05
   H       0.29022997330006E-02     0.13118985856594E-01     0.11113464119991E-06



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    110    0.000000105025918    0.0    0.00      -8.605120338055      -8.605120338055      -8.605120338055      -8.605120233029   0.0000   0.0000   0.0000   0.0000
    111    0.000000089065640    0.0    0.00      -8.605098608740      -8.605098608740      -8.605098608740      -8.605098519675   0.0000   0.0000   0.0000   0.0000
    112    0.000000073419989    0.0    0.00      -8.605083718567      -8.605083718567      -8.605083718567      -8.605083645147   0.0000   0.0000   0.0000   0.0000
    113    0.000000059173864    0.0    0.00      -8.605082474172      -8.605082474172      -8.605082474172      -8.605082414998   0.0000   0.0000   0.0000   0.0000
    114    0.000000046593962    0.0    0.00      -8.605083242459      -8.605083242459      -8.605083242459      -8.605083195865   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.465940D-07  0.1D-03  0.768287D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:   115
     Pressure of Nuclei (GPa)             0.00000    115
     Pressure Total (GPa)          -150.30825    115


                total energy =       -8.60509496867 Hartree a.u.
              kinetic energy =       11.71416 Hartree a.u.
        electrostatic energy =      -23.00050 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62260 Hartree a.u.
  n-l pseudopotential energy =        2.13129 Hartree a.u.
 exchange-correlation energy =        4.17255 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72217159        -0.24084543        -0.00001093
       -0.24084543      -149.72251825         0.00021126
       -0.00001093         0.00021126      -151.48005604
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18066344841213E-01    -0.18034168397628E-01     0.34400074156241E-04
   H       0.13102789915084E-01     0.28972560378287E-02    -0.62786064936187E-06
   H       0.29005309492835E-02     0.13099142224797E-01     0.71954229080742E-05



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
    115    0.000000036175653    0.0    0.00      -8.605094968665      -8.605094968665      -8.605094968665      -8.605094932490   0.0000   0.0000   0.0000   0.0000

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.00s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.62892       0.62892 (AU)
   ekin          :       11.04821      11.04821 (AU)
   epot          :      -20.74579     -20.74579 (AU)
   total energy  :       -7.45355      -7.45355 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :       -7.45355      -7.45355 (AU)
   econs         :       -7.45355      -7.45355 (AU)
   pressure      :     -129.72443    -129.72443 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      1.37s CPU      1.38s WALL (       1 calls)
     main_loop    :     16.21s CPU     16.69s WALL (     115 calls)
     cpr_total    :     16.22s CPU     16.70s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     16.21s CPU     16.70s WALL (     115 calls)
     move_electro :     16.10s CPU     16.57s WALL (     115 calls)

     Called by move_electrons:
     rhoofr       :      5.41s CPU      5.47s WALL (     116 calls)
     vofrho       :      8.76s CPU      9.17s WALL (     116 calls)
     dforce       :      2.02s CPU      2.04s WALL (     232 calls)
     calphi       :      0.01s CPU      0.01s WALL (     116 calls)
     nlfl         :      0.00s CPU      0.00s WALL (     116 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (     116 calls)
     rsg          :      0.01s CPU      0.01s WALL (     116 calls)
     rhoset       :      0.01s CPU      0.01s WALL (     116 calls)
     sigset       :      0.01s CPU      0.01s WALL (     116 calls)
     tauset       :      0.01s CPU      0.01s WALL (     116 calls)
     ortho        :      0.03s CPU      0.03s WALL (     116 calls)
     updatc       :      0.00s CPU      0.00s WALL (     116 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (     116 calls)
     nlfq         :      0.02s CPU      0.02s WALL (     116 calls)
     nlsm1        :      0.00s CPU      0.00s WALL (     117 calls)
     nlsm2        :      0.01s CPU      0.01s WALL (     116 calls)
     fft          :      2.20s CPU      2.28s WALL (    1161 calls)
     ffts         :      0.93s CPU      0.94s WALL (     464 calls)
     fftw         :      2.56s CPU      2.60s WALL (    2784 calls)
     fft_scatt_xy :      1.25s CPU      1.28s WALL (    4409 calls)
     fft_scatt_yz :      1.95s CPU      1.99s WALL (    4409 calls)
     fft_scatt_tg :      0.02s CPU      0.02s WALL (     696 calls)
     betagx       :      0.54s CPU      0.54s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      1.21s CPU      1.21s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.05s CPU      0.05s WALL (       1 calls)
     from_scratch :      0.14s CPU      0.16s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.00s CPU      0.00s WALL (     117 calls)


     CP           :     17.62s CPU     18.12s WALL


   This run was terminated on:   2:10: 2   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
