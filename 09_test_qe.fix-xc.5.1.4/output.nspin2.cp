
     Program CP v.6.7GPU starts on  6Jun2021 at 12:14:17 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
         "P. Giannozzi et al., J. Chem. Phys. 152 154105 (2020);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
     Message from routine scan_end:
     No INFO block end statement, possibly corrupted file
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     WARNING: libxc functional with ID  263 depends
      on external parameters: check the user_guide of
      QE if you need to modify them or to check their
      default values.

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Local Spin Density calculation
   Number of Electrons=     8
   Spins up   =     4, occupations: 
   1.00 1.00 1.00 1.00
   Spins down =     4, occupations: 
   1.00 1.00 1.00 1.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249

     Using Pencil Decomposition


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

     Subspace diagonalization in iterative solution of the eigenvalue problem:
     a serial algorithm will be used


   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   spin =   1 nbnd =     4
    1.00 1.00 1.00 1.00
   spin =   2 nbnd =     4
    1.00 1.00 1.00 1.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.560053903521069    0.0    0.00      11.266732542850      11.266732542850      11.266732542850      11.826786446371   0.0000   0.0000   0.0000   0.0000
      2    1.603795329853129    0.0    0.00       9.782787968081       9.782787968081       9.782787968081      11.386583297934   0.0000   0.0000   0.0000   0.0000
      3    2.934881008684580    0.0    0.00       7.541785868361       7.541785868361       7.541785868361      10.476666877046   0.0000   0.0000   0.0000   0.0000
      4    4.363763604622264    0.0    0.00       4.659291331368       4.659291331368       4.659291331368       9.023054935990   0.0000   0.0000   0.0000   0.0000
      5    5.742671137581654    0.0    0.00       1.252945830681       1.252945830681       1.252945830681       6.995616968263   0.0000   0.0000   0.0000   0.0000
      6    6.777819125796152    0.0    0.00      -2.341966537831      -2.341966537831      -2.341966537831       4.435852587965   0.0000   0.0000   0.0000   0.0000
      7    6.993279224333411    0.0    0.00      -5.384698247169      -5.384698247169      -5.384698247169       1.608580977164   0.0000   0.0000   0.0000   0.0000
      8    6.145396605496087    0.0    0.00      -7.170616364579      -7.170616364579      -7.170616364579      -1.025219759083   0.0000   0.0000   0.0000   0.0000
      9    4.605201298177610    0.0    0.00      -7.714930694819      -7.714930694819      -7.714930694819      -3.109729396642   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.99 (Slater)  1.36 (Becke)  0.00 (expected)


                total energy =       -7.65761275266 Hartree a.u.
              kinetic energy =        5.55432 Hartree a.u.
        electrostatic energy =      -12.29809 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -1.38444 Hartree a.u.
  n-l pseudopotential energy =        2.40118 Hartree a.u.
 exchange-correlation energy =       -1.93059 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -96.23  -17.35   -7.06    1.67

   Eigenvalues (eV), kp =   1 , spin =  2

  -77.08  -17.55   -2.56   -0.87


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.81954118567918E+00    -0.68782266213582E+00     0.15710864587741E+00
   H       0.11117511110626E+01    -0.21768624418487E+00     0.13598081977667E+00
   H      -0.23238139340416E+00     0.12191179633486E+01     0.59729543726439E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    3.087977341898245    0.0    0.00      -7.657612752659      -7.657612752659      -7.657612752659      -4.569635410761   0.0000   0.0000   0.0000   0.0000
     11    2.090706639944963    0.0    0.00      -7.653127904309      -7.653127904309      -7.653127904309      -5.562421264364   0.0000   0.0000   0.0000   0.0000
     12    1.669699753893150    0.0    0.00      -7.980670022470      -7.980670022470      -7.980670022470      -6.310970268577   0.0000   0.0000   0.0000   0.0000
     13    1.604862449822732    0.0    0.00      -8.588113675447      -8.588113675447      -8.588113675447      -6.983251225625   0.0000   0.0000   0.0000   0.0000
     14    1.641100074126613    0.0    0.00      -9.291806812464      -9.291806812464      -9.291806812464      -7.650706738337   0.0000   0.0000   0.0000   0.0000
     15    1.626044432926755    0.0    0.00      -9.937845142556      -9.937845142556      -9.937845142556      -8.311800709629   0.0000   0.0000   0.0000   0.0000
     16    1.532516952795582    0.0    0.00     -10.473514277871     -10.473514277871     -10.473514277871      -8.940997325075   0.0000   0.0000   0.0000   0.0000
     17    1.404324549662028    0.0    0.00     -10.926005858827     -10.926005858827     -10.926005858827      -9.521681309165   0.0000   0.0000   0.0000   0.0000
     18    1.290555575488649    0.0    0.00     -11.345791198783     -11.345791198783     -11.345791198783     -10.055235623294   0.0000   0.0000   0.0000   0.0000
     19    1.210834247050838    0.0    0.00     -11.764690751746     -11.764690751746     -11.764690751746     -10.553856504695   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.23 (Slater)  1.34 (Becke)  0.00 (expected)


                total energy =      -12.18326517946 Hartree a.u.
              kinetic energy =        7.41624 Hartree a.u.
        electrostatic energy =      -17.95194 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.59210 Hartree a.u.
  n-l pseudopotential energy =        3.85961 Hartree a.u.
 exchange-correlation energy =       -2.91507 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -60.56  -27.49   -4.62   -0.73

   Eigenvalues (eV), kp =   1 , spin =  2

  -61.26  -28.51   -8.70   -1.13


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13643106449708E+00     0.11686801542972E+01     0.60356413583993E+00
   H       0.67511960858644E+00    -0.26119952623667E+00     0.23738318109228E+00
   H      -0.23975584098168E+00     0.63638349994351E+00     0.37394999707139E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.155150824711479    0.0    0.00     -12.183265179458     -12.183265179458     -12.183265179458     -11.028114354746   0.0000   0.0000   0.0000   0.0000
     21    1.102649030086242    0.0    0.00     -12.584033324473     -12.584033324473     -12.584033324473     -11.481384294387   0.0000   0.0000   0.0000   0.0000
     22    1.038814460542803    0.0    0.00     -12.949236726564     -12.949236726564     -12.949236726564     -11.910422266021   0.0000   0.0000   0.0000   0.0000
     23    0.961966518388354    0.0    0.00     -13.271857186280     -13.271857186280     -13.271857186280     -12.309890667892   0.0000   0.0000   0.0000   0.0000
     24    0.879837462430506    0.0    0.00     -13.556425711941     -13.556425711941     -13.556425711941     -12.676588249511   0.0000   0.0000   0.0000   0.0000
     25    0.802429806728668    0.0    0.00     -13.813793145264     -13.813793145264     -13.813793145264     -13.011363338536   0.0000   0.0000   0.0000   0.0000
     26    0.736251358098577    0.0    0.00     -14.053972725323     -14.053972725323     -14.053972725323     -13.317721367224   0.0000   0.0000   0.0000   0.0000
     27    0.682817960425461    0.0    0.00     -14.283662572744     -14.283662572744     -14.283662572744     -13.600844612319   0.0000   0.0000   0.0000   0.0000
     28    0.639349248501645    0.0    0.00     -14.504520011735     -14.504520011735     -14.504520011735     -13.865170763234   0.0000   0.0000   0.0000   0.0000
     29    0.600781049872456    0.0    0.00     -14.714080294477     -14.714080294477     -14.714080294477     -14.113299244604   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 1.87 (Slater)  1.17 (Becke)  0.00 (expected)


                total energy =      -14.90931153674 Hartree a.u.
              kinetic energy =        9.13819 Hartree a.u.
        electrostatic energy =      -20.83208 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.09571 Hartree a.u.
  n-l pseudopotential energy =        3.31224 Hartree a.u.
 exchange-correlation energy =       -3.43195 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -44.26  -26.48   -6.62   -4.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -44.91  -27.37  -20.89   -1.45


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.39488149603424E+00     0.44809169679695E+00    -0.89627768863043E-01
   H       0.36654010015240E+00    -0.15484445149137E+00    -0.65615896901116E-01
   H      -0.16972495941098E+00     0.23848630108894E+00    -0.37290591278377E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.563150887344041    0.0    0.00     -14.909311536736     -14.909311536736     -14.909311536736     -14.346160649392   0.0000   0.0000   0.0000   0.0000
     31    0.524712217960598    0.0    0.00     -15.088303778171     -15.088303778171     -15.088303778171     -14.563591560210   0.0000   0.0000   0.0000   0.0000
     32    0.485693486028691    0.0    0.00     -15.251014920340     -15.251014920340     -15.251014920340     -14.765321434311   0.0000   0.0000   0.0000   0.0000
     33    0.447469481475417    0.0    0.00     -15.399010265765     -15.399010265765     -15.399010265765     -14.951540784290   0.0000   0.0000   0.0000   0.0000
     34    0.411306974215521    0.0    0.00     -15.534186684927     -15.534186684927     -15.534186684927     -15.122879710711   0.0000   0.0000   0.0000   0.0000
     35    0.377863655383932    0.0    0.00     -15.658218822689     -15.658218822689     -15.658218822689     -15.280355167305   0.0000   0.0000   0.0000   0.0000
     36    0.347310563698092    0.0    0.00     -15.772408085749     -15.772408085749     -15.772408085749     -15.425097522051   0.0000   0.0000   0.0000   0.0000
     37    0.319410971424535    0.0    0.00     -15.877652211180     -15.877652211180     -15.877652211180     -15.558241239756   0.0000   0.0000   0.0000   0.0000
     38    0.293540053795523    0.0    0.00     -15.974281314217     -15.974281314217     -15.974281314217     -15.680741260422   0.0000   0.0000   0.0000   0.0000
     39    0.268892172123779    0.0    0.00     -16.062036332185     -16.062036332185     -16.062036332185     -15.793144160061   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 1.43 (Slater)  1.02 (Becke)  0.00 (expected)


                total energy =      -16.14084694047 Hartree a.u.
              kinetic energy =       11.25620 Hartree a.u.
        electrostatic energy =      -22.48083 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.55091 Hartree a.u.
  n-l pseudopotential energy =        2.50380 Hartree a.u.
 exchange-correlation energy =       -3.86910 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -34.89  -20.57  -14.99   -6.16

   Eigenvalues (eV), kp =   1 , spin =  2

  -34.02  -21.26  -16.89   -2.53


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.29462794656655E+00    -0.22545207983862E+00     0.48091605909111E-01
   H       0.14456168094876E+00    -0.19227192337526E-01    -0.26926886002115E-02
   H      -0.66908242511493E-01     0.84458491467983E-01     0.59486452555737E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.245040265577625    0.0    0.00     -16.140846940470     -16.140846940470     -16.140846940470     -15.895806674893   0.0000   0.0000   0.0000   0.0000
     41    0.222052004083421    0.0    0.00     -16.211128946150     -16.211128946150     -16.211128946150     -15.989076942067   0.0000   0.0000   0.0000   0.0000
     42    0.200260663304616    0.0    0.00     -16.273614105097     -16.273614105097     -16.273614105097     -16.073353441792   0.0000   0.0000   0.0000   0.0000
     43    0.180087862774656    0.0    0.00     -16.329309829828     -16.329309829828     -16.329309829828     -16.149221967054   0.0000   0.0000   0.0000   0.0000
     44    0.161886171215957    0.0    0.00     -16.379293081408     -16.379293081408     -16.379293081408     -16.217406910192   0.0000   0.0000   0.0000   0.0000
     45    0.145913941830087    0.0    0.00     -16.424687212430     -16.424687212430     -16.424687212430     -16.278773270600   0.0000   0.0000   0.0000   0.0000
     46    0.132207125868511    0.0    0.00     -16.466437383317     -16.466437383317     -16.466437383317     -16.334230257448   0.0000   0.0000   0.0000   0.0000
     47    0.120624083552357    0.0    0.00     -16.505288547972     -16.505288547972     -16.505288547972     -16.384664464419   0.0000   0.0000   0.0000   0.0000
     48    0.110879577170907    0.0    0.00     -16.541754652495     -16.541754652495     -16.541754652495     -16.430875075324   0.0000   0.0000   0.0000   0.0000
     49    0.102571617052972    0.0    0.00     -16.576081690903     -16.576081690903     -16.576081690903     -16.473510073850   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.98 (Slater)  0.79 (Becke)  0.00 (expected)


                total energy =      -16.60831823019 Hartree a.u.
              kinetic energy =       11.69044 Hartree a.u.
        electrostatic energy =      -22.80921 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60948 Hartree a.u.
  n-l pseudopotential energy =        2.12652 Hartree a.u.
 exchange-correlation energy =       -4.00659 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -31.97  -17.38  -13.46  -10.07

   Eigenvalues (eV), kp =   1 , spin =  2

  -30.66  -17.57  -14.04   -3.81


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20492355250032E+00    -0.19342150750437E+00    -0.13444381082593E-01
   H       0.53134100781042E-01    -0.29978556123966E-01    -0.18269577748625E-01
   H      -0.23571151818621E-01     0.31583197928366E-02    -0.22950625555088E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.095267276710891    0.0    0.00     -16.608318230191     -16.608318230191     -16.608318230191     -16.513050953481   0.0000   0.0000   0.0000   0.0000
     51    0.088590867139893    0.0    0.00     -16.638397782304     -16.638397782304     -16.638397782304     -16.549806915164   0.0000   0.0000   0.0000   0.0000
     52    0.082262743711223    0.0    0.00     -16.666233590709     -16.666233590709     -16.666233590709     -16.583970846997   0.0000   0.0000   0.0000   0.0000
     53    0.076131528833513    0.0    0.00     -16.691760352570     -16.691760352570     -16.691760352570     -16.615628823737   0.0000   0.0000   0.0000   0.0000
     54    0.070158280945105    0.0    0.00     -16.715033057231     -16.715033057231     -16.715033057231     -16.644874776286   0.0000   0.0000   0.0000   0.0000
     55    0.064314769107544    0.0    0.00     -16.736068784433     -16.736068784433     -16.736068784433     -16.671754015325   0.0000   0.0000   0.0000   0.0000
     56    0.058596709697264    0.0    0.00     -16.754910722853     -16.754910722853     -16.754910722853     -16.696314013156   0.0000   0.0000   0.0000   0.0000
     57    0.053048192651643    0.0    0.00     -16.771663850437     -16.771663850437     -16.771663850437     -16.718615657786   0.0000   0.0000   0.0000   0.0000
     58    0.047736005719815    0.0    0.00     -16.786474197578     -16.786474197578     -16.786474197578     -16.738738191859   0.0000   0.0000   0.0000   0.0000
     59    0.042739557924601    0.0    0.00     -16.799535095645     -16.799535095645     -16.799535095645     -16.756795537721   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.61 (Slater)  0.59 (Becke)  0.00 (expected)


                total energy =      -16.81105985033 Hartree a.u.
              kinetic energy =       11.65800 Hartree a.u.
        electrostatic energy =      -22.92191 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62644 Hartree a.u.
  n-l pseudopotential energy =        2.16535 Hartree a.u.
 exchange-correlation energy =       -4.08605 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -30.22  -15.20  -12.16  -10.88

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.99  -15.25  -11.80   -4.67


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.10000018997258E+00    -0.14683494427524E-01     0.40804480227949E-01
   H       0.43793755019528E-01    -0.10850944148332E-01    -0.26647000464779E-01
   H      -0.60886806017389E-02    -0.10526673644036E-02    -0.73802309328900E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.038129325913927    0.0    0.00     -16.811059850330     -16.811059850330     -16.811059850330     -16.772930524416   0.0000   0.0000   0.0000   0.0000
     61    0.033953389970423    0.0    0.00     -16.821260756651     -16.821260756651     -16.821260756651     -16.787307366681   0.0000   0.0000   0.0000   0.0000
     62    0.030243417634046    0.0    0.00     -16.830350628096     -16.830350628096     -16.830350628096     -16.800107210462   0.0000   0.0000   0.0000   0.0000
     63    0.027017650258344    0.0    0.00     -16.838539749609     -16.838539749609     -16.838539749609     -16.811522099351   0.0000   0.0000   0.0000   0.0000
     64    0.024268172325489    0.0    0.00     -16.846015518017     -16.846015518017     -16.846015518017     -16.821747345691   0.0000   0.0000   0.0000   0.0000
     65    0.021957360699863    0.0    0.00     -16.852922461874     -16.852922461874     -16.852922461874     -16.830965101174   0.0000   0.0000   0.0000   0.0000
     66    0.020033714675779    0.0    0.00     -16.859373841069     -16.859373841069     -16.859373841069     -16.839340126393   0.0000   0.0000   0.0000   0.0000
     67    0.018441775734140    0.0    0.00     -16.865458427737     -16.865458427737     -16.865458427737     -16.847016652003   0.0000   0.0000   0.0000   0.0000
     68    0.017123128059618    0.0    0.00     -16.871239110579     -16.871239110579     -16.871239110579     -16.854115982519   0.0000   0.0000   0.0000   0.0000
     69    0.016018603395386    0.0    0.00     -16.876753476077     -16.876753476077     -16.876753476077     -16.860734872682   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.34 (Slater)  0.41 (Becke)  0.00 (expected)


                total energy =      -16.88202137362 Hartree a.u.
              kinetic energy =       11.61881 Hartree a.u.
        electrostatic energy =      -22.94783 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.61929 Hartree a.u.
  n-l pseudopotential energy =        2.18113 Hartree a.u.
 exchange-correlation energy =       -4.11483 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -29.14  -14.33  -11.02   -9.55

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.27  -14.30  -10.71   -5.62


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.34142360425173E-01    -0.34733185257407E-02    -0.10894007378672E-01
   H       0.30538799893994E-01    -0.35413439483338E-02    -0.20293479663647E-01
   H      -0.92486999203029E-03     0.17922036585258E-01    -0.99011914069325E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.015074527942555    0.0    0.00     -16.882021373620     -16.882021373620     -16.882021373620     -16.866946845677   0.0000   0.0000   0.0000   0.0000
     71    0.014251169955502    0.0    0.00     -16.887057938919     -16.887057938919     -16.887057938919     -16.872806768963   0.0000   0.0000   0.0000   0.0000
     72    0.013520181800124    0.0    0.00     -16.891878415320     -16.891878415320     -16.891878415320     -16.878358233520   0.0000   0.0000   0.0000   0.0000
     73    0.012853338160731    0.0    0.00     -16.896485674567     -16.896485674567     -16.896485674567     -16.883632336406   0.0000   0.0000   0.0000   0.0000
     74    0.012224884671263    0.0    0.00     -16.900872446859     -16.900872446859     -16.900872446859     -16.888647562188   0.0000   0.0000   0.0000   0.0000
     75    0.011621048624160    0.0    0.00     -16.905036874597     -16.905036874597     -16.905036874597     -16.893415825973   0.0000   0.0000   0.0000   0.0000
     76    0.011036496076629    0.0    0.00     -16.908983558203     -16.908983558203     -16.908983558203     -16.897947062127   0.0000   0.0000   0.0000   0.0000
     77    0.010466315333328    0.0    0.00     -16.912713836370     -16.912713836370     -16.912713836370     -16.902247521037   0.0000   0.0000   0.0000   0.0000
     78    0.009905930877531    0.0    0.00     -16.916227645775     -16.916227645775     -16.916227645775     -16.906321714898   0.0000   0.0000   0.0000   0.0000
     79    0.009354374588874    0.0    0.00     -16.919527431971     -16.919527431971     -16.919527431971     -16.910173057382   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.15 (Slater)  0.24 (Becke)  0.00 (expected)


                total energy =      -16.92261889844 Hartree a.u.
              kinetic energy =       11.67524 Hartree a.u.
        electrostatic energy =      -22.98203 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62042 Hartree a.u.
  n-l pseudopotential energy =        2.15221 Hartree a.u.
 exchange-correlation energy =       -4.14762 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -28.09  -13.66  -10.01   -8.26

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.63  -13.57   -9.92   -6.24


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.29741678525155E-01    -0.22265028712978E-01    -0.12863491293973E-01
   H       0.15516929112637E-01    -0.12348079880419E-03    -0.11527488764667E-01
   H       0.27122373760513E-02     0.12497797529171E-01    -0.68682473750914E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.008812936732243    0.0    0.00     -16.922618898439     -16.922618898439     -16.922618898439     -16.913805961706   0.0000   0.0000   0.0000   0.0000
     81    0.008281291353871    0.0    0.00     -16.925505730126     -16.925505730126     -16.925505730126     -16.917224438772   0.0000   0.0000   0.0000   0.0000
     82    0.007757834485364    0.0    0.00     -16.928189782122     -16.928189782122     -16.928189782122     -16.920431947636   0.0000   0.0000   0.0000   0.0000
     83    0.007241226848525    0.0    0.00     -16.930672669504     -16.930672669504     -16.930672669504     -16.923431442656   0.0000   0.0000   0.0000   0.0000
     84    0.006730751785507    0.0    0.00     -16.932956193677     -16.932956193677     -16.932956193677     -16.926225441891   0.0000   0.0000   0.0000   0.0000
     85    0.006226732076553    0.0    0.00     -16.935042992603     -16.935042992603     -16.935042992603     -16.928816260526   0.0000   0.0000   0.0000   0.0000
     86    0.005731409136934    0.0    0.00     -16.936938185806     -16.936938185806     -16.936938185806     -16.931206776669   0.0000   0.0000   0.0000   0.0000
     87    0.005249058420744    0.0    0.00     -16.938650389827     -16.938650389827     -16.938650389827     -16.933401331407   0.0000   0.0000   0.0000   0.0000
     88    0.004785054632464    0.0    0.00     -16.940191389137     -16.940191389137     -16.940191389137     -16.935406334505   0.0000   0.0000   0.0000   0.0000
     89    0.004344412929776    0.0    0.00     -16.941574746089     -16.941574746089     -16.941574746089     -16.937230333159   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.05 (Slater)  0.13 (Becke)  0.00 (expected)


                total energy =      -16.94281400042 Hartree a.u.
              kinetic energy =       11.70588 Hartree a.u.
        electrostatic energy =      -22.99705 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62301 Hartree a.u.
  n-l pseudopotential energy =        2.13689 Hartree a.u.
 exchange-correlation energy =       -4.16551 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.55  -13.27   -9.56   -7.53

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.34  -13.21   -9.56   -6.63


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.24801840623079E-01    -0.19021446849719E-01    -0.36665495938765E-02
   H       0.13387107426599E-01     0.22139237818756E-02    -0.53227386671527E-02
   H       0.30012258017074E-02     0.12045819273639E-01    -0.30896203684557E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.003930494633205    0.0    0.00     -16.942814000418     -16.942814000418     -16.942814000418     -16.938883505785   0.0000   0.0000   0.0000   0.0000
     91    0.003544684045921    0.0    0.00     -16.943921539156     -16.943921539156     -16.943921539156     -16.940376855110   0.0000   0.0000   0.0000   0.0000
     92    0.003186944224366    0.0    0.00     -16.944908550882     -16.944908550882     -16.944908550882     -16.941721606658   0.0000   0.0000   0.0000   0.0000
     93    0.002856465871620    0.0    0.00     -16.945785262842     -16.945785262842     -16.945785262842     -16.942928796970   0.0000   0.0000   0.0000   0.0000
     94    0.002552092586763    0.0    0.00     -16.946561175471     -16.946561175471     -16.946561175471     -16.944009082885   0.0000   0.0000   0.0000   0.0000
     95    0.002272558285565    0.0    0.00     -16.947245188558     -16.947245188558     -16.947245188558     -16.944972630273   0.0000   0.0000   0.0000   0.0000
     96    0.002016596581367    0.0    0.00     -16.947845710659     -16.947845710659     -16.947845710659     -16.945829114078   0.0000   0.0000   0.0000   0.0000
     97    0.001782957787405    0.0    0.00     -16.948370674216     -16.948370674216     -16.948370674216     -16.946587716429   0.0000   0.0000   0.0000   0.0000
     98    0.001570433824381    0.0    0.00     -16.948827560299     -16.948827560299     -16.948827560299     -16.947257126475   0.0000   0.0000   0.0000   0.0000
     99    0.001377886031782    0.0    0.00     -16.949223456341     -16.949223456341     -16.949223456341     -16.947845570309   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   100

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.01 (Slater)  0.06 (Becke)  0.00 (expected)


                total energy =      -16.94956509439 Hartree a.u.
              kinetic energy =       11.71062 Hartree a.u.
        electrostatic energy =      -22.99920 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62261 Hartree a.u.
  n-l pseudopotential energy =        2.13322 Hartree a.u.
 exchange-correlation energy =       -4.17159 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.35  -13.11   -9.42   -7.21

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.09   -9.43   -6.85


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20482437095648E-01    -0.17678225091755E-01    -0.43130498517988E-02
   H       0.13562653760662E-01     0.27075327114706E-02    -0.24751062546719E-02
   H       0.27650656956269E-02     0.13114695366459E-01    -0.14947452119575E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    100    0.001204251153541    0.0    0.00     -16.949565094392     -16.949565094392     -16.949565094392     -16.948360843238   0.0000   0.0000   0.0000   0.0000
    101    0.001048506804125    0.0    0.00     -16.949858812549     -16.949858812549     -16.949858812549     -16.948810305745   0.0000   0.0000   0.0000   0.0000
    102    0.000909612273734    0.0    0.00     -16.950110519721     -16.950110519721     -16.950110519721     -16.949200907447   0.0000   0.0000   0.0000   0.0000
    103    0.000786480613556    0.0    0.00     -16.950325649569     -16.950325649569     -16.950325649569     -16.949539168955   0.0000   0.0000   0.0000   0.0000
    104    0.000677962865866    0.0    0.00     -16.950509150411     -16.950509150411     -16.950509150411     -16.949831187545   0.0000   0.0000   0.0000   0.0000
    105    0.000582851865799    0.0    0.00     -16.950665420320     -16.950665420320     -16.950665420320     -16.950082568454   0.0000   0.0000   0.0000   0.0000
    106    0.000499903386769    0.0    0.00     -16.950798322782     -16.950798322782     -16.950798322782     -16.950298419395   0.0000   0.0000   0.0000   0.0000
    107    0.000427870647264    0.0    0.00     -16.950911222036     -16.950911222036     -16.950911222036     -16.950483351389   0.0000   0.0000   0.0000   0.0000
    108    0.000365547003360    0.0    0.00     -16.951007033775     -16.951007033775     -16.951007033775     -16.950641486771   0.0000   0.0000   0.0000   0.0000
    109    0.000311796554368    0.0    0.00     -16.951088270028     -16.951088270028     -16.951088270028     -16.950776473473   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   110

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.03 (Becke)  0.00 (expected)


                total energy =      -16.95115710101 Hartree a.u.
              kinetic energy =       11.71214 Hartree a.u.
        electrostatic energy =      -22.99951 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62215 Hartree a.u.
  n-l pseudopotential energy =        2.13173 Hartree a.u.
 exchange-correlation energy =       -4.17336 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.29  -13.06   -9.38   -7.08

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.25  -13.06   -9.38   -6.95


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18472882313113E-01    -0.17488236436703E-01    -0.40374897711962E-02
   H       0.13346660817509E-01     0.29065409125060E-02    -0.10239485858452E-02
   H       0.28727463843925E-02     0.13178107289481E-01    -0.82133868528084E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    110    0.000265571493860    0.0    0.00     -16.951157101006     -16.951157101006     -16.951157101006     -16.950891529512   0.0000   0.0000   0.0000   0.0000
    111    0.000225919150862    0.0    0.00     -16.951215379305     -16.951215379305     -16.951215379305     -16.950989460154   0.0000   0.0000   0.0000   0.0000
    112    0.000191983803184    0.0    0.00     -16.951264710126     -16.951264710126     -16.951264710126     -16.951072726323   0.0000   0.0000   0.0000   0.0000
    113    0.000163001660873    0.0    0.00     -16.951306455751     -16.951306455751     -16.951306455751     -16.951143454090   0.0000   0.0000   0.0000   0.0000
    114    0.000138295315178    0.0    0.00     -16.951341776151     -16.951341776151     -16.951341776151     -16.951203480836   0.0000   0.0000   0.0000   0.0000
    115    0.000117267061859    0.0    0.00     -16.951371665329     -16.951371665329     -16.951371665329     -16.951254398267   0.0000   0.0000   0.0000   0.0000
    116    0.000099392550773    0.0    0.00     -16.951396953833     -16.951396953833     -16.951396953833     -16.951297561282   0.0000   0.0000   0.0000   0.0000
    117    0.000084214688492    0.0    0.00     -16.951418356207     -16.951418356207     -16.951418356207     -16.951334141519   0.0000   0.0000   0.0000   0.0000
    118    0.000071337551679    0.0    0.00     -16.951436469746     -16.951436469746     -16.951436469746     -16.951365132195   0.0000   0.0000   0.0000   0.0000
    119    0.000060420218042    0.0    0.00     -16.951451798995     -16.951451798995     -16.951451798995     -16.951391378777   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   120

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.02 (Becke)  0.00 (expected)


                total energy =      -16.95146478321 Hartree a.u.
              kinetic energy =       11.71381 Hartree a.u.
        electrostatic energy =      -23.00037 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62254 Hartree a.u.
  n-l pseudopotential energy =        2.13149 Hartree a.u.
 exchange-correlation energy =       -4.17384 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.27  -13.05   -9.37   -7.03

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.25  -13.05   -9.37   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18041914547677E-01    -0.17941633055919E-01    -0.13657483961738E-02
   H       0.13167683937968E-01     0.29276299837268E-02    -0.28527609640824E-03
   H       0.29117620641969E-02     0.13127024558929E-01    -0.31475246167039E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    120    0.000051170334999    0.0    0.00     -16.951464783209     -16.951464783209     -16.951464783209     -16.951413612874   0.0000   0.0000   0.0000   0.0000
    121    0.000043337927358    0.0    0.00     -16.951475780475     -16.951475780475     -16.951475780475     -16.951432442547   0.0000   0.0000   0.0000   0.0000
    122    0.000036709473662    0.0    0.00     -16.951485100552     -16.951485100552     -16.951485100552     -16.951448391079   0.0000   0.0000   0.0000   0.0000
    123    0.000031102626803    0.0    0.00     -16.951493003817     -16.951493003817     -16.951493003817     -16.951461901190   0.0000   0.0000   0.0000   0.0000
    124    0.000026361665976    0.0    0.00     -16.951499713600     -16.951499713600     -16.951499713600     -16.951473351934   0.0000   0.0000   0.0000   0.0000
    125    0.000022353650141    0.0    0.00     -16.951505414961     -16.951505414961     -16.951505414961     -16.951483061311   0.0000   0.0000   0.0000   0.0000
    126    0.000018965264056    0.0    0.00     -16.951510261671     -16.951510261671     -16.951510261671     -16.951491296407   0.0000   0.0000   0.0000   0.0000
    127    0.000016100094190    0.0    0.00     -16.951514387040     -16.951514387040     -16.951514387040     -16.951498286945   0.0000   0.0000   0.0000   0.0000
    128    0.000013676356447    0.0    0.00     -16.951517898339     -16.951517898339     -16.951517898339     -16.951504221983   0.0000   0.0000   0.0000   0.0000
    129    0.000011624892159    0.0    0.00     -16.951520888928     -16.951520888928     -16.951520888928     -16.951509264036   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   130

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.01 (Becke)  0.00 (expected)


                total energy =      -16.95152344022 Hartree a.u.
              kinetic energy =       11.71402 Hartree a.u.
        electrostatic energy =      -23.00045 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62257 Hartree a.u.
  n-l pseudopotential energy =        2.13134 Hartree a.u.
 exchange-correlation energy =       -4.17386 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.00


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18048746833808E-01    -0.18041936656818E-01     0.34175422712013E-04
   H       0.13115586177057E-01     0.29083084003951E-02    -0.27185045767336E-04
   H       0.29100070130211E-02     0.13116037475666E-01    -0.62027733732375E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    130    0.000009887367352    0.0    0.00     -16.951523440219     -16.951523440219     -16.951523440219     -16.951513552851   0.0000   0.0000   0.0000   0.0000
    131    0.000008414680832    0.0    0.00     -16.951525616777     -16.951525616777     -16.951525616777     -16.951517202096   0.0000   0.0000   0.0000   0.0000
    132    0.000007165521515    0.0    0.00     -16.951527473672     -16.951527473672     -16.951527473672     -16.951520308151   0.0000   0.0000   0.0000   0.0000
    133    0.000006105150466    0.0    0.00     -16.951529059874     -16.951529059874     -16.951529059874     -16.951522954724   0.0000   0.0000   0.0000   0.0000
    134    0.000005204358635    0.0    0.00     -16.951530414377     -16.951530414377     -16.951530414377     -16.951525210019   0.0000   0.0000   0.0000   0.0000
    135    0.000004438567534    0.0    0.00     -16.951531571555     -16.951531571555     -16.951531571555     -16.951527132987   0.0000   0.0000   0.0000   0.0000
    136    0.000003787088956    0.0    0.00     -16.951532560591     -16.951532560591     -16.951532560591     -16.951528773502   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.378709D-05  0.1D-03  0.989036D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:   137

 Spin contamination: s(s+1)= 0.00 (Slater)  0.00 (Becke)  0.00 (expected)


                total energy =      -16.95153340550 Hartree a.u.
              kinetic energy =       11.71397 Hartree a.u.
        electrostatic energy =      -23.00042 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62256 Hartree a.u.
  n-l pseudopotential energy =        2.13134 Hartree a.u.
 exchange-correlation energy =       -4.17386 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18043135199572E-01    -0.18043094734875E-01     0.82817035447052E-04
   H       0.13107877258462E-01     0.29071868108393E-02     0.12299712081886E-04
   H       0.29103762519019E-02     0.13110782535186E-01    -0.53939815985186E-05



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
    137    0.000003232500836    0.0    0.00     -16.951533405500     -16.951533405500     -16.951533405500     -16.951530172999   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.323250D-05  0.1D-03  0.844909D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.01s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.52768       0.52768 (AU)
   ekin          :       10.68389      10.68389 (AU)
   epot          :      -27.80657     -27.80657 (AU)
   total energy  :      -14.79287     -14.79287 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :      -14.79287     -14.79287 (AU)
   econs         :      -14.79287     -14.79287 (AU)
   pressure      :        0.00000       0.00000 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      0.69s CPU      0.71s WALL (       1 calls)
     main_loop    :     23.78s CPU     24.83s WALL (     137 calls)
     cpr_total    :     23.79s CPU     24.84s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     23.79s CPU     24.84s WALL (     137 calls)
     move_electro :     23.64s CPU     24.67s WALL (     137 calls)

     Called by move_electrons:
     rhoofr       :      4.49s CPU      4.56s WALL (     138 calls)
     vofrho       :     14.59s CPU     15.53s WALL (     138 calls)
     dforce       :      4.62s CPU      4.66s WALL (     552 calls)
     calphi       :      0.02s CPU      0.02s WALL (     138 calls)
     nlfl         :      0.00s CPU      0.00s WALL (     138 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (     276 calls)
     rsg          :      0.01s CPU      0.01s WALL (     276 calls)
     rhoset       :      0.02s CPU      0.02s WALL (     276 calls)
     sigset       :      0.01s CPU      0.01s WALL (     276 calls)
     tauset       :      0.01s CPU      0.01s WALL (     276 calls)
     ortho        :      0.06s CPU      0.06s WALL (     138 calls)
     updatc       :      0.01s CPU      0.01s WALL (     138 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (     138 calls)
     nlfq         :      0.02s CPU      0.02s WALL (     138 calls)
     nlsm1        :      0.00s CPU      0.00s WALL (     139 calls)
     nlsm2        :      0.02s CPU      0.02s WALL (     138 calls)
     fft          :      3.61s CPU      3.75s WALL (    2071 calls)
     ffts         :      0.96s CPU      0.97s WALL (     552 calls)
     fftw         :      5.62s CPU      5.68s WALL (    6624 calls)
     fft_scatt_xy :      2.47s CPU      2.51s WALL (    9247 calls)
     fft_scatt_yz :      2.95s CPU      2.99s WALL (    9247 calls)
     betagx       :      0.21s CPU      0.21s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      0.52s CPU      0.52s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.00s CPU      0.00s WALL (       1 calls)
     from_scratch :      0.16s CPU      0.18s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.00s CPU      0.00s WALL (     139 calls)
     exch_corr    :     12.64s CPU     13.49s WALL (     138 calls)


     CP           :     24.57s CPU     25.64s WALL


   This run was terminated on:  12:14:43   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
