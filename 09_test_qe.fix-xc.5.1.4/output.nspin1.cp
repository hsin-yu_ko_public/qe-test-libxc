
     Program CP v.6.7GPU starts on  6Jun2021 at 12:14: 3 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
         "P. Giannozzi et al., J. Chem. Phys. 152 154105 (2020);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
     Message from routine scan_end:
     No INFO block end statement, possibly corrupted file
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     WARNING: libxc functional with ID  263 depends
      on external parameters: check the user_guide of
      QE if you need to modify them or to check their
      default values.

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Number of Electrons=     8, of States =     4
   Occupation numbers :
   2.00 2.00 2.00 2.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   internal stress tensor calculated
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249

     Using Pencil Decomposition


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

     Subspace diagonalization in iterative solution of the eigenvalue problem:
     a serial algorithm will be used


   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   nbnd =     4
    2.00 2.00 2.00 2.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)      134.62224      0

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.976566965114075    0.0    0.00      10.642034842777      10.642034842777      10.642034842777      11.618601807891   0.0000   0.0000   0.0000   0.0000
      2    2.768909244316808    0.0    0.00       8.091717363674       8.091717363674       8.091717363674      10.860626607991   0.0000   0.0000   0.0000   0.0000
      3    5.066052757826499    0.0    0.00       4.287968920567       4.287968920567       4.287968920567       9.354021678393   0.0000   0.0000   0.0000   0.0000
      4    7.567941514537075    0.0    0.00      -0.677739498548      -0.677739498548      -0.677739498548       6.890202015989   0.0000   0.0000   0.0000   0.0000
      5    8.977433067914937    0.0    0.00      -5.692814829294      -5.692814829294      -5.692814829294       3.284618238621   0.0000   0.0000   0.0000   0.0000
      6    7.554421252116249    0.0    0.00      -7.503536445757      -7.503536445757      -7.503536445757       0.050884806359   0.0000   0.0000   0.0000   0.0000
      7    4.658184311523836    0.0    0.00      -6.735444237537      -6.735444237537      -6.735444237537      -2.077259926013   0.0000   0.0000   0.0000   0.0000
      8    2.924499483201120    0.0    0.00      -6.447647791954      -6.447647791954      -6.447647791954      -3.523148308753   0.0000   0.0000   0.0000   0.0000
      9    2.653259525291091    0.0    0.00      -7.344624634580      -7.344624634580      -7.344624634580      -4.691365109289   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       37.19795     10
     Pressure of Nuclei (GPa)             0.00000     10
     Pressure Total (GPa)            37.19795     10


                total energy =       -8.82441279571 Hartree a.u.
              kinetic energy =        8.00419 Hartree a.u.
        electrostatic energy =      -15.96150 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.36136 Hartree a.u.
  n-l pseudopotential energy =        3.94856 Hartree a.u.
 exchange-correlation energy =       -2.45431 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -78.97  -17.43   -7.60   -1.26


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       49.55626893        -3.78803098        -1.13911098
       -3.78803098        42.25160521        -0.61837295
       -1.13911098        -0.61837295        19.78598278
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.79060086424262E+00     0.89196972727373E+00     0.21854172723695E+00
   H       0.96502993122548E+00    -0.18157631339444E+00     0.40982310021123E-02
   H      -0.23142632782410E+00     0.95623541655043E+00     0.28179857085022E+00



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.921745780617082    0.0    0.00      -8.824412795711      -8.824412795711      -8.824412795711      -5.902667015094   0.0000   0.0000   0.0000   0.0000
     11    2.951109137228798    0.0    0.00     -10.066213221571     -10.066213221571     -10.066213221571      -7.115104084342   0.0000   0.0000   0.0000   0.0000
     12    2.604414114750524    0.0    0.00     -10.804063603373     -10.804063603373     -10.804063603373      -8.199649488623   0.0000   0.0000   0.0000   0.0000
     13    2.166325509966065    0.0    0.00     -11.289389139017     -11.289389139017     -11.289389139017      -9.123063629051   0.0000   0.0000   0.0000   0.0000
     14    1.842508293133441    0.0    0.00     -11.757373184545     -11.757373184545     -11.757373184545      -9.914864891412   0.0000   0.0000   0.0000   0.0000
     15    1.637426584307997    0.0    0.00     -12.251073727043     -12.251073727043     -12.251073727043     -10.613647142735   0.0000   0.0000   0.0000   0.0000
     16    1.483825867135163    0.0    0.00     -12.725357997842     -12.725357997842     -12.725357997842     -11.241532130707   0.0000   0.0000   0.0000   0.0000
     17    1.340521529061403    0.0    0.00     -13.146690985561     -13.146690985561     -13.146690985561     -11.806169456500   0.0000   0.0000   0.0000   0.0000
     18    1.206185281959566    0.0    0.00     -13.519018916616     -13.519018916616     -13.519018916616     -12.312833634656   0.0000   0.0000   0.0000   0.0000
     19    1.092407501148654    0.0    0.00     -13.862686105575     -13.862686105575     -13.862686105575     -12.770278604426   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)        7.16359     20
     Pressure of Nuclei (GPa)             0.00000     20
     Pressure Total (GPa)             7.16359     20


                total energy =      -14.18999142381 Hartree a.u.
              kinetic energy =        9.87237 Hartree a.u.
        electrostatic energy =      -20.72792 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.07474 Hartree a.u.
  n-l pseudopotential energy =        3.13050 Hartree a.u.
 exchange-correlation energy =       -3.39020 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -43.03  -26.72   -8.54   -3.59


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       14.75987437       -12.77715286         1.93396132
      -12.77715286        10.11895374         3.45620538
        1.93396132         3.45620538        -3.38804511
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18597853582781E+00     0.17980838529470E+00     0.39778613652761E+00
   H       0.46628871130504E+00    -0.49226216267314E+00    -0.85162824254451E-01
   H      -0.27132083725880E+00     0.24632311994737E+00    -0.44014981491614E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.001999541068420    0.0    0.00     -14.189991423813     -14.189991423813     -14.189991423813     -13.187991882744   0.0000   0.0000   0.0000   0.0000
     21    0.927768450228188    0.0    0.00     -14.501492521230     -14.501492521230     -14.501492521230     -13.573724071002   0.0000   0.0000   0.0000   0.0000
     22    0.859467290812596    0.0    0.00     -14.790700735928     -14.790700735928     -14.790700735928     -13.931233445115   0.0000   0.0000   0.0000   0.0000
     23    0.789431275241202    0.0    0.00     -15.050457147468     -15.050457147468     -15.050457147468     -14.261025872227   0.0000   0.0000   0.0000   0.0000
     24    0.714663551270504    0.0    0.00     -15.276340356792     -15.276340356792     -15.276340356792     -14.561676805522   0.0000   0.0000   0.0000   0.0000
     25    0.635832217674028    0.0    0.00     -15.467183457340     -15.467183457340     -15.467183457340     -14.831351239666   0.0000   0.0000   0.0000   0.0000
     26    0.555535681268679    0.0    0.00     -15.624550596290     -15.624550596290     -15.624550596290     -15.069014915021   0.0000   0.0000   0.0000   0.0000
     27    0.477391388744929    0.0    0.00     -15.751953022240     -15.751953022240     -15.751953022240     -15.274561633495   0.0000   0.0000   0.0000   0.0000
     28    0.405934146583465    0.0    0.00     -15.856060565068     -15.856060565068     -15.856060565068     -15.450126418485   0.0000   0.0000   0.0000   0.0000
     29    0.344750132826024    0.0    0.00     -15.943843593440     -15.943843593440     -15.943843593440     -15.599093460614   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -0.34870     30
     Pressure of Nuclei (GPa)             0.00000     30
     Pressure Total (GPa)            -0.34870     30


                total energy =      -16.02199113787 Hartree a.u.
              kinetic energy =       12.11283 Hartree a.u.
        electrostatic energy =      -22.71589 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.65252 Hartree a.u.
  n-l pseudopotential energy =        2.12735 Hartree a.u.
 exchange-correlation energy =       -3.89376 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -32.47  -19.20  -12.84   -4.42


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
        5.55591376        -6.97875907        -0.90468280
       -6.97875907         1.24222157        -0.19481689
       -0.90468280        -0.19481689        -7.84422684
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.52262967921282E+00    -0.33971096712791E+00    -0.11747775230725E+00
   H       0.21328405934752E+00    -0.14714284701010E+00    -0.20435692641667E-01
   H      -0.10963352808133E+00     0.13679991590682E+00    -0.49499029981132E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.295909590603224    0.0    0.00     -16.021991137874     -16.021991137874     -16.021991137874     -15.726081547271   0.0000   0.0000   0.0000   0.0000
     31    0.260234566744361    0.0    0.00     -16.096627723321     -16.096627723321     -16.096627723321     -15.836393156577   0.0000   0.0000   0.0000   0.0000
     32    0.236837729792708    0.0    0.00     -16.171916080930     -16.171916080930     -16.171916080930     -15.935078351137   0.0000   0.0000   0.0000   0.0000
     33    0.223915150870093    0.0    0.00     -16.250685116166     -16.250685116166     -16.250685116166     -16.026769965296   0.0000   0.0000   0.0000   0.0000
     34    0.218870719276263    0.0    0.00     -16.334035132124     -16.334035132124     -16.334035132124     -16.115164412847   0.0000   0.0000   0.0000   0.0000
     35    0.218114188089632    0.0    0.00     -16.420827024257     -16.420827024257     -16.420827024257     -16.202712836167   0.0000   0.0000   0.0000   0.0000
     36    0.217959703214864    0.0    0.00     -16.508083835165     -16.508083835165     -16.508083835165     -16.290124131951   0.0000   0.0000   0.0000   0.0000
     37    0.215542598005473    0.0    0.00     -16.592709112657     -16.592709112657     -16.592709112657     -16.377166514652   0.0000   0.0000   0.0000   0.0000
     38    0.208870865686165    0.0    0.00     -16.671278197219     -16.671278197219     -16.671278197219     -16.462407331533   0.0000   0.0000   0.0000   0.0000
     39    0.196790848724351    0.0    0.00     -16.740639505363     -16.740639505363     -16.740639505363     -16.543848656639   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -7.47157     40
     Pressure of Nuclei (GPa)             0.00000     40
     Pressure Total (GPa)            -7.47157     40


                total energy =      -16.79853391896 Hartree a.u.
              kinetic energy =       12.04304 Hartree a.u.
        electrostatic energy =      -23.02989 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63042 Hartree a.u.
  n-l pseudopotential energy =        1.97136 Hartree a.u.
 exchange-correlation energy =       -4.15262 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.67  -13.64  -10.08   -7.93


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -8.16987628        -0.38095373        -0.00021057
       -0.38095373        -7.07724032        -0.21728962
       -0.00021057        -0.21728962        -7.16760038
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13855913773040E+00    -0.99152211847297E-01     0.86090293687478E-02
   H      -0.59789110977231E-01    -0.17329377503388E-01    -0.61323134580368E-02
   H      -0.23617133021231E-01    -0.45405389132300E-01    -0.30852319458082E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.179324113370434    0.0    0.00     -16.798533918961     -16.798533918961     -16.798533918961     -16.619209805591   0.0000   0.0000   0.0000   0.0000
     41    0.157527365486792    0.0    0.00     -16.844087966505     -16.844087966505     -16.844087966505     -16.686560601019   0.0000   0.0000   0.0000   0.0000
     42    0.133098586704962    0.0    0.00     -16.877610607500     -16.877610607500     -16.877610607500     -16.744512020795   0.0000   0.0000   0.0000   0.0000
     43    0.108063681928435    0.0    0.00     -16.900475390569     -16.900475390569     -16.900475390569     -16.792411708640   0.0000   0.0000   0.0000   0.0000
     44    0.084465807481627    0.0    0.00     -16.914972644723     -16.914972644723     -16.914972644723     -16.830506837242   0.0000   0.0000   0.0000   0.0000
     45    0.063963581097965    0.0    0.00     -16.923773521995     -16.923773521995     -16.923773521995     -16.859809940898   0.0000   0.0000   0.0000   0.0000
     46    0.047375863048687    0.0    0.00     -16.929154878254     -16.929154878254     -16.929154878254     -16.881779015205   0.0000   0.0000   0.0000   0.0000
     47    0.034695945211958    0.0    0.00     -16.932688817305     -16.932688817305     -16.932688817305     -16.897992872093   0.0000   0.0000   0.0000   0.0000
     48    0.025369880032665    0.0    0.00     -16.935254977606     -16.935254977606     -16.935254977606     -16.909885097574   0.0000   0.0000   0.0000   0.0000
     49    0.018619179596010    0.0    0.00     -16.937234783232     -16.937234783232     -16.937234783232     -16.918615603636   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -7.65573     50
     Pressure of Nuclei (GPa)             0.00000     50
     Pressure Total (GPa)            -7.65573     50


                total energy =      -16.93877341465 Hartree a.u.
              kinetic energy =       11.61675 Hartree a.u.
        electrostatic energy =      -22.98751 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63674 Hartree a.u.
  n-l pseudopotential energy =        2.22039 Hartree a.u.
 exchange-correlation energy =       -4.15166 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.80  -13.29  -10.07   -7.32


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -6.39132486        -0.04808944         0.07375135
       -0.04808944        -7.00748088         0.06145649
        0.07375135         0.06145649        -9.56838570
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.66019854952804E-01     0.35900796782584E-01     0.85669240494840E-02
   H       0.48635232800327E-01    -0.96284839011800E-02    -0.10567373976174E-02
   H       0.55006599435060E-03     0.40958930209481E-01    -0.23153743000140E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.013721755945404    0.0    0.00     -16.938773414649     -16.938773414649     -16.938773414649     -16.925051658703   0.0000   0.0000   0.0000   0.0000
     51    0.010165451905696    0.0    0.00     -16.939968751896     -16.939968751896     -16.939968751896     -16.929803299991   0.0000   0.0000   0.0000   0.0000
     52    0.007629168987556    0.0    0.00     -16.940953115759     -16.940953115759     -16.940953115759     -16.933323946771   0.0000   0.0000   0.0000   0.0000
     53    0.005885127081753    0.0    0.00     -16.941873717931     -16.941873717931     -16.941873717931     -16.935988590850   0.0000   0.0000   0.0000   0.0000
     54    0.004749286461431    0.0    0.00     -16.942838651533     -16.942838651533     -16.942838651533     -16.938089365071   0.0000   0.0000   0.0000   0.0000
     55    0.004049033469045    0.0    0.00     -16.943884354952     -16.943884354952     -16.943884354952     -16.939835321483   0.0000   0.0000   0.0000   0.0000
     56    0.003617266601522    0.0    0.00     -16.944982571794     -16.944982571794     -16.944982571794     -16.941365305192   0.0000   0.0000   0.0000   0.0000
     57    0.003318732444936    0.0    0.00     -16.946071371157     -16.946071371157     -16.946071371157     -16.942752638712   0.0000   0.0000   0.0000   0.0000
     58    0.003060621773277    0.0    0.00     -16.947087849780     -16.947087849780     -16.947087849780     -16.944027228007   0.0000   0.0000   0.0000   0.0000
     59    0.002795404631837    0.0    0.00     -16.947990416570     -16.947990416570     -16.947990416570     -16.945195011938   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.39893     60
     Pressure of Nuclei (GPa)             0.00000     60
     Pressure Total (GPa)            -8.39893     60


                total energy =      -16.94876364322 Hartree a.u.
              kinetic energy =       11.67560 Hartree a.u.
        electrostatic energy =      -22.98002 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60914 Hartree a.u.
  n-l pseudopotential energy =        2.13086 Hartree a.u.
 exchange-correlation energy =       -4.16606 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.38  -13.22   -9.33   -7.11


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.87914967        -0.41155994        -0.03985147
       -0.41155994        -7.71134933         0.01197504
       -0.03985147         0.01197504        -9.60627828
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.44893884917160E-01    -0.29698346729454E-01    -0.44025506095042E-02
   H       0.14054953276934E-01     0.65068573853918E-02    -0.25222396541600E-03
   H       0.40124425596861E-02     0.14718166164348E-01    -0.21448666000214E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.002511428523858    0.0    0.00     -16.948763643220     -16.948763643220     -16.948763643220     -16.946252214696   0.0000   0.0000   0.0000   0.0000
     61    0.002215237594051    0.0    0.00     -16.949409973019     -16.949409973019     -16.949409973019     -16.947194735425   0.0000   0.0000   0.0000   0.0000
     62    0.001917420373397    0.0    0.00     -16.949937886692     -16.949937886692     -16.949937886692     -16.948020466319   0.0000   0.0000   0.0000   0.0000
     63    0.001625504015016    0.0    0.00     -16.950355580550     -16.950355580550     -16.950355580550     -16.948730076535   0.0000   0.0000   0.0000   0.0000
     64    0.001341003751179    0.0    0.00     -16.950670590260     -16.950670590260     -16.950670590260     -16.949329586509   0.0000   0.0000   0.0000   0.0000
     65    0.001069456524701    0.0    0.00     -16.950893334613     -16.950893334613     -16.950893334613     -16.949823878089   0.0000   0.0000   0.0000   0.0000
     66    0.000825740940272    0.0    0.00     -16.951041140038     -16.951041140038     -16.951041140038     -16.950215399097   0.0000   0.0000   0.0000   0.0000
     67    0.000622714521463    0.0    0.00     -16.951136312198     -16.951136312198     -16.951136312198     -16.950513597676   0.0000   0.0000   0.0000   0.0000
     68    0.000464824799403    0.0    0.00     -16.951199703864     -16.951199703864     -16.951199703864     -16.950734879065   0.0000   0.0000   0.0000   0.0000
     69    0.000348624343648    0.0    0.00     -16.951246335313     -16.951246335313     -16.951246335313     -16.950897710970   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.10285     70
     Pressure of Nuclei (GPa)             0.00000     70
     Pressure Total (GPa)            -8.10285     70


                total energy =      -16.95128420605 Hartree a.u.
              kinetic energy =       11.73463 Hartree a.u.
        electrostatic energy =      -23.01064 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62892 Hartree a.u.
  n-l pseudopotential energy =        2.13029 Hartree a.u.
 exchange-correlation energy =       -4.17664 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.21  -13.03   -9.35   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.52963333        -0.21627348         0.01644128
       -0.21627348        -7.55501395        -0.00568229
        0.01644128        -0.00568229        -9.22391553
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.88204210787097E-02    -0.12626673533908E-01     0.16839720089483E-02
   H       0.11648794917235E-01     0.30057885701388E-02     0.35459622433332E-04
   H       0.27438902970038E-02     0.11840942110806E-01     0.22484115958972E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.000265665515974    0.0    0.00     -16.951284206045     -16.951284206045     -16.951284206045     -16.951018540530   0.0000   0.0000   0.0000   0.0000
     71    0.000206454755025    0.0    0.00     -16.951316163947     -16.951316163947     -16.951316163947     -16.951109709192   0.0000   0.0000   0.0000   0.0000
     72    0.000162727082232    0.0    0.00     -16.951342385988     -16.951342385988     -16.951342385988     -16.951179658906   0.0000   0.0000   0.0000   0.0000
     73    0.000128873225120    0.0    0.00     -16.951362968466     -16.951362968466     -16.951362968466     -16.951234095241   0.0000   0.0000   0.0000   0.0000
     74    0.000102099036937    0.0    0.00     -16.951379046774     -16.951379046774     -16.951379046774     -16.951276947737   0.0000   0.0000   0.0000   0.0000
     75    0.000081362785122    0.0    0.00     -16.951392595909     -16.951392595909     -16.951392595909     -16.951311233124   0.0000   0.0000   0.0000   0.0000
     76    0.000066084543752    0.0    0.00     -16.951405522009     -16.951405522009     -16.951405522009     -16.951339437465   0.0000   0.0000   0.0000   0.0000
     77    0.000055338430388    0.0    0.00     -16.951418885197     -16.951418885197     -16.951418885197     -16.951363546767   0.0000   0.0000   0.0000   0.0000
     78    0.000047897307268    0.0    0.00     -16.951432811384     -16.951432811384     -16.951432811384     -16.951384914076   0.0000   0.0000   0.0000   0.0000
     79    0.000042690550179    0.0    0.00     -16.951446804211     -16.951446804211     -16.951446804211     -16.951404113661   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.23230     80
     Pressure of Nuclei (GPa)             0.00000     80
     Pressure Total (GPa)            -8.23230     80


                total energy =      -16.95146026178 Hartree a.u.
              kinetic energy =       11.71068 Hartree a.u.
        electrostatic energy =      -22.99812 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62109 Hartree a.u.
  n-l pseudopotential energy =        2.13051 Hartree a.u.
 exchange-correlation energy =       -4.17344 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.27  -13.04   -9.38   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.64173089        -0.23506490        -0.00237005
       -0.23506490        -7.64476924         0.00284928
       -0.00237005         0.00284928        -9.41040144
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.21414625244748E-01    -0.21004063460840E-01     0.21372384390863E-05
   H       0.13150396203038E-01     0.24700764378654E-02     0.14097425746198E-03
   H       0.27847319000038E-02     0.13034586388060E-01     0.12383107841617E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.000038748550856    0.0    0.00     -16.951460261777     -16.951460261777     -16.951460261777     -16.951421513226   0.0000   0.0000   0.0000   0.0000
     81    0.000035305387137    0.0    0.00     -16.951472528726     -16.951472528726     -16.951472528726     -16.951437223339   0.0000   0.0000   0.0000   0.0000
     82    0.000031896336462    0.0    0.00     -16.951483214627     -16.951483214627     -16.951483214627     -16.951451318291   0.0000   0.0000   0.0000   0.0000
     83    0.000028353460866    0.0    0.00     -16.951492151954     -16.951492151954     -16.951492151954     -16.951463798494   0.0000   0.0000   0.0000   0.0000
     84    0.000024708364857    0.0    0.00     -16.951499377344     -16.951499377344     -16.951499377344     -16.951474668979   0.0000   0.0000   0.0000   0.0000
     85    0.000021065372476    0.0    0.00     -16.951505007567     -16.951505007567     -16.951505007567     -16.951483942194   0.0000   0.0000   0.0000   0.0000
     86    0.000017568321975    0.0    0.00     -16.951509193710     -16.951509193710     -16.951509193710     -16.951491625388   0.0000   0.0000   0.0000   0.0000
     87    0.000014295495239    0.0    0.00     -16.951512133151     -16.951512133151     -16.951512133151     -16.951497837656   0.0000   0.0000   0.0000   0.0000
     88    0.000011296726984    0.0    0.00     -16.951514023948     -16.951514023948     -16.951514023948     -16.951502727221   0.0000   0.0000   0.0000   0.0000
     89    0.000008646037883    0.0    0.00     -16.951515053931     -16.951515053931     -16.951515053931     -16.951506407893   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.20638     90
     Pressure of Nuclei (GPa)             0.00000     90
     Pressure Total (GPa)            -8.20638     90


                total energy =      -16.95151552261 Hartree a.u.
              kinetic energy =       11.71445 Hartree a.u.
        electrostatic energy =      -23.00043 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62273 Hartree a.u.
  n-l pseudopotential energy =        2.13073 Hartree a.u.
 exchange-correlation energy =       -4.17353 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.06   -9.38   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.62476469        -0.24127294        -0.00163420
       -0.24127294        -7.62020325        -0.00165069
       -0.00163420        -0.00165069        -9.37417497
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17044985123347E-01    -0.16977574825458E-01    -0.23021228688027E-03
   H       0.13060423544921E-01     0.29436648525486E-02    -0.31625842714527E-04
   H       0.28802242652952E-02     0.13104630269080E-01    -0.60525429125794E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.000006425633644    0.0    0.00     -16.951515522613     -16.951515522613     -16.951515522613     -16.951509096980   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.642563D-05  0.1D-03  0.468682D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:    91
     Pressure of Nuclei (GPa)             0.00000     91
     Pressure Total (GPa)            -8.20868     91


                total energy =      -16.95151568947 Hartree a.u.
              kinetic energy =       11.71421 Hartree a.u.
        electrostatic energy =      -23.00026 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62259 Hartree a.u.
  n-l pseudopotential energy =        2.13064 Hartree a.u.
 exchange-correlation energy =       -4.17351 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.06   -9.38   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.62695738        -0.24262223        -0.00241751
       -0.24262223        -7.62364930        -0.00112274
       -0.00241751        -0.00112274        -9.37541869
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17264215077349E-01    -0.16931787369464E-01    -0.15175928739548E-03
   H       0.13049048425711E-01     0.29566803000799E-02    -0.32639712799449E-04
   H       0.28695821615646E-02     0.13103468830263E-01    -0.59701701010746E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
     91    0.000004681417119    0.0    0.00     -16.951515689473     -16.951515689473     -16.951515689473     -16.951511008055   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.468142D-05  0.1D-03  0.166859D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.00s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.79478       0.79478 (AU)
   ekin          :       10.87268      10.87268 (AU)
   epot          :      -27.72609     -27.72609 (AU)
   total energy  :      -14.57970     -14.57970 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :      -14.57970     -14.57970 (AU)
   econs         :      -14.57970     -14.57970 (AU)
   pressure      :        2.20477       2.20477 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      1.35s CPU      1.36s WALL (       1 calls)
     main_loop    :     12.30s CPU     12.66s WALL (      91 calls)
     cpr_total    :     12.31s CPU     12.67s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     12.31s CPU     12.67s WALL (      91 calls)
     move_electro :     12.22s CPU     12.57s WALL (      91 calls)

     Called by move_electrons:
     rhoofr       :      4.14s CPU      4.17s WALL (      92 calls)
     vofrho       :      6.65s CPU      6.97s WALL (      92 calls)
     dforce       :      1.54s CPU      1.55s WALL (     184 calls)
     calphi       :      0.01s CPU      0.01s WALL (      92 calls)
     nlfl         :      0.00s CPU      0.00s WALL (      92 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (      92 calls)
     rsg          :      0.00s CPU      0.01s WALL (      92 calls)
     rhoset       :      0.01s CPU      0.01s WALL (      92 calls)
     sigset       :      0.00s CPU      0.00s WALL (      92 calls)
     tauset       :      0.00s CPU      0.00s WALL (      92 calls)
     ortho        :      0.02s CPU      0.02s WALL (      92 calls)
     updatc       :      0.00s CPU      0.00s WALL (      92 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (      92 calls)
     nlfq         :      0.01s CPU      0.01s WALL (      92 calls)
     nlsm1        :      0.00s CPU      0.00s WALL (      93 calls)
     nlsm2        :      0.01s CPU      0.01s WALL (      92 calls)
     fft          :      1.58s CPU      1.65s WALL (     921 calls)
     ffts         :      0.70s CPU      0.71s WALL (     368 calls)
     fftw         :      1.97s CPU      1.98s WALL (    2208 calls)
     fft_scatt_xy :      0.94s CPU      0.96s WALL (    3497 calls)
     fft_scatt_yz :      1.42s CPU      1.44s WALL (    3497 calls)
     betagx       :      0.54s CPU      0.54s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      1.19s CPU      1.20s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.02s CPU      0.02s WALL (       1 calls)
     from_scratch :      0.14s CPU      0.15s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.00s CPU      0.00s WALL (      93 calls)
     exch_corr    :      4.78s CPU      5.05s WALL (      92 calls)


     CP           :     13.73s CPU     14.10s WALL


   This run was terminated on:  12:14:17   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
