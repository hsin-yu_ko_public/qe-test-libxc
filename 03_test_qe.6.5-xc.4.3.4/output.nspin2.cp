
     Program CP v.6.5 starts on  6Jun2021 at  2: 8:52 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Local Spin Density calculation
   Number of Electrons=     8
   Spins up   =     4, occupations: 
   1.00 1.00 1.00 1.00
   Spins down =     4, occupations: 
   1.00 1.00 1.00 1.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   spin =   1 nbnd =     4
    1.00 1.00 1.00 1.00
   spin =   2 nbnd =     4
    1.00 1.00 1.00 1.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.611973367287740    0.0    0.00      11.235136143232      11.235136143232      11.235136143232      11.847109510520   0.0000   0.0000   0.0000   0.0000
      2    1.763463188862377    0.0    0.00       9.605227375784       9.605227375784       9.605227375784      11.368690564647   0.0000   0.0000   0.0000   0.0000
      3    3.257016713120856    0.0    0.00       7.110901100303       7.110901100303       7.110901100303      10.367917813424   0.0000   0.0000   0.0000   0.0000
      4    4.878844513842386    0.0    0.00       3.865457971578       3.865457971578       3.865457971578       8.744302485420   0.0000   0.0000   0.0000   0.0000
      5    6.377904908817333    0.0    0.00       0.080998063426       0.080998063426       0.080998063426       6.458902972244   0.0000   0.0000   0.0000   0.0000
      6    7.269264563624662    0.0    0.00      -3.633364291521      -3.633364291521      -3.633364291521       3.635900272104   0.0000   0.0000   0.0000   0.0000
      7    7.015931754874480    0.0    0.00      -6.301517192474      -6.301517192474      -6.301517192474       0.714414562401   0.0000   0.0000   0.0000   0.0000
      8    5.649547382420120    0.0    0.00      -7.423718037331      -7.423718037331      -7.423718037331      -1.774170654911   0.0000   0.0000   0.0000   0.0000
      9    3.904177939827524    0.0    0.00      -7.493309126482      -7.493309126482      -7.493309126482      -3.589131186654   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.93 (Slater)  1.55 (Becke)  0.00 (expected)


                total energy =       -7.35751191038 Hartree a.u.
              kinetic energy =        5.64397 Hartree a.u.
        electrostatic energy =      -12.31972 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -1.58163 Hartree a.u.
  n-l pseudopotential energy =        2.83423 Hartree a.u.
 exchange-correlation energy =       -1.93436 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -83.05   -8.49   -3.53    2.04

   Eigenvalues (eV), kp =   1 , spin =  2

  -78.09  -24.62   -5.61   -2.26


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.58831848382542E+00    -0.10190331775050E+01    -0.19760170308552E+00
   H       0.12088309465682E+01    -0.20128099775843E+00    -0.22558421355059E+00
   H      -0.19930372447285E+00     0.12066141363243E+01     0.93651909898094E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.545543125753811    0.0    0.00      -7.357511910379      -7.357511910379      -7.357511910379      -4.811968784625   0.0000   0.0000   0.0000   0.0000
     11    1.843624286111806    0.0    0.00      -7.513639551808      -7.513639551808      -7.513639551808      -5.670015265696   0.0000   0.0000   0.0000   0.0000
     12    1.647203566373679    0.0    0.00      -8.027466182894      -8.027466182894      -8.027466182894      -6.380262616520   0.0000   0.0000   0.0000   0.0000
     13    1.669733932409729    0.0    0.00      -8.735453761574      -8.735453761574      -8.735453761574      -7.065719829164   0.0000   0.0000   0.0000   0.0000
     14    1.688001121783370    0.0    0.00      -9.439306111479      -9.439306111479      -9.439306111479      -7.751304989696   0.0000   0.0000   0.0000   0.0000
     15    1.618170254449055    0.0    0.00     -10.031680156584     -10.031680156584     -10.031680156584      -8.413509902135   0.0000   0.0000   0.0000   0.0000
     16    1.487027439689405    0.0    0.00     -10.514937991366     -10.514937991366     -10.514937991366      -9.027910551676   0.0000   0.0000   0.0000   0.0000
     17    1.353701647327256    0.0    0.00     -10.943227738828     -10.943227738828     -10.943227738828      -9.589526091501   0.0000   0.0000   0.0000   0.0000
     18    1.251802594281580    0.0    0.00     -11.359736434605     -11.359736434605     -11.359736434605     -10.107933840324   0.0000   0.0000   0.0000   0.0000
     19    1.179494916631254    0.0    0.00     -11.774110027475     -11.774110027475     -11.774110027475     -10.594615110843   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.25 (Slater)  1.39 (Becke)  0.00 (expected)


                total energy =      -12.17404332735 Hartree a.u.
              kinetic energy =        7.14561 Hartree a.u.
        electrostatic energy =      -17.87214 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.59876 Hartree a.u.
  n-l pseudopotential energy =        4.03153 Hartree a.u.
 exchange-correlation energy =       -2.88028 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -60.84  -10.27   -0.76   -0.06

   Eigenvalues (eV), kp =   1 , spin =  2

  -62.07  -33.06  -21.53   -3.13


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.60441976497345E+00     0.35733408576187E+00    -0.29151630070970E+00
   H       0.72885761281727E+00    -0.96019098979068E-01    -0.18396692544789E+00
   H      -0.45305107807301E-01     0.87580348082181E+00    -0.82063614845047E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.118620423321729    0.0    0.00     -12.174043327346     -12.174043327346     -12.174043327346     -11.055422904024   0.0000   0.0000   0.0000   0.0000
     21    1.054725933322155    0.0    0.00     -12.545149801390     -12.545149801390     -12.545149801390     -11.490423868067   0.0000   0.0000   0.0000   0.0000
     22    0.985121099345763    0.0    0.00     -12.882314026541     -12.882314026541     -12.882314026541     -11.897192927195   0.0000   0.0000   0.0000   0.0000
     23    0.916830141683790    0.0    0.00     -13.192385114680     -13.192385114680     -13.192385114680     -12.275554972996   0.0000   0.0000   0.0000   0.0000
     24    0.858471814790227    0.0    0.00     -13.487160632836     -13.487160632836     -13.487160632836     -12.628688818046   0.0000   0.0000   0.0000   0.0000
     25    0.814919263963151    0.0    0.00     -13.776658275800     -13.776658275800     -13.776658275800     -12.961739011837   0.0000   0.0000   0.0000   0.0000
     26    0.786626191498599    0.0    0.00     -14.067893898033     -14.067893898033     -14.067893898033     -13.281267706534   0.0000   0.0000   0.0000   0.0000
     27    0.769245387614425    0.0    0.00     -14.361700123220     -14.361700123220     -14.361700123220     -13.592454735606   0.0000   0.0000   0.0000   0.0000
     28    0.755422786401357    0.0    0.00     -14.653329803511     -14.653329803511     -14.653329803511     -13.897907017109   0.0000   0.0000   0.0000   0.0000
     29    0.737727938494098    0.0    0.00     -14.935124787545     -14.935124787545     -14.935124787545     -14.197396849051   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 1.33 (Slater)  1.11 (Becke)  0.00 (expected)


                total energy =      -15.19792265998 Hartree a.u.
              kinetic energy =        9.30123 Hartree a.u.
        electrostatic energy =      -21.12160 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.13850 Hartree a.u.
  n-l pseudopotential energy =        3.32424 Hartree a.u.
 exchange-correlation energy =       -3.56330 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -41.11  -18.26  -15.85    0.24

   Eigenvalues (eV), kp =   1 , spin =  2

  -43.19  -25.78  -22.22   -4.49


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.56596431877463E-01     0.19414095777304E+00     0.76211703718073E+00
   H       0.28950171110210E+00    -0.58258591841208E-01     0.78266806976430E-01
   H      -0.96268360221740E-01     0.26083410096427E+00     0.92862780139107E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.710061060783631    0.0    0.00     -15.197922659980     -15.197922659980     -15.197922659980     -14.487861599197   0.0000   0.0000   0.0000   0.0000
     31    0.668895305009297    0.0    0.00     -15.433190166240     -15.433190166240     -15.433190166240     -14.764294861231   0.0000   0.0000   0.0000   0.0000
     32    0.614333048967300    0.0    0.00     -15.635563541395     -15.635563541395     -15.635563541395     -15.021230492428   0.0000   0.0000   0.0000   0.0000
     33    0.549300161474017    0.0    0.00     -15.803092472332     -15.803092472332     -15.803092472332     -15.253792310858   0.0000   0.0000   0.0000   0.0000
     34    0.478542671085645    0.0    0.00     -15.937324764259     -15.937324764259     -15.937324764259     -15.458782093174   0.0000   0.0000   0.0000   0.0000
     35    0.407595406286993    0.0    0.00     -16.042751865974     -16.042751865974     -16.042751865974     -15.635156459687   0.0000   0.0000   0.0000   0.0000
     36    0.341404083092937    0.0    0.00     -16.125390994245     -16.125390994245     -16.125390994245     -15.783986911152   0.0000   0.0000   0.0000   0.0000
     37    0.283454129933647    0.0    0.00     -16.191482175265     -16.191482175265     -16.191482175265     -15.908028045331   0.0000   0.0000   0.0000   0.0000
     38    0.235502507687461    0.0    0.00     -16.246506757981     -16.246506757981     -16.246506757981     -16.011004250294   0.0000   0.0000   0.0000   0.0000
     39    0.197763526819071    0.0    0.00     -16.294788646648     -16.294788646648     -16.294788646648     -16.097025119829   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.69 (Slater)  0.87 (Becke)  0.00 (expected)


                total energy =      -16.33929535441 Hartree a.u.
              kinetic energy =       11.36520 Hartree a.u.
        electrostatic energy =      -22.66984 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.65694 Hartree a.u.
  n-l pseudopotential energy =        2.56786 Hartree a.u.
 exchange-correlation energy =       -3.94556 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -32.69  -17.85  -14.27   -1.85

   Eigenvalues (eV), kp =   1 , spin =  2

  -34.15  -20.06  -16.70   -8.87


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13639194003334E+00    -0.28357746642139E+00    -0.83347657479470E-01
   H       0.13209443307845E+00    -0.34546753812645E-01     0.49890747216309E-01
   H      -0.31749481414200E-01     0.12678124253074E+00    -0.27533233734570E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.169304881562841    0.0    0.00     -16.339295354414     -16.339295354414     -16.339295354414     -16.169990472851   0.0000   0.0000   0.0000   0.0000
     41    0.148499081234211    0.0    0.00     -16.381775849062     -16.381775849062     -16.381775849062     -16.233276767828   0.0000   0.0000   0.0000   0.0000
     42    0.133435231514530    0.0    0.00     -16.422957879967     -16.422957879967     -16.422957879967     -16.289522648453   0.0000   0.0000   0.0000   0.0000
     43    0.122269236769134    0.0    0.00     -16.462885609619     -16.462885609619     -16.462885609619     -16.340616372850   0.0000   0.0000   0.0000   0.0000
     44    0.113449421404147    0.0    0.00     -16.501211855595     -16.501211855595     -16.501211855595     -16.387762434191   0.0000   0.0000   0.0000   0.0000
     45    0.105822729436946    0.0    0.00     -16.537467515185     -16.537467515185     -16.537467515185     -16.431644785748   0.0000   0.0000   0.0000   0.0000
     46    0.098628607432042    0.0    0.00     -16.571192428498     -16.571192428498     -16.571192428498     -16.472563821066   0.0000   0.0000   0.0000   0.0000
     47    0.091456958033538    0.0    0.00     -16.602050790524     -16.602050790524     -16.602050790524     -16.510593832491   0.0000   0.0000   0.0000   0.0000
     48    0.084181733769724    0.0    0.00     -16.629901421069     -16.629901421069     -16.629901421069     -16.545719687299   0.0000   0.0000   0.0000   0.0000
     49    0.076838099279550    0.0    0.00     -16.654742419320     -16.654742419320     -16.654742419320     -16.577904320041   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.67 (Slater)  0.77 (Becke)  0.00 (expected)


                total energy =      -16.67668413303 Hartree a.u.
              kinetic energy =       11.54988 Hartree a.u.
        electrostatic energy =      -22.85247 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.64376 Hartree a.u.
  n-l pseudopotential energy =        2.29619 Hartree a.u.
 exchange-correlation energy =       -4.02653 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -30.35  -16.84  -12.93   -3.21

   Eigenvalues (eV), kp =   1 , spin =  2

  -32.04  -17.27  -14.10  -11.59


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.82239557818540E-01    -0.10950156738748E+00    -0.21217828771183E-01
   H       0.51723661944520E-01    -0.15037973382255E-01     0.53850024826974E-01
   H      -0.22534981959820E-01     0.88209357733640E-01     0.22728652700119E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.069534918662381    0.0    0.00     -16.676684133034     -16.676684133034     -16.676684133034     -16.607149214372   0.0000   0.0000   0.0000   0.0000
     51    0.062392534302657    0.0    0.00     -16.695890394202     -16.695890394202     -16.695890394202     -16.633497859899   0.0000   0.0000   0.0000   0.0000
     52    0.055518174168951    0.0    0.00     -16.712550881294     -16.712550881294     -16.712550881294     -16.657032707125   0.0000   0.0000   0.0000   0.0000
     53    0.049033555079516    0.0    0.00     -16.726916066259     -16.726916066259     -16.726916066259     -16.677882511180   0.0000   0.0000   0.0000   0.0000
     54    0.043072831856150    0.0    0.00     -16.739316746416     -16.739316746416     -16.739316746416     -16.696243914560   0.0000   0.0000   0.0000   0.0000
     55    0.037736170935645    0.0    0.00     -16.750085233368     -16.750085233368     -16.750085233368     -16.712349062432   0.0000   0.0000   0.0000   0.0000
     56    0.033068594986325    0.0    0.00     -16.759524173917     -16.759524173917     -16.759524173917     -16.726455578930   0.0000   0.0000   0.0000   0.0000
     57    0.029075226174276    0.0    0.00     -16.767909721484     -16.767909721484     -16.767909721484     -16.738834495310   0.0000   0.0000   0.0000   0.0000
     58    0.025732763913263    0.0    0.00     -16.775484320215     -16.775484320215     -16.775484320215     -16.749751556302   0.0000   0.0000   0.0000   0.0000
     59    0.022984181200230    0.0    0.00     -16.782441309347     -16.782441309347     -16.782441309347     -16.759457128147   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.68 (Slater)  0.70 (Becke)  0.00 (expected)


                total energy =      -16.78891889034 Hartree a.u.
              kinetic energy =       11.45052 Hartree a.u.
        electrostatic energy =      -22.85552 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60975 Hartree a.u.
  n-l pseudopotential energy =        2.27344 Hartree a.u.
 exchange-correlation energy =       -4.04761 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -29.90  -15.96  -12.33   -4.19

   Eigenvalues (eV), kp =   1 , spin =  2

  -31.32  -16.06  -12.79  -11.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.48579009700988E-01    -0.73417063896158E-01     0.32013665586137E-01
   H       0.35466465365473E-01    -0.82210492583802E-02     0.27220097528074E-01
   H      -0.14459586117667E-01     0.63834448019555E-01     0.26601067862052E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.020748941932466    0.0    0.00     -16.788918890341     -16.788918890341     -16.788918890341     -16.768169948408   0.0000   0.0000   0.0000   0.0000
     61    0.018944986919122    0.0    0.00     -16.795022543862     -16.795022543862     -16.795022543862     -16.776077556943   0.0000   0.0000   0.0000   0.0000
     62    0.017500013533942    0.0    0.00     -16.800839032184     -16.800839032184     -16.800839032184     -16.783339018650   0.0000   0.0000   0.0000   0.0000
     63    0.016353183729951    0.0    0.00     -16.806440188957     -16.806440188957     -16.806440188957     -16.790087005227   0.0000   0.0000   0.0000   0.0000
     64    0.015452420344677    0.0    0.00     -16.811883962696     -16.811883962696     -16.811883962696     -16.796431542352   0.0000   0.0000   0.0000   0.0000
     65    0.014752131760583    0.0    0.00     -16.817213448348     -16.817213448348     -16.817213448348     -16.802461316588   0.0000   0.0000   0.0000   0.0000
     66    0.014214144824139    0.0    0.00     -16.822461456057     -16.822461456057     -16.822461456057     -16.808247311232   0.0000   0.0000   0.0000   0.0000
     67    0.013807139353453    0.0    0.00     -16.827653720667     -16.827653720667     -16.827653720667     -16.813846581314   0.0000   0.0000   0.0000   0.0000
     68    0.013504013354725    0.0    0.00     -16.832809721161     -16.832809721161     -16.832809721161     -16.819305707806   0.0000   0.0000   0.0000   0.0000
     69    0.013279341677417    0.0    0.00     -16.837940195240     -16.837940195240     -16.837940195240     -16.824660853562   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.54 (Slater)  0.56 (Becke)  0.00 (expected)


                total energy =      -16.84304689939 Hartree a.u.
              kinetic energy =       11.54601 Hartree a.u.
        electrostatic energy =      -22.91799 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62167 Hartree a.u.
  n-l pseudopotential energy =        2.23242 Hartree a.u.
 exchange-correlation energy =       -4.08182 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -29.09  -15.19  -11.51   -4.97

   Eigenvalues (eV), kp =   1 , spin =  2

  -30.20  -15.17  -11.66  -10.97


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.37836244130556E-01    -0.61547249487263E-01    -0.36865039003940E-02
   H       0.28340003584281E-01    -0.27922986261749E-02     0.14954011704408E-01
   H      -0.58906800874534E-02     0.43177749926481E-01     0.18022659327643E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.013108959679399    0.0    0.00     -16.843046899394     -16.843046899394     -16.843046899394     -16.829937939715   0.0000   0.0000   0.0000   0.0000
     71    0.012972310733271    0.0    0.00     -16.848126688763     -16.848126688763     -16.848126688763     -16.835154378030   0.0000   0.0000   0.0000   0.0000
     72    0.012852010203522    0.0    0.00     -16.853172108195     -16.853172108195     -16.853172108195     -16.840320097992   0.0000   0.0000   0.0000   0.0000
     73    0.012732866388773    0.0    0.00     -16.858171362601     -16.858171362601     -16.858171362601     -16.845438496212   0.0000   0.0000   0.0000   0.0000
     74    0.012602736684473    0.0    0.00     -16.863109806862     -16.863109806862     -16.863109806862     -16.850507070178   0.0000   0.0000   0.0000   0.0000
     75    0.012454837630459    0.0    0.00     -16.867974289090     -16.867974289090     -16.867974289090     -16.855519451460   0.0000   0.0000   0.0000   0.0000
     76    0.012287529443834    0.0    0.00     -16.872756438345     -16.872756438345     -16.872756438345     -16.860468908901   0.0000   0.0000   0.0000   0.0000
     77    0.012098472303602    0.0    0.00     -16.877446217368     -16.877446217368     -16.877446217368     -16.865347745064   0.0000   0.0000   0.0000   0.0000
     78    0.011882306177820    0.0    0.00     -16.882028160790     -16.882028160790     -16.882028160790     -16.870145854612   0.0000   0.0000   0.0000   0.0000
     79    0.011634741452516    0.0    0.00     -16.886485570553     -16.886485570553     -16.886485570553     -16.874850829100   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.32 (Slater)  0.39 (Becke)  0.00 (expected)


                total energy =      -16.89080665053 Hartree a.u.
              kinetic energy =       11.63833 Hartree a.u.
        electrostatic energy =      -22.96457 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62386 Hartree a.u.
  n-l pseudopotential energy =        2.18017 Hartree a.u.
 exchange-correlation energy =       -4.12089 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -28.14  -14.29  -10.58   -5.68

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.93  -14.28  -10.68   -9.37


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.25071606109891E-01    -0.38946069853240E-01     0.53009574638385E-03
   H       0.17435094040784E-01     0.18225665318466E-03     0.11745929920560E-01
   H      -0.11451630595988E-02     0.24257174822052E-01     0.13960653166970E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.011356236003453    0.0    0.00     -16.890806650533     -16.890806650533     -16.890806650533     -16.879450414529   0.0000   0.0000   0.0000   0.0000
     81    0.011048123774720    0.0    0.00     -16.894981136538     -16.894981136538     -16.894981136538     -16.883933012763   0.0000   0.0000   0.0000   0.0000
     82    0.010710245377206    0.0    0.00     -16.898996447395     -16.898996447395     -16.898996447395     -16.888286202018   0.0000   0.0000   0.0000   0.0000
     83    0.010345547090377    0.0    0.00     -16.902843381735     -16.902843381735     -16.902843381735     -16.892497834645   0.0000   0.0000   0.0000   0.0000
     84    0.009961169996579    0.0    0.00     -16.906520629266     -16.906520629266     -16.906520629266     -16.896559459270   0.0000   0.0000   0.0000   0.0000
     85    0.009563855700520    0.0    0.00     -16.910028342809     -16.910028342809     -16.910028342809     -16.900464487108   0.0000   0.0000   0.0000   0.0000
     86    0.009159062573926    0.0    0.00     -16.913367679265     -16.913367679265     -16.913367679265     -16.904208616691   0.0000   0.0000   0.0000   0.0000
     87    0.008752789484856    0.0    0.00     -16.916542992733     -16.916542992733     -16.916542992733     -16.907790203248   0.0000   0.0000   0.0000   0.0000
     88    0.008349461314258    0.0    0.00     -16.919559578901     -16.919559578901     -16.919559578901     -16.911210117586   0.0000   0.0000   0.0000   0.0000
     89    0.007950041968851    0.0    0.00     -16.922419704708     -16.922419704708     -16.922419704708     -16.914469662740   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.14 (Slater)  0.24 (Becke)  0.00 (expected)


                total energy =      -16.92512373795 Hartree a.u.
              kinetic energy =       11.68238 Hartree a.u.
        electrostatic energy =      -22.98481 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62145 Hartree a.u.
  n-l pseudopotential energy =        2.14820 Hartree a.u.
 exchange-correlation energy =       -4.14944 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.60  -13.59   -9.89   -6.27

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.03  -13.61   -9.97   -8.16


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20925336022654E-01    -0.26389909411073E-01     0.97409624175049E-02
   H       0.13143563981652E-01     0.20974258550744E-02     0.74651033519437E-02
   H       0.15398871891463E-02     0.15134419311070E-01     0.91204151058253E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.007553688346512    0.0    0.00     -16.925123737949     -16.925123737949     -16.925123737949     -16.917570049602   0.0000   0.0000   0.0000   0.0000
     91    0.007159009388979    0.0    0.00     -16.927671582067     -16.927671582067     -16.927671582067     -16.920512572678   0.0000   0.0000   0.0000   0.0000
     92    0.006763241510728    0.0    0.00     -16.930060455236     -16.930060455236     -16.930060455236     -16.923297213725   0.0000   0.0000   0.0000   0.0000
     93    0.006363521015922    0.0    0.00     -16.932286217823     -16.932286217823     -16.932286217823     -16.925922696807   0.0000   0.0000   0.0000   0.0000
     94    0.005958699377370    0.0    0.00     -16.934345749336     -16.934345749336     -16.934345749336     -16.928387049959   0.0000   0.0000   0.0000   0.0000
     95    0.005549732133748    0.0    0.00     -16.936238053221     -16.936238053221     -16.936238053221     -16.930688321087   0.0000   0.0000   0.0000   0.0000
     96    0.005139437745777    0.0    0.00     -16.937964747341     -16.937964747341     -16.937964747341     -16.932825309595   0.0000   0.0000   0.0000   0.0000
     97    0.004732292275189    0.0    0.00     -16.939530641601     -16.939530641601     -16.939530641601     -16.934798349325   0.0000   0.0000   0.0000   0.0000
     98    0.004333889782176    0.0    0.00     -16.940943785145     -16.940943785145     -16.940943785145     -16.936609895363   0.0000   0.0000   0.0000   0.0000
     99    0.003950019561982    0.0    0.00     -16.942214750945     -16.942214750945     -16.942214750945     -16.938264731383   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   100

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.05 (Slater)  0.13 (Becke)  0.00 (expected)


                total energy =      -16.94335544175 Hartree a.u.
              kinetic energy =       11.70414 Hartree a.u.
        electrostatic energy =      -22.99774 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62350 Hartree a.u.
  n-l pseudopotential energy =        2.13975 Hartree a.u.
 exchange-correlation energy =       -4.16600 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.34  -13.21   -9.54   -6.65

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.54  -13.24   -9.58   -7.48


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20691351489527E-01    -0.23066653060414E-01     0.34132260177928E-02
   H       0.12696297392863E-01     0.26093557201961E-02     0.35914970843580E-02
   H       0.24633438788777E-02     0.13284350828871E-01     0.45282881206988E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    100    0.003585589535180    0.0    0.00     -16.943355441748     -16.943355441748     -16.943355441748     -16.939769852213   0.0000   0.0000   0.0000   0.0000
    101    0.003243692561321    0.0    0.00     -16.944377487925     -16.944377487925     -16.944377487925     -16.941133795363   0.0000   0.0000   0.0000   0.0000
    102    0.002925377293582    0.0    0.00     -16.945291204278     -16.945291204278     -16.945291204278     -16.942365826985   0.0000   0.0000   0.0000   0.0000
    103    0.002630236224674    0.0    0.00     -16.946105487058     -16.946105487058     -16.946105487058     -16.943475250833   0.0000   0.0000   0.0000   0.0000
    104    0.002357250946983    0.0    0.00     -16.946828348126     -16.946828348126     -16.946828348126     -16.944471097179   0.0000   0.0000   0.0000   0.0000
    105    0.002105317727625    0.0    0.00     -16.947467310376     -16.947467310376     -16.947467310376     -16.945361992649   0.0000   0.0000   0.0000   0.0000
    106    0.001873402025432    0.0    0.00     -16.948029555523     -16.948029555523     -16.948029555523     -16.946156153497   0.0000   0.0000   0.0000   0.0000
    107    0.001660577558868    0.0    0.00     -16.948521992771     -16.948521992771     -16.948521992771     -16.946861415212   0.0000   0.0000   0.0000   0.0000
    108    0.001466028115856    0.0    0.00     -16.948951289895     -16.948951289895     -16.948951289895     -16.947485261779   0.0000   0.0000   0.0000   0.0000
    109    0.001288998034894    0.0    0.00     -16.949323867566     -16.949323867566     -16.949323867566     -16.948034869531   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   110

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.01 (Slater)  0.07 (Becke)  0.00 (expected)


                total energy =      -16.94964586370 Hartree a.u.
              kinetic energy =       11.70923 Hartree a.u.
        electrostatic energy =      -22.99862 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62227 Hartree a.u.
  n-l pseudopotential energy =        2.13354 Hartree a.u.
 exchange-correlation energy =       -4.17152 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.09   -9.41   -6.87

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.35  -13.11   -9.44   -7.19


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18986044022569E-01    -0.20105614525078E-01     0.40174904755285E-02
   H       0.13342915776945E-01     0.27147257022947E-02     0.17254193973367E-02
   H       0.26799994032256E-02     0.13535225161038E-01     0.20930350662056E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    110    0.001128763807925    0.0    0.00     -16.949645863697     -16.949645863697     -16.949645863697     -16.948517099889   0.0000   0.0000   0.0000   0.0000
    111    0.000984608484961    0.0    0.00     -16.949923134523     -16.949923134523     -16.949923134523     -16.948938526038   0.0000   0.0000   0.0000   0.0000
    112    0.000855768434012    0.0    0.00     -16.950161196344     -16.950161196344     -16.950161196344     -16.949305427910   0.0000   0.0000   0.0000   0.0000
    113    0.000741374273034    0.0    0.00     -16.950365160659     -16.950365160659     -16.950365160659     -16.949623786386   0.0000   0.0000   0.0000   0.0000
    114    0.000640428279815    0.0    0.00     -16.950539612662     -16.950539612662     -16.950539612662     -16.949899184383   0.0000   0.0000   0.0000   0.0000
    115    0.000551833429886    0.0    0.00     -16.950688618336     -16.950688618336     -16.950688618336     -16.950136784907   0.0000   0.0000   0.0000   0.0000
    116    0.000474438829674    0.0    0.00     -16.950815712420     -16.950815712420     -16.950815712420     -16.950341273590   0.0000   0.0000   0.0000   0.0000
    117    0.000407094619618    0.0    0.00     -16.950924008041     -16.950924008041     -16.950924008041     -16.950516913421   0.0000   0.0000   0.0000   0.0000
    118    0.000348699188272    0.0    0.00     -16.951016185852     -16.951016185852     -16.951016185852     -16.950667486663   0.0000   0.0000   0.0000   0.0000
    119    0.000298221234379    0.0    0.00     -16.951094582821     -16.951094582821     -16.951094582821     -16.950796361587   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   120

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.03 (Becke)  0.00 (expected)


                total energy =      -16.95116121850 Hartree a.u.
              kinetic energy =       11.71265 Hartree a.u.
        electrostatic energy =      -22.99970 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62224 Hartree a.u.
  n-l pseudopotential energy =        2.13157 Hartree a.u.
 exchange-correlation energy =       -4.17343 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.25  -13.06   -9.38   -6.95

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.29  -13.06   -9.39   -7.07


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17862216442360E-01    -0.18211399174386E-01     0.39300893460226E-02
   H       0.13222907443685E-01     0.28782525441861E-02     0.84064510670422E-03
   H       0.28748612268556E-02     0.13265828411784E-01     0.92277734693015E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    120    0.000254710583335    0.0    0.00     -16.951161218502     -16.951161218502     -16.951161218502     -16.950906507918   0.0000   0.0000   0.0000   0.0000
    121    0.000217302479344    0.0    0.00     -16.951217830279     -16.951217830279     -16.951217830279     -16.951000527800   0.0000   0.0000   0.0000   0.0000
    122    0.000185213391909    0.0    0.00     -16.951265919570     -16.951265919570     -16.951265919570     -16.951080706178   0.0000   0.0000   0.0000   0.0000
    123    0.000157740919111    0.0    0.00     -16.951306760146     -16.951306760146     -16.951306760146     -16.951149019227   0.0000   0.0000   0.0000   0.0000
    124    0.000134260003688    0.0    0.00     -16.951341442365     -16.951341442365     -16.951341442365     -16.951207182361   0.0000   0.0000   0.0000   0.0000
    125    0.000114216919071    0.0    0.00     -16.951370892397     -16.951370892397     -16.951370892397     -16.951256675478   0.0000   0.0000   0.0000   0.0000
    126    0.000097126552695    0.0    0.00     -16.951395899397     -16.951395899397     -16.951395899397     -16.951298772844   0.0000   0.0000   0.0000   0.0000
    127    0.000082566228953    0.0    0.00     -16.951417135231     -16.951417135231     -16.951417135231     -16.951334569002   0.0000   0.0000   0.0000   0.0000
    128    0.000070170889224    0.0    0.00     -16.951435164248     -16.951435164248     -16.951435164248     -16.951364993359   0.0000   0.0000   0.0000   0.0000
    129    0.000059626169692    0.0    0.00     -16.951450474762     -16.951450474762     -16.951450474762     -16.951390848592   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   130

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.02 (Becke)  0.00 (expected)


                total energy =      -16.95146347717 Hartree a.u.
              kinetic energy =       11.71397 Hartree a.u.
        electrostatic energy =      -23.00040 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62255 Hartree a.u.
  n-l pseudopotential energy =        2.13137 Hartree a.u.
 exchange-correlation energy =       -4.17385 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.25  -13.05   -9.37   -6.99

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.27  -13.05   -9.37   -7.02


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17915343138749E-01    -0.17927701512319E-01     0.13188787996848E-02
   H       0.13119791255593E-01     0.29197770833913E-02     0.28674008787422E-03
   H       0.29200036474546E-02     0.13124499592341E-01     0.28218630284863E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    130    0.000050661261696    0.0    0.00     -16.951463477172     -16.951463477172     -16.951463477172     -16.951412815910   0.0000   0.0000   0.0000   0.0000
    131    0.000043044159857    0.0    0.00     -16.951474526905     -16.951474526905     -16.951474526905     -16.951431482745   0.0000   0.0000   0.0000   0.0000
    132    0.000036575528706    0.0    0.00     -16.951483920561     -16.951483920561     -16.951483920561     -16.951447345032   0.0000   0.0000   0.0000   0.0000
    133    0.000031084448141    0.0    0.00     -16.951491908596     -16.951491908596     -16.951491908596     -16.951460824148   0.0000   0.0000   0.0000   0.0000
    134    0.000026424393231    0.0    0.00     -16.951498707167     -16.951498707167     -16.951498707167     -16.951472282774   0.0000   0.0000   0.0000   0.0000
    135    0.000022470067224    0.0    0.00     -16.951504494713     -16.951504494713     -16.951504494713     -16.951482024646   0.0000   0.0000   0.0000   0.0000
    136    0.000019114729940    0.0    0.00     -16.951509426213     -16.951509426213     -16.951509426213     -16.951490311483   0.0000   0.0000   0.0000   0.0000
    137    0.000016267151454    0.0    0.00     -16.951513629953     -16.951513629953     -16.951513629953     -16.951497362802   0.0000   0.0000   0.0000   0.0000
    138    0.000013849641866    0.0    0.00     -16.951517215220     -16.951517215220     -16.951517215220     -16.951503365578   0.0000   0.0000   0.0000   0.0000
    139    0.000011796715910    0.0    0.00     -16.951520273777     -16.951520273777     -16.951520273777     -16.951508477061   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   140

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.01 (Becke)  0.00 (expected)


                total energy =      -16.95152288495 Hartree a.u.
              kinetic energy =       11.71399 Hartree a.u.
        electrostatic energy =      -23.00043 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62256 Hartree a.u.
  n-l pseudopotential energy =        2.13133 Hartree a.u.
 exchange-correlation energy =       -4.17386 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18050821316575E-01    -0.18052688562060E-01    -0.52891030893653E-04
   H       0.13109455962675E-01     0.29105846583121E-02     0.41881801363691E-04
   H       0.29107228954271E-02     0.13107685558509E-01     0.31795511697558E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    140    0.000010052693419    0.0    0.00     -16.951522884949     -16.951522884949     -16.951522884949     -16.951512832255   0.0000   0.0000   0.0000   0.0000
    141    0.000008570381968    0.0    0.00     -16.951525114067     -16.951525114067     -16.951525114067     -16.951516543685   0.0000   0.0000   0.0000   0.0000
    142    0.000007309926339    0.0    0.00     -16.951527019595     -16.951527019595     -16.951527019595     -16.951519709669   0.0000   0.0000   0.0000   0.0000
    143    0.000006237486200    0.0    0.00     -16.951528648027     -16.951528648027     -16.951528648027     -16.951522410541   0.0000   0.0000   0.0000   0.0000
    144    0.000005324447946    0.0    0.00     -16.951530041767     -16.951530041767     -16.951530041767     -16.951524717319   0.0000   0.0000   0.0000   0.0000
    145    0.000004546699783    0.0    0.00     -16.951531233314     -16.951531233314     -16.951531233314     -16.951526686614   0.0000   0.0000   0.0000   0.0000
    146    0.000003883882121    0.0    0.00     -16.951532251137     -16.951532251137     -16.951532251137     -16.951528367255   0.0000   0.0000   0.0000   0.0000
    147    0.000003318700778    0.0    0.00     -16.951533120485     -16.951533120485     -16.951533120485     -16.951529801784   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.331870D-05  0.1D-03  0.869347D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:   148

 Spin contamination: s(s+1)= 0.00 (Slater)  0.00 (Becke)  0.00 (expected)


                total energy =      -16.95153386585 Hartree a.u.
              kinetic energy =       11.71394 Hartree a.u.
        electrostatic energy =      -23.00042 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62255 Hartree a.u.
  n-l pseudopotential energy =        2.13135 Hartree a.u.
 exchange-correlation energy =       -4.17386 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18051119852325E-01    -0.18051196440916E-01    -0.88637754133053E-04
   H       0.13111771066248E-01     0.29101815498080E-02    -0.54112752185620E-05
   H       0.29097993408318E-02     0.13110909620607E-01    -0.10205928117093E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
    148    0.000002836528242    0.0    0.00     -16.951533865854     -16.951533865854     -16.951533865854     -16.951531029326   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.283653D-05  0.1D-03  0.745370D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.00s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.48877       0.48877 (AU)
   ekin          :       10.73722      10.73722 (AU)
   epot          :      -28.14329     -28.14329 (AU)
   total energy  :      -15.01288     -15.01288 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :      -15.01288     -15.01288 (AU)
   econs         :      -15.01288     -15.01288 (AU)
   pressure      :        0.00000       0.00000 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      0.71s CPU      0.72s WALL (       1 calls)
     main_loop    :     21.69s CPU     22.90s WALL (     148 calls)
     cpr_total    :     21.70s CPU     22.91s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     21.70s CPU     22.91s WALL (     148 calls)
     move_electro :     21.54s CPU     22.73s WALL (     148 calls)

     Called by move_electrons:
     rhoofr       :      4.66s CPU      4.74s WALL (     149 calls)
     vofrho       :     12.17s CPU     13.26s WALL (     149 calls)
     dforce       :      4.77s CPU      4.79s WALL (     596 calls)
     calphi       :      0.02s CPU      0.02s WALL (     149 calls)
     nlfl         :      0.00s CPU      0.00s WALL (     149 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (     298 calls)
     rsg          :      0.01s CPU      0.01s WALL (     298 calls)
     rhoset       :      0.02s CPU      0.02s WALL (     298 calls)
     sigset       :      0.01s CPU      0.01s WALL (     298 calls)
     tauset       :      0.01s CPU      0.01s WALL (     298 calls)
     ortho        :      0.06s CPU      0.06s WALL (     149 calls)
     updatc       :      0.01s CPU      0.01s WALL (     149 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (     149 calls)
     nlfq         :      0.02s CPU      0.02s WALL (     149 calls)
     nlsm1        :      0.01s CPU      0.01s WALL (     150 calls)
     nlsm2        :      0.02s CPU      0.02s WALL (     149 calls)
     fft          :      3.69s CPU      3.85s WALL (    2236 calls)
     ffts         :      1.00s CPU      1.01s WALL (     596 calls)
     fftw         :      5.79s CPU      5.84s WALL (    7152 calls)
     fft_scatt_xy :      2.54s CPU      2.59s WALL (    9984 calls)
     fft_scatt_yz :      2.96s CPU      3.01s WALL (    9984 calls)
     fft_scatt_tg :      0.04s CPU      0.04s WALL (    1788 calls)
     betagx       :      0.22s CPU      0.22s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      0.54s CPU      0.54s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.00s CPU      0.00s WALL (       1 calls)
     from_scratch :      0.16s CPU      0.17s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.01s CPU      0.01s WALL (     150 calls)


     CP           :     22.45s CPU     23.67s WALL


   This run was terminated on:   2: 9:16   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
