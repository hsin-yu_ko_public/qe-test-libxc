
     Program CP v.6.5 starts on  6Jun2021 at  2: 8:38 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Number of Electrons=     8, of States =     4
   Occupation numbers :
   2.00 2.00 2.00 2.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   internal stress tensor calculated
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   nbnd =     4
    2.00 2.00 2.00 2.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       90.70435      0

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    1.119327831822813    0.0    0.00      13.512050014699      13.512050014699      13.512050014699      14.631377846521   0.0000   0.0000   0.0000   0.0000
      2    3.238108059735323    0.0    0.00      10.473568736484      10.473568736484      10.473568736484      13.711676796220   0.0000   0.0000   0.0000   0.0000
      3    6.031788472690844    0.0    0.00       5.866389966497       5.866389966497       5.866389966497      11.898178439188   0.0000   0.0000   0.0000   0.0000
      4    8.576699668553035    0.0    0.00       0.477434513608       0.477434513608       0.477434513608       9.054134182161   0.0000   0.0000   0.0000   0.0000
      5    8.715549962523822    0.0    0.00      -2.905325801102      -2.905325801102      -2.905325801102       5.810224161422   0.0000   0.0000   0.0000   0.0000
      6    6.138094454780316    0.0    0.00      -2.835296547194      -2.835296547194      -2.835296547194       3.302797907587   0.0000   0.0000   0.0000   0.0000
      7    3.628184252076303    0.0    0.00      -2.053484119479      -2.053484119479      -2.053484119479       1.574700132597   0.0000   0.0000   0.0000   0.0000
      8    2.736751096364981    0.0    0.00      -2.197894731013      -2.197894731013      -2.197894731013       0.538856365352   0.0000   0.0000   0.0000   0.0000
      9    2.782770510501043    0.0    0.00      -2.988499133924      -2.988499133924      -2.988499133924      -0.205728623423   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)      -38.80804     10
     Pressure of Nuclei (GPa)             0.00000     10
     Pressure Total (GPa)           -38.80804     10


                total energy =       -3.77735404682 Hartree a.u.
              kinetic energy =        7.39113 Hartree a.u.
        electrostatic energy =      -15.38565 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.30472 Hartree a.u.
  n-l pseudopotential energy =        4.22823 Hartree a.u.
 exchange-correlation energy =        2.29366 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -88.86  -10.61   -5.27   -2.22


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
      -29.11311498        -8.10778126        -1.04208233
       -8.10778126       -35.93249304         1.64280496
       -1.04208233         1.64280496       -51.37851852
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.67454684444161E+00    -0.69278247344702E+00     0.10592591164924E+00
   H       0.11074374764071E+01    -0.24830719666477E+00    -0.43781703185677E-01
   H      -0.29341132330206E+00     0.11342478926536E+01     0.42149880771646E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.821056692148283    0.0    0.00      -3.777354046817      -3.777354046817      -3.777354046817      -0.956297354669   0.0000   0.0000   0.0000   0.0000
     11    2.533977504605510    0.0    0.00      -4.317453685830      -4.317453685830      -4.317453685830      -1.783476181224   0.0000   0.0000   0.0000   0.0000
     12    2.133360498771269    0.0    0.00      -4.691280924121      -4.691280924121      -4.691280924121      -2.557920425349   0.0000   0.0000   0.0000   0.0000
     13    1.829718574011223    0.0    0.00      -5.001667351576      -5.001667351576      -5.001667351576      -3.171948777565   0.0000   0.0000   0.0000   0.0000
     14    1.641760810864351    0.0    0.00      -5.272559425951      -5.272559425951      -5.272559425951      -3.630798615086   0.0000   0.0000   0.0000   0.0000
     15    1.525196367973725    0.0    0.00      -5.514206601233      -5.514206601233      -5.514206601233      -3.989010233260   0.0000   0.0000   0.0000   0.0000
     16    1.475966245200622    0.0    0.00      -5.777413278914      -5.777413278914      -5.777413278914      -4.301447033713   0.0000   0.0000   0.0000   0.0000
     17    1.512785153788563    0.0    0.00      -6.119519933823      -6.119519933823      -6.119519933823      -4.606734780034   0.0000   0.0000   0.0000   0.0000
     18    1.614939394438906    0.0    0.00      -6.546137745667      -6.546137745667      -6.546137745667      -4.931198351228   0.0000   0.0000   0.0000   0.0000
     19    1.710025245324448    0.0    0.00      -7.000646704476      -7.000646704476      -7.000646704476      -5.290621459152   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -116.23125     20
     Pressure of Nuclei (GPa)             0.00000     20
     Pressure Total (GPa)          -116.23125     20


                total energy =       -7.41207131432 Hartree a.u.
              kinetic energy =        9.29445 Hartree a.u.
        electrostatic energy =      -20.67969 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.93443 Hartree a.u.
  n-l pseudopotential energy =        3.35935 Hartree a.u.
 exchange-correlation energy =        3.54825 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -41.42  -24.68  -19.63   -0.18


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -117.19932330        -5.89345820         0.77270618
       -5.89345820      -112.41109558         0.55155496
        0.77270618         0.55155496      -119.08334340
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.35991793731110E+00    -0.65948074823430E-01     0.79405026180700E+00
   H       0.22422936294640E+00     0.58725321433103E-01     0.12360842394883E+00
   H      -0.13725968533704E+00     0.30968371987832E+00     0.90197833641381E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.719622044671492    0.0    0.00      -7.412071314323      -7.412071314323      -7.412071314323      -5.692449269652   0.0000   0.0000   0.0000   0.0000
     21    1.610356336039236    0.0    0.00      -7.734407508357      -7.734407508357      -7.734407508357      -6.124051172318   0.0000   0.0000   0.0000   0.0000
     22    1.404770579887538    0.0    0.00      -7.958468942881      -7.958468942881      -7.958468942881      -6.553698362993   0.0000   0.0000   0.0000   0.0000
     23    1.156332976917359    0.0    0.00      -8.097052123877      -8.097052123877      -8.097052123877      -6.940719146960   0.0000   0.0000   0.0000   0.0000
     24    0.917844804269524    0.0    0.00      -8.170725895323      -8.170725895323      -8.170725895323      -7.252881091053   0.0000   0.0000   0.0000   0.0000
     25    0.720514339371743    0.0    0.00      -8.199210118392      -8.199210118392      -8.199210118392      -7.478695779020   0.0000   0.0000   0.0000   0.0000
     26    0.571763818155556    0.0    0.00      -8.201241684524      -8.201241684524      -8.201241684524      -7.629477866369   0.0000   0.0000   0.0000   0.0000
     27    0.464888955046980    0.0    0.00      -8.193705441009      -8.193705441009      -8.193705441009      -7.728816485962   0.0000   0.0000   0.0000   0.0000
     28    0.388731635768266    0.0    0.00      -8.192418962439      -8.192418962439      -8.192418962439      -7.803687326671   0.0000   0.0000   0.0000   0.0000
     29    0.333932241820868    0.0    0.00      -8.209454647099      -8.209454647099      -8.209454647099      -7.875522405278   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -141.26542     30
     Pressure of Nuclei (GPa)             0.00000     30
     Pressure Total (GPa)          -141.26542     30


                total energy =       -8.24976304520 Hartree a.u.
              kinetic energy =       12.66866 Hartree a.u.
        electrostatic energy =      -23.23844 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.83407 Hartree a.u.
  n-l pseudopotential energy =        2.03278 Hartree a.u.
 exchange-correlation energy =        4.12130 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -28.40  -16.02  -11.90   -6.63


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -139.85305229        -2.09524629         0.72498614
       -2.09524629      -138.18769877         0.39298956
        0.72498614         0.39298956      -145.75550151
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.44340046150505E+00    -0.13249432074429E-01    -0.28518635725864E+00
   H      -0.76812570216531E-03    -0.26149559866930E-01     0.29214681588751E-01
   H      -0.40384650688341E-02     0.31335429497510E-01     0.20094541549913E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.293851524883705    0.0    0.00      -8.249763045196      -8.249763045196      -8.249763045196      -7.955911520313   0.0000   0.0000   0.0000   0.0000
     31    0.263020591797481    0.0    0.00      -8.311062718984      -8.311062718984      -8.311062718984      -8.048042127186   0.0000   0.0000   0.0000   0.0000
     32    0.236887232591796    0.0    0.00      -8.385013685004      -8.385013685004      -8.385013685004      -8.148126452412   0.0000   0.0000   0.0000   0.0000
     33    0.212035664581347    0.0    0.00      -8.460402594832      -8.460402594832      -8.460402594832      -8.248366930251   0.0000   0.0000   0.0000   0.0000
     34    0.186553345166656    0.0    0.00      -8.527284320869      -8.527284320869      -8.527284320869      -8.340730975702   0.0000   0.0000   0.0000   0.0000
     35    0.160182665802526    0.0    0.00      -8.578704669073      -8.578704669073      -8.578704669073      -8.418522003270   0.0000   0.0000   0.0000   0.0000
     36    0.133924889526737    0.0    0.00      -8.611715658897      -8.611715658897      -8.611715658897      -8.477790769370   0.0000   0.0000   0.0000   0.0000
     37    0.109268681624012    0.0    0.00      -8.627939894090      -8.627939894090      -8.627939894090      -8.518671212466   0.0000   0.0000   0.0000   0.0000
     38    0.087514826633915    0.0    0.00      -8.631491116122      -8.631491116122      -8.631491116122      -8.543976289488   0.0000   0.0000   0.0000   0.0000
     39    0.069557848687894    0.0    0.00      -8.627433158615      -8.627433158615      -8.627433158615      -8.557875309927   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -149.33435     40
     Pressure of Nuclei (GPa)             0.00000     40
     Pressure Total (GPa)          -149.33435     40


                total energy =       -8.62034583902 Hartree a.u.
              kinetic energy =       11.60720 Hartree a.u.
        electrostatic energy =      -22.94498 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.59876 Hartree a.u.
  n-l pseudopotential energy =        2.17328 Hartree a.u.
 exchange-correlation energy =        4.14292 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.96  -13.37   -9.91   -7.61


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -148.96496251         0.07950731        -0.36942098
        0.07950731      -149.16414712         0.12630664
       -0.36942098         0.12630664      -149.87395002
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.11793873193696E+00    -0.50580847502250E-01     0.65562615842228E-01
   H       0.14206902248918E-01     0.87495351168401E-02     0.59492486874547E-03
   H       0.28179614045702E-02    -0.28908559536767E-02     0.19575884565503E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.055568123094479    0.0    0.00      -8.620345839023      -8.620345839023      -8.620345839023      -8.564777715929   0.0000   0.0000   0.0000   0.0000
     41    0.044997898744709    0.0    0.00      -8.613902159360      -8.613902159360      -8.613902159360      -8.568904260615   0.0000   0.0000   0.0000   0.0000
     42    0.036938919555157    0.0    0.00      -8.610073098658      -8.610073098658      -8.610073098658      -8.573134179102   0.0000   0.0000   0.0000   0.0000
     43    0.030444039002202    0.0    0.00      -8.609447015819      -8.609447015819      -8.609447015819      -8.579002976817   0.0000   0.0000   0.0000   0.0000
     44    0.024802263528193    0.0    0.00      -8.611395984156      -8.611395984156      -8.611395984156      -8.586593720628   0.0000   0.0000   0.0000   0.0000
     45    0.019667603499605    0.0    0.00      -8.614559432433      -8.614559432433      -8.614559432433      -8.594891828934   0.0000   0.0000   0.0000   0.0000
     46    0.015022730123200    0.0    0.00      -8.617483272869      -8.617483272869      -8.617483272869      -8.602460542745   0.0000   0.0000   0.0000   0.0000
     47    0.011032854779502    0.0    0.00      -8.618968079907      -8.618968079907      -8.618968079907      -8.607935225127   0.0000   0.0000   0.0000   0.0000
     48    0.007873279921329    0.0    0.00      -8.618533046305      -8.618533046305      -8.618533046305      -8.610659766384   0.0000   0.0000   0.0000   0.0000
     49    0.005608914170835    0.0    0.00      -8.615998834638      -8.615998834638      -8.615998834638      -8.610389920467   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -149.93958     50
     Pressure of Nuclei (GPa)             0.00000     50
     Pressure Total (GPa)          -149.93958     50


                total energy =       -8.61178637315 Hartree a.u.
              kinetic energy =       11.68631 Hartree a.u.
        electrostatic energy =      -22.98928 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62332 Hartree a.u.
  n-l pseudopotential energy =        2.14852 Hartree a.u.
 exchange-correlation energy =        4.16599 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.44  -13.08   -9.39   -7.31


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.28751906        -0.31265752         0.02929704
       -0.31265752      -149.29568721         0.00403140
        0.02929704         0.00403140      -151.23553867
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.61248315592621E-01    -0.30623551644723E-01    -0.20006666508425E-01
   H       0.27628817532160E-01    -0.30011695403653E-03     0.22800495405530E-02
   H       0.11942258874839E-02     0.33698860854425E-01    -0.72146999362842E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.004159088379716    0.0    0.00      -8.611786373146      -8.611786373146      -8.611786373146      -8.607627284766   0.0000   0.0000   0.0000   0.0000
     51    0.003333225353411    0.0    0.00      -8.606508209768      -8.606508209768      -8.606508209768      -8.603174984414   0.0000   0.0000   0.0000   0.0000
     52    0.002900368129673    0.0    0.00      -8.600972335331      -8.600972335331      -8.600972335331      -8.598071967201   0.0000   0.0000   0.0000   0.0000
     53    0.002654245634751    0.0    0.00      -8.596018800723      -8.596018800723      -8.596018800723      -8.593364555088   0.0000   0.0000   0.0000   0.0000
     54    0.002451262020707    0.0    0.00      -8.592292806527      -8.592292806527      -8.592292806527      -8.589841544506   0.0000   0.0000   0.0000   0.0000
     55    0.002218241370947    0.0    0.00      -8.590100343920      -8.590100343920      -8.590100343920      -8.587882102549   0.0000   0.0000   0.0000   0.0000
     56    0.001939386927551    0.0    0.00      -8.589683113519      -8.589683113519      -8.589683113519      -8.587743726591   0.0000   0.0000   0.0000   0.0000
     57    0.001634102683978    0.0    0.00      -8.590827551799      -8.590827551799      -8.590827551799      -8.589193449115   0.0000   0.0000   0.0000   0.0000
     58    0.001334915080915    0.0    0.00      -8.593107231000      -8.593107231000      -8.593107231000      -8.591772315919   0.0000   0.0000   0.0000   0.0000
     59    0.001070786927355    0.0    0.00      -8.596159380634      -8.596159380634      -8.596159380634      -8.595088593707   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.36629     60
     Pressure of Nuclei (GPa)             0.00000     60
     Pressure Total (GPa)          -150.36629     60


                total energy =       -8.59945651391 Hartree a.u.
              kinetic energy =       11.73439 Hartree a.u.
        electrostatic energy =      -23.00769 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62535 Hartree a.u.
  n-l pseudopotential energy =        2.12428 Hartree a.u.
 exchange-correlation energy =        4.17492 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.20  -13.05   -9.34   -6.98


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.81828519        -0.25976499        -0.00216609
       -0.25976499      -149.77547351        -0.02359889
       -0.00216609        -0.02359889      -151.50509895
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.68675479625124E-02    -0.95337318140065E-02     0.39470196466685E-02
   H       0.10994681505280E-01     0.46575414155295E-02    -0.22532559282745E-02
   H       0.34930682734338E-02     0.97858561655506E-02    -0.55419790305063E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.000857847227150    0.0    0.00      -8.599456513913      -8.599456513913      -8.599456513913      -8.598598666686   0.0000   0.0000   0.0000   0.0000
     61    0.000697575436681    0.0    0.00      -8.602588915129      -8.602588915129      -8.602588915129      -8.601891339692   0.0000   0.0000   0.0000   0.0000
     62    0.000580517293176    0.0    0.00      -8.605252182298      -8.605252182298      -8.605252182298      -8.604671665005   0.0000   0.0000   0.0000   0.0000
     63    0.000492584872310    0.0    0.00      -8.607273903686      -8.607273903686      -8.607273903686      -8.606781318814   0.0000   0.0000   0.0000   0.0000
     64    0.000420820367094    0.0    0.00      -8.608587607383      -8.608587607383      -8.608587607383      -8.608166787016   0.0000   0.0000   0.0000   0.0000
     65    0.000356685527717    0.0    0.00      -8.609256229915      -8.609256229915      -8.609256229915      -8.608899544388   0.0000   0.0000   0.0000   0.0000
     66    0.000296625627251    0.0    0.00      -8.609372484678      -8.609372484678      -8.609372484678      -8.609075859051   0.0000   0.0000   0.0000   0.0000
     67    0.000240751720178    0.0    0.00      -8.609103215275      -8.609103215275      -8.609103215275      -8.608862463554   0.0000   0.0000   0.0000   0.0000
     68    0.000190860283365    0.0    0.00      -8.608608228021      -8.608608228021      -8.608608228021      -8.608417367738   0.0000   0.0000   0.0000   0.0000
     69    0.000148715444789    0.0    0.00      -8.608042304951      -8.608042304951      -8.608042304951      -8.607893589506   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.27977     70
     Pressure of Nuclei (GPa)             0.00000     70
     Pressure Total (GPa)          -150.27977     70


                total energy =       -8.60749213297 Hartree a.u.
              kinetic energy =       11.70561 Hartree a.u.
        electrostatic energy =      -22.99754 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62101 Hartree a.u.
  n-l pseudopotential energy =        2.13414 Hartree a.u.
 exchange-correlation energy =        4.17130 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.28  -13.06   -9.39   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.67390214        -0.23704498        -0.00868694
       -0.23704498      -149.69362197        -0.00139609
       -0.00868694        -0.00139609      -151.47179092
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20646669212746E-01    -0.19192076045544E-01    -0.52412614876154E-02
   H       0.12621538504413E-01     0.24351931856064E-02    -0.12235384128745E-03
   H       0.29548171818987E-02     0.12967408293797E-01    -0.20050332592306E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.000115088836673    0.0    0.00      -8.607492132972      -8.607492132972      -8.607492132972      -8.607377044135   0.0000   0.0000   0.0000   0.0000
     71    0.000089516955818    0.0    0.00      -8.607032255132      -8.607032255132      -8.607032255132      -8.606942738177   0.0000   0.0000   0.0000   0.0000
     72    0.000070599658668    0.0    0.00      -8.606655441180      -8.606655441180      -8.606655441180      -8.606584841521   0.0000   0.0000   0.0000   0.0000
     73    0.000056545094222    0.0    0.00      -8.606374119127      -8.606374119127      -8.606374119127      -8.606317574033   0.0000   0.0000   0.0000   0.0000
     74    0.000045716240561    0.0    0.00      -8.606163226124      -8.606163226124      -8.606163226124      -8.606117509884   0.0000   0.0000   0.0000   0.0000
     75    0.000036971981147    0.0    0.00      -8.605989103759      -8.605989103759      -8.605989103759      -8.605952131777   0.0000   0.0000   0.0000   0.0000
     76    0.000029718660809    0.0    0.00      -8.605826354336      -8.605826354336      -8.605826354336      -8.605796635675   0.0000   0.0000   0.0000   0.0000
     77    0.000023757624521    0.0    0.00      -8.605648684237      -8.605648684237      -8.605648684237      -8.605624926612   0.0000   0.0000   0.0000   0.0000
     78    0.000019035032417    0.0    0.00      -8.605463744153      -8.605463744153      -8.605463744153      -8.605444709121   0.0000   0.0000   0.0000   0.0000
     79    0.000015454119656    0.0    0.00      -8.605278517346      -8.605278517346      -8.605278517346      -8.605263063226   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.31506     80
     Pressure of Nuclei (GPa)             0.00000     80
     Pressure Total (GPa)          -150.31506     80


                total energy =       -8.60507569491 Hartree a.u.
              kinetic energy =       11.71592 Hartree a.u.
        electrostatic energy =      -23.00064 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62262 Hartree a.u.
  n-l pseudopotential energy =        2.12973 Hartree a.u.
 exchange-correlation energy =        4.17254 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.73443982        -0.23738128         0.00539702
       -0.23738128      -149.72946044         0.00411755
        0.00539702         0.00411755      -151.48126973
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17022435401783E-01    -0.18821079662271E-01     0.26323916651544E-02
   H       0.13055423962858E-01     0.28446190721758E-02     0.14644910220128E-03
   H       0.28120330560699E-02     0.12945656715011E-01     0.26348448989200E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.000012814044660    0.0    0.00      -8.605075694909      -8.605075694909      -8.605075694909      -8.605062880864   0.0000   0.0000   0.0000   0.0000
     81    0.000010851566767    0.0    0.00      -8.604890267090      -8.604890267090      -8.604890267090      -8.604879415523   0.0000   0.0000   0.0000   0.0000
     82    0.000009318921529    0.0    0.00      -8.604722296623      -8.604722296623      -8.604722296623      -8.604712977701   0.0000   0.0000   0.0000   0.0000
     83    0.000008038855763    0.0    0.00      -8.604595623158      -8.604595623158      -8.604595623158      -8.604587584302   0.0000   0.0000   0.0000   0.0000
     84    0.000006920142259    0.0    0.00      -8.604521537233      -8.604521537233      -8.604521537233      -8.604514617091   0.0000   0.0000   0.0000   0.0000
     85    0.000005924202434    0.0    0.00      -8.604509226349      -8.604509226349      -8.604509226349      -8.604503302147   0.0000   0.0000   0.0000   0.0000
     86    0.000005034791361    0.0    0.00      -8.604546976503      -8.604546976503      -8.604546976503      -8.604541941712   0.0000   0.0000   0.0000   0.0000
     87    0.000004240322705    0.0    0.00      -8.604625131254      -8.604625131254      -8.604625131254      -8.604620890931   0.0000   0.0000   0.0000   0.0000
     88    0.000003527057829    0.0    0.00      -8.604724141007      -8.604724141007      -8.604724141007      -8.604720613949   0.0000   0.0000   0.0000   0.0000
     89    0.000002886446649    0.0    0.00      -8.604837846449      -8.604837846449      -8.604837846449      -8.604834960002   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30767     90
     Pressure of Nuclei (GPa)             0.00000     90
     Pressure Total (GPa)          -150.30767     90


                total energy =       -8.60495025106 Hartree a.u.
              kinetic energy =       11.71393 Hartree a.u.
        electrostatic energy =      -23.00054 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62266 Hartree a.u.
  n-l pseudopotential energy =        2.13170 Hartree a.u.
 exchange-correlation energy =        4.17263 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72064830        -0.24349303        -0.00183761
       -0.24349303      -149.72159258        -0.00008378
       -0.00183761        -0.00008378      -151.48076984
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18488428850110E-01    -0.17405903821209E-01    -0.44490930204704E-03
   H       0.13220793644303E-01     0.29886436228252E-02     0.97383128791383E-04
   H       0.29076075869648E-02     0.13226401782110E-01     0.93752853223142E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.000002318680121    0.0    0.00      -8.604950251063      -8.604950251063      -8.604950251063      -8.604947932383   0.0000   0.0000   0.0000   0.0000
     91    0.000001829056130    0.0    0.00      -8.605059785240      -8.605059785240      -8.605059785240      -8.605057956184   0.0000   0.0000   0.0000   0.0000
     92    0.000001425992154    0.0    0.00      -8.605153609967      -8.605153609967      -8.605153609967      -8.605152183975   0.0000   0.0000   0.0000   0.0000
     93    0.000001114218905    0.0    0.00      -8.605214789973      -8.605214789973      -8.605214789973      -8.605213675755   0.0000   0.0000   0.0000   0.0000
     94    0.000000886641420    0.0    0.00      -8.605253335869      -8.605253335869      -8.605253335869      -8.605252449228   0.0000   0.0000   0.0000   0.0000
     95    0.000000727599947    0.0    0.00      -8.605259301161      -8.605259301161      -8.605259301161      -8.605258573561   0.0000   0.0000   0.0000   0.0000
     96    0.000000615616152    0.0    0.00      -8.605259581044      -8.605259581044      -8.605259581044      -8.605258965428   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.615616D-06  0.1D-03  0.279883D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:    97
     Pressure of Nuclei (GPa)             0.00000     97
     Pressure Total (GPa)          -150.30715     97


                total energy =       -8.60524543170 Hartree a.u.
              kinetic energy =       11.71360 Hartree a.u.
        electrostatic energy =      -23.00029 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62248 Hartree a.u.
  n-l pseudopotential energy =        2.13145 Hartree a.u.
 exchange-correlation energy =        4.17248 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72074659        -0.24144739         0.00252029
       -0.24144739      -149.72091786         0.00005151
        0.00252029         0.00005151      -151.47977072
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17994379773755E-01    -0.18313426462470E-01     0.55214115344729E-03
   H       0.13151029830299E-01     0.28868945888287E-02    -0.13549849341123E-04
   H       0.29666345291803E-02     0.13138176489892E-01    -0.59875219603861E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
     97    0.000000528996475    0.0    0.00      -8.605245431702      -8.605245431702      -8.605245431702      -8.605244902705   0.0000   0.0000   0.0000   0.0000

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.00s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.75349       0.75349 (AU)
   ekin          :       10.81123      10.81123 (AU)
   epot          :      -20.34265     -20.34265 (AU)
   total energy  :       -7.22871      -7.22871 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :       -7.22871      -7.22871 (AU)
   econs         :       -7.22871      -7.22871 (AU)
   pressure      :     -126.53636    -126.53636 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      1.61s CPU      1.62s WALL (       1 calls)
     main_loop    :     12.51s CPU     12.87s WALL (      97 calls)
     cpr_total    :     12.51s CPU     12.88s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     12.51s CPU     12.88s WALL (      97 calls)
     move_electro :     12.42s CPU     12.77s WALL (      97 calls)

     Called by move_electrons:
     rhoofr       :      4.12s CPU      4.16s WALL (      98 calls)
     vofrho       :      6.84s CPU      7.16s WALL (      98 calls)
     dforce       :      1.56s CPU      1.57s WALL (     196 calls)
     calphi       :      0.01s CPU      0.01s WALL (      98 calls)
     nlfl         :      0.00s CPU      0.00s WALL (      98 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (      98 calls)
     rsg          :      0.01s CPU      0.01s WALL (      98 calls)
     rhoset       :      0.01s CPU      0.01s WALL (      98 calls)
     sigset       :      0.00s CPU      0.01s WALL (      98 calls)
     tauset       :      0.00s CPU      0.00s WALL (      98 calls)
     ortho        :      0.02s CPU      0.02s WALL (      98 calls)
     updatc       :      0.00s CPU      0.00s WALL (      98 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (      98 calls)
     nlfq         :      0.01s CPU      0.01s WALL (      98 calls)
     nlsm1        :      0.00s CPU      0.00s WALL (      99 calls)
     nlsm2        :      0.01s CPU      0.01s WALL (      98 calls)
     fft          :      1.58s CPU      1.66s WALL (     981 calls)
     ffts         :      0.65s CPU      0.66s WALL (     392 calls)
     fftw         :      1.93s CPU      1.94s WALL (    2352 calls)
     fft_scatt_xy :      0.97s CPU      0.98s WALL (    3725 calls)
     fft_scatt_yz :      1.23s CPU      1.26s WALL (    3725 calls)
     fft_scatt_tg :      0.01s CPU      0.01s WALL (     588 calls)
     betagx       :      0.70s CPU      0.70s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      1.46s CPU      1.46s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.04s CPU      0.04s WALL (       1 calls)
     from_scratch :      0.14s CPU      0.15s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.00s CPU      0.00s WALL (      99 calls)


     CP           :     14.18s CPU     14.56s WALL


   This run was terminated on:   2: 8:52   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
