
     Program CP v.6.3MaX starts on  6Jun2021 at  2: 4:42 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Message from routine xml_input_dump:
     no longer available
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation      = SCAN ( 0  0  0  0 0 5)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1


   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Number of Electrons=     8, of States =     4
   Occupation numbers :
   2.00 2.00 2.00 2.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation      = SCAN ( 0  0  0  0 0 5)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   internal stress tensor calculated
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   nbnd =     4
    2.00 2.00 2.00 2.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)      137.22190      0

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    1.119327831744344    0.0    0.00      10.809261641086      10.809261641086      10.809261641086      11.928589472830   0.0000   0.0000   0.0000   0.0000
      2    3.238108059467544    0.0    0.00       7.839808295328       7.839808295328       7.839808295328      11.077916354796   0.0000   0.0000   0.0000   0.0000
      3    6.031788472081899    0.0    0.00       3.262362789045       3.262362789045       3.262362789045       9.294151261127   0.0000   0.0000   0.0000   0.0000
      4    8.576699667858900    0.0    0.00      -2.359891731940      -2.359891731940      -2.359891731940       6.216807935919   0.0000   0.0000   0.0000   0.0000
      5    8.715549962580484    0.0    0.00      -6.204200443078      -6.204200443078      -6.204200443078       2.511349519503   0.0000   0.0000   0.0000   0.0000
      6    6.138094455548520    0.0    0.00      -6.245807374852      -6.245807374852      -6.245807374852      -0.107712919303   0.0000   0.0000   0.0000   0.0000
      7    3.628184252740704    0.0    0.00      -5.502705042226      -5.502705042226      -5.502705042226      -1.874520789485   0.0000   0.0000   0.0000   0.0000
      8    2.736751096747532    0.0    0.00      -5.908457904009      -5.908457904009      -5.908457904009      -3.171706807261   0.0000   0.0000   0.0000   0.0000
      9    2.782770510732807    0.0    0.00      -7.153811069543      -7.153811069543      -7.153811069543      -4.371040558810   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       39.30348     10
     Pressure of Nuclei (GPa)             0.00000     10
     Pressure Total (GPa)            39.30348     10


                total energy =       -8.36512020144 Hartree a.u.
              kinetic energy =        7.39113 Hartree a.u.
        electrostatic energy =      -15.38565 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.30472 Hartree a.u.
  n-l pseudopotential energy =        4.22823 Hartree a.u.
 exchange-correlation energy =       -2.29410 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -88.86  -10.61   -5.27   -2.22


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       48.99841140        -8.10778126        -1.04208233
       -8.10778126        42.17903335         1.64280496
       -1.04208233         1.64280496        26.73300787
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.67454684424193E+00    -0.69278247375722E+00     0.10592591182531E+00
   H       0.11074374763589E+01    -0.24830719665903E+00    -0.43781703061295E-01
   H      -0.29341132333966E+00     0.11342478926401E+01     0.42149880819598E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.821056692362472    0.0    0.00      -8.365120201437      -8.365120201437      -8.365120201437      -5.544063509075   0.0000   0.0000   0.0000   0.0000
     11    2.533977504948472    0.0    0.00      -9.129996954528      -9.129996954528      -9.129996954528      -6.596019449579   0.0000   0.0000   0.0000   0.0000
     12    2.133360493292297    0.0    0.00      -9.635963134540      -9.635963134540      -9.635963134540      -7.502602641248   0.0000   0.0000   0.0000   0.0000
     13    1.829718573763712    0.0    0.00     -10.117854885041     -10.117854885041     -10.117854885041      -8.288136311278   0.0000   0.0000   0.0000   0.0000
     14    1.641760821726957    0.0    0.00     -10.625012882896     -10.625012882896     -10.625012882896      -8.983252061169   0.0000   0.0000   0.0000   0.0000
     15    1.525196373654394    0.0    0.00     -11.136012670715     -11.136012670715     -11.136012670715      -9.610816297061   0.0000   0.0000   0.0000   0.0000
     16    1.475966236278300    0.0    0.00     -11.674793850067     -11.674793850067     -11.674793850067     -10.198827613788   0.0000   0.0000   0.0000   0.0000
     17    1.512785139408440    0.0    0.00     -12.302028308941     -12.302028308941     -12.302028308941     -10.789243169533   0.0000   0.0000   0.0000   0.0000
     18    1.614939392220669    0.0    0.00     -13.032775996095     -13.032775996095     -13.032775996095     -11.417836603875   0.0000   0.0000   0.0000   0.0000
     19    1.710025261519837    0.0    0.00     -13.803717564239     -13.803717564239     -13.803717564239     -12.093692302719   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)        4.59601     20
     Pressure of Nuclei (GPa)             0.00000     20
     Pressure Total (GPa)             4.59601     20


                total energy =      -14.50868354609 Hartree a.u.
              kinetic energy =        9.29445 Hartree a.u.
        electrostatic energy =      -20.67969 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.93443 Hartree a.u.
  n-l pseudopotential energy =        3.35935 Hartree a.u.
 exchange-correlation energy =       -3.54836 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -41.42  -24.68  -19.63   -0.18


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
        3.62793748        -5.89345824         0.77270621
       -5.89345824         8.41616509         0.55155477
        0.77270621         0.55155477         1.74391723
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.35991793609346E+00    -0.65948083757429E-01     0.79405026526401E+00
   H       0.22422936219454E+00     0.58725320826884E-01     0.12360842565375E+00
   H      -0.13725968460173E+00     0.30968371792365E+00     0.90197830050924E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.719622067499005    0.0    0.00     -14.508683546087     -14.508683546087     -14.508683546087     -12.789061478588   0.0000   0.0000   0.0000   0.0000
     21    1.610356360207439    0.0    0.00     -15.069094304457     -15.069094304457     -15.069094304457     -13.458737944249   0.0000   0.0000   0.0000   0.0000
     22    1.404770614060070    0.0    0.00     -15.464068165050     -15.464068165050     -15.464068165050     -14.059297550989   0.0000   0.0000   0.0000   0.0000
     23    1.156333005736188    0.0    0.00     -15.722002684422     -15.722002684422     -15.722002684422     -14.565669678686   0.0000   0.0000   0.0000   0.0000
     24    0.917844820353134    0.0    0.00     -15.892094678676     -15.892094678676     -15.892094678676     -14.974249858323   0.0000   0.0000   0.0000   0.0000
     25    0.720514346898804    0.0    0.00     -16.018010631165     -16.018010631165     -16.018010631165     -15.297496284267   0.0000   0.0000   0.0000   0.0000
     26    0.571763816263593    0.0    0.00     -16.125132824416     -16.125132824416     -16.125132824416     -15.553369008152   0.0000   0.0000   0.0000   0.0000
     27    0.464888967516876    0.0    0.00     -16.224200449534     -16.224200449534     -16.224200449534     -15.759311482017   0.0000   0.0000   0.0000   0.0000
     28    0.388731662074806    0.0    0.00     -16.317992036887     -16.317992036887     -16.317992036887     -15.929260374812   0.0000   0.0000   0.0000   0.0000
     29    0.333932251379517    0.0    0.00     -16.407118883725     -16.407118883725     -16.407118883725     -16.073186632346   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -0.92617     30
     Pressure of Nuclei (GPa)             0.00000     30
     Pressure Total (GPa)            -0.92617     30


                total energy =      -16.49238336207 Hartree a.u.
              kinetic energy =       12.66866 Hartree a.u.
        electrostatic energy =      -23.23844 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.83407 Hartree a.u.
  n-l pseudopotential energy =        2.03278 Hartree a.u.
 exchange-correlation energy =       -4.12132 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -28.40  -16.02  -11.90   -6.63


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
        0.48619632        -2.09524592         0.72498654
       -2.09524592         2.15154779         0.39298980
        0.72498654         0.39298980        -5.41625352
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.44340048312354E+00    -0.13249497126059E-01    -0.28518639963077E+00
   H      -0.76812329707136E-03    -0.26149560260527E-01     0.29214665173617E-01
   H      -0.40384605525831E-02     0.31335431196766E-01     0.20094537980005E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.293851536885311    0.0    0.00     -16.492383362070     -16.492383362070     -16.492383362070     -16.198531825184   0.0000   0.0000   0.0000   0.0000
     31    0.263020643103943    0.0    0.00     -16.572963482827     -16.572963482827     -16.572963482827     -16.309942839723   0.0000   0.0000   0.0000   0.0000
     32    0.236887339063833    0.0    0.00     -16.647045568583     -16.647045568583     -16.647045568583     -16.410158229519   0.0000   0.0000   0.0000   0.0000
     33    0.212035801076514    0.0    0.00     -16.712206675028     -16.712206675028     -16.712206675028     -16.500170873952   0.0000   0.0000   0.0000   0.0000
     34    0.186553451009455    0.0    0.00     -16.766523927818     -16.766523927818     -16.766523927818     -16.579970476809   0.0000   0.0000   0.0000   0.0000
     35    0.160182736260499    0.0    0.00     -16.809392175240     -16.809392175240     -16.809392175240     -16.649209438979   0.0000   0.0000   0.0000   0.0000
     36    0.133924909442677    0.0    0.00     -16.841684048687     -16.841684048687     -16.841684048687     -16.707759139244   0.0000   0.0000   0.0000   0.0000
     37    0.109268551741101    0.0    0.00     -16.865310181119     -16.865310181119     -16.865310181119     -16.756041629378   0.0000   0.0000   0.0000   0.0000
     38    0.087514515606379    0.0    0.00     -16.882527068293     -16.882527068293     -16.882527068293     -16.795012552687   0.0000   0.0000   0.0000   0.0000
     39    0.069557465673056    0.0    0.00     -16.895664692226     -16.895664692226     -16.895664692226     -16.826107226553   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.25402     40
     Pressure of Nuclei (GPa)             0.00000     40
     Pressure Total (GPa)            -8.25402     40


                total energy =      -16.90649275294 Hartree a.u.
              kinetic energy =       11.60719 Hartree a.u.
        electrostatic energy =      -22.94498 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.59876 Hartree a.u.
  n-l pseudopotential energy =        2.17328 Hartree a.u.
 exchange-correlation energy =       -4.14322 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.96  -13.37   -9.91   -7.61


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.88463148         0.07950812        -0.36942192
        0.07950812        -8.08381111         0.12630552
       -0.36942192         0.12630552        -8.79362015
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.11793887388038E+00    -0.50580648352365E-01     0.65562564789146E-01
   H       0.14206981365403E-01     0.87495269130136E-02     0.59495491620908E-03
   H       0.28179347372867E-02    -0.28908102328399E-02     0.19575790800831E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.055567795630703    0.0    0.00     -16.906492752941     -16.906492752941     -16.906492752941     -16.850924957310   0.0000   0.0000   0.0000   0.0000
     41    0.044997664386513    0.0    0.00     -16.915931991419     -16.915931991419     -16.915931991419     -16.870934327033   0.0000   0.0000   0.0000   0.0000
     42    0.036938642319781    0.0    0.00     -16.924234508482     -16.924234508482     -16.924234508482     -16.887295866162   0.0000   0.0000   0.0000   0.0000
     43    0.030443487228240    0.0    0.00     -16.931221533465     -16.931221533465     -16.931221533465     -16.900778046237   0.0000   0.0000   0.0000   0.0000
     44    0.024801295430317    0.0    0.00     -16.936624732056     -16.936624732056     -16.936624732056     -16.911823436625   0.0000   0.0000   0.0000   0.0000
     45    0.019666273991007    0.0    0.00     -16.940348041668     -16.940348041668     -16.940348041668     -16.920681767677   0.0000   0.0000   0.0000   0.0000
     46    0.015021375033130    0.0    0.00     -16.942573869378     -16.942573869378     -16.942573869378     -16.927552494345   0.0000   0.0000   0.0000   0.0000
     47    0.011031750862788    0.0    0.00     -16.943713057734     -16.943713057734     -16.943713057734     -16.932681306872   0.0000   0.0000   0.0000   0.0000
     48    0.007872451040041    0.0    0.00     -16.944258458440     -16.944258458440     -16.944258458440     -16.936386007400   0.0000   0.0000   0.0000   0.0000
     49    0.005608299360216    0.0    0.00     -16.944635109431     -16.944635109431     -16.944635109431     -16.939026810071   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.05605     50
     Pressure of Nuclei (GPa)             0.00000     50
     Pressure Total (GPa)            -8.05605     50


                total energy =      -16.94510926833 Hartree a.u.
              kinetic energy =       11.68630 Hartree a.u.
        electrostatic energy =      -22.98928 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62332 Hartree a.u.
  n-l pseudopotential energy =        2.14852 Hartree a.u.
 exchange-correlation energy =       -4.16733 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.44  -13.08   -9.39   -7.31


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.40398719        -0.31266007         0.02928459
       -0.31266007        -7.41214861         0.00402039
        0.02928459         0.00402039        -9.35201169
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.61248757557321E-01    -0.30622540912924E-01    -0.20008437226429E-01
   H       0.27628760547974E-01    -0.30020581107276E-03     0.22800017968669E-02
   H       0.11941445870010E-02     0.33698757946088E-01    -0.72147195817597E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.004158607449803    0.0    0.00     -16.945109268327     -16.945109268327     -16.945109268327     -16.940950660878   0.0000   0.0000   0.0000   0.0000
     51    0.003332825781325    0.0    0.00     -16.945774188572     -16.945774188572     -16.945774188572     -16.942441362790   0.0000   0.0000   0.0000   0.0000
     52    0.002900049956984    0.0    0.00     -16.946593893074     -16.946593893074     -16.946593893074     -16.943693843117   0.0000   0.0000   0.0000   0.0000
     53    0.002654043738877    0.0    0.00     -16.947469505550     -16.947469505550     -16.947469505550     -16.944815461812   0.0000   0.0000   0.0000   0.0000
     54    0.002451172344058    0.0    0.00     -16.948297196748     -16.948297196748     -16.948297196748     -16.945846024404   0.0000   0.0000   0.0000   0.0000
     55    0.002218229463520    0.0    0.00     -16.949003265267     -16.949003265267     -16.949003265267     -16.946785035804   0.0000   0.0000   0.0000   0.0000
     56    0.001939377407489    0.0    0.00     -16.949556115777     -16.949556115777     -16.949556115777     -16.947616738369   0.0000   0.0000   0.0000   0.0000
     57    0.001633998613667    0.0    0.00     -16.949961773720     -16.949961773720     -16.949961773720     -16.948327775107   0.0000   0.0000   0.0000   0.0000
     58    0.001334663828471    0.0    0.00     -16.950250611511     -16.950250611511     -16.950250611511     -16.948915947682   0.0000   0.0000   0.0000   0.0000
     59    0.001070398944599    0.0    0.00     -16.950461804468     -16.950461804468     -16.950461804468     -16.949391405523   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.17886     60
     Pressure of Nuclei (GPa)             0.00000     60
     Pressure Total (GPa)            -8.17886     60


                total energy =      -16.95063010348 Hartree a.u.
              kinetic energy =       11.73437 Hartree a.u.
        electrostatic energy =      -23.00769 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62535 Hartree a.u.
  n-l pseudopotential energy =        2.12428 Hartree a.u.
 exchange-correlation energy =       -4.17625 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.20  -13.05   -9.34   -6.98


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.63085654        -0.25978779        -0.00215639
       -0.25978779        -7.58804141        -0.02357809
       -0.00215639        -0.02357809        -9.31768383
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.68676029693708E-02    -0.95348595165226E-02     0.39472734407062E-02
   H       0.10995340497436E-01     0.46571374395012E-02    -0.22534091539715E-02
   H       0.34928393159935E-02     0.97863742589769E-02    -0.55377930168459E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.000857364861122    0.0    0.00     -16.950630103482     -16.950630103482     -16.950630103482     -16.949772738620   0.0000   0.0000   0.0000   0.0000
     61    0.000697041900881    0.0    0.00     -16.950778259130     -16.950778259130     -16.950778259130     -16.950081217229   0.0000   0.0000   0.0000   0.0000
     62    0.000580007165328    0.0    0.00     -16.950915794520     -16.950915794520     -16.950915794520     -16.950335787354   0.0000   0.0000   0.0000   0.0000
     63    0.000492171343697    0.0    0.00     -16.951042454441     -16.951042454441     -16.951042454441     -16.950550283097   0.0000   0.0000   0.0000   0.0000
     64    0.000420525406418    0.0    0.00     -16.951153625186     -16.951153625186     -16.951153625186     -16.950733099779   0.0000   0.0000   0.0000   0.0000
     65    0.000356493440049    0.0    0.00     -16.951245045566     -16.951245045566     -16.951245045566     -16.950888552126   0.0000   0.0000   0.0000   0.0000
     66    0.000296514490736    0.0    0.00     -16.951315338510     -16.951315338510     -16.951315338510     -16.951018824019   0.0000   0.0000   0.0000   0.0000
     67    0.000240709171715    0.0    0.00     -16.951366341328     -16.951366341328     -16.951366341328     -16.951125632156   0.0000   0.0000   0.0000   0.0000
     68    0.000190877076981    0.0    0.00     -16.951402055520     -16.951402055520     -16.951402055520     -16.951211178443   0.0000   0.0000   0.0000   0.0000
     69    0.000148788126718    0.0    0.00     -16.951427166497     -16.951427166497     -16.951427166497     -16.951278378370   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.21527     70
     Pressure of Nuclei (GPa)             0.00000     70
     Pressure Total (GPa)            -8.21527     70


                total energy =      -16.95144581153 Hartree a.u.
              kinetic energy =       11.70560 Hartree a.u.
        electrostatic energy =      -22.99754 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62101 Hartree a.u.
  n-l pseudopotential energy =        2.13414 Hartree a.u.
 exchange-correlation energy =       -4.17265 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.28  -13.06   -9.39   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.60940381        -0.23706847        -0.00870009
       -0.23706847        -7.62911806        -0.00138848
       -0.00870009        -0.00138848        -9.40728044
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20647176752718E-01    -0.19191038268773E-01    -0.52383038479017E-02
   H       0.12621848831726E-01     0.24350264202277E-02    -0.12217243854973E-03
   H       0.29543338413035E-02     0.12967892720557E-01    -0.20027441343796E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.000115205565282    0.0    0.00     -16.951445811525     -16.951445811525     -16.951445811525     -16.951330605960   0.0000   0.0000   0.0000   0.0000
     71    0.000089655907343    0.0    0.00     -16.951460862750     -16.951460862750     -16.951460862750     -16.951371206843   0.0000   0.0000   0.0000   0.0000
     72    0.000070732538734    0.0    0.00     -16.951473822395     -16.951473822395     -16.951473822395     -16.951403089856   0.0000   0.0000   0.0000   0.0000
     73    0.000056651191146    0.0    0.00     -16.951485129101     -16.951485129101     -16.951485129101     -16.951428477910   0.0000   0.0000   0.0000   0.0000
     74    0.000045787488597    0.0    0.00     -16.951494707331     -16.951494707331     -16.951494707331     -16.951448919842   0.0000   0.0000   0.0000   0.0000
     75    0.000037008030231    0.0    0.00     -16.951502430281     -16.951502430281     -16.951502430281     -16.951465422251   0.0000   0.0000   0.0000   0.0000
     76    0.000029727016373    0.0    0.00     -16.951508414790     -16.951508414790     -16.951508414790     -16.951478687773   0.0000   0.0000   0.0000   0.0000
     77    0.000023744970689    0.0    0.00     -16.951513034553     -16.951513034553     -16.951513034553     -16.951489289583   0.0000   0.0000   0.0000   0.0000
     78    0.000019007403040    0.0    0.00     -16.951516769242     -16.951516769242     -16.951516769242     -16.951497761839   0.0000   0.0000   0.0000   0.0000
     79    0.000015418782928    0.0    0.00     -16.951520011922     -16.951520011922     -16.951520011922     -16.951504593139   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       -8.20811     80
     Pressure of Nuclei (GPa)             0.00000     80
     Pressure Total (GPa)            -8.20811     80


                total energy =      -16.95152298230 Hartree a.u.
              kinetic energy =       11.71591 Hartree a.u.
        electrostatic energy =      -23.00064 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62262 Hartree a.u.
  n-l pseudopotential energy =        2.12973 Hartree a.u.
 exchange-correlation energy =       -4.17391 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.62751083        -0.23739077         0.00539672
       -0.23739077        -7.62252299         0.00410352
        0.00539672         0.00410352        -9.37429668
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17021683417324E-01    -0.18821530113104E-01     0.26297617562805E-02
   H       0.13056177695461E-01     0.28441998559698E-02     0.14645032784008E-03
   H       0.28118390926891E-02     0.12946216455311E-01     0.26305305833990E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.000012778075581    0.0    0.00     -16.951522982304     -16.951522982304     -16.951522982304     -16.951510204229   0.0000   0.0000   0.0000   0.0000
     81    0.000010819381095    0.0    0.00     -16.951525735341     -16.951525735341     -16.951525735341     -16.951514915960   0.0000   0.0000   0.0000   0.0000
     82    0.000009292369498    0.0    0.00     -16.951528229163     -16.951528229163     -16.951528229163     -16.951518936793   0.0000   0.0000   0.0000   0.0000
     83    0.000008019825326    0.0    0.00     -16.951530416324     -16.951530416324     -16.951530416324     -16.951522396499   0.0000   0.0000   0.0000   0.0000
     84    0.000006907160832    0.0    0.00     -16.951532283540     -16.951532283540     -16.951532283540     -16.951525376379   0.0000   0.0000   0.0000   0.0000
     85    0.000005916343411    0.0    0.00     -16.951533850942     -16.951533850942     -16.951533850942     -16.951527934599   0.0000   0.0000   0.0000   0.0000
     86    0.000005032161423    0.0    0.00     -16.951535151025     -16.951535151025     -16.951535151025     -16.951530118864   0.0000   0.0000   0.0000   0.0000
     87    0.000004241644443    0.0    0.00     -16.951536211202     -16.951536211202     -16.951536211202     -16.951531969557   0.0000   0.0000   0.0000   0.0000
     88    0.000003530701299    0.0    0.00     -16.951537050908     -16.951537050908     -16.951537050908     -16.951533520207   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.353070D-05  0.1D-03  0.839706D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:    89
     Pressure of Nuclei (GPa)             0.00000     89
     Pressure Total (GPa)            -8.19624     89


                total energy =      -16.95153768934 Hartree a.u.
              kinetic energy =       11.71423 Hartree a.u.
        electrostatic energy =      -23.00071 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62276 Hartree a.u.
  n-l pseudopotential energy =        2.13172 Hartree a.u.
 exchange-correlation energy =       -4.17402 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.36   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
       -7.60971361        -0.24344140        -0.00310562
       -0.24344140        -7.61058789         0.00033411
       -0.00310562         0.00033411        -9.36841011
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18362360332837E-01    -0.17398823717986E-01    -0.74825252525285E-03
   H       0.13197333888196E-01     0.29990962460362E-02     0.11678369095904E-03
   H       0.28794042849885E-02     0.13242352565580E-01     0.11998157960661E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
     89    0.000002889939209    0.0    0.00     -16.951537689341     -16.951537689341     -16.951537689341     -16.951534799402   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.288994D-05  0.1D-03  0.638433D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.00s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.82121       0.82121 (AU)
   ekin          :       10.73011      10.73011 (AU)
   epot          :      -27.58614     -27.58614 (AU)
   total energy  :      -14.53792     -14.53792 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :      -14.53792     -14.53792 (AU)
   econs         :      -14.53792     -14.53792 (AU)
   pressure      :        2.15389       2.15389 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      1.37s CPU      1.38s WALL (       1 calls)
     main_loop    :     11.97s CPU     12.25s WALL (      89 calls)
     cpr_total    :     11.98s CPU     12.25s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     11.98s CPU     12.25s WALL (      89 calls)
     move_electro :     11.87s CPU     12.14s WALL (      89 calls)

     Called by move_electrons:
     rhoofr       :      3.99s CPU      4.02s WALL (      90 calls)
     vofrho       :      6.53s CPU      6.76s WALL (      90 calls)
     dforce       :      1.47s CPU      1.48s WALL (     180 calls)
     calphi       :      0.00s CPU      0.00s WALL (      90 calls)
     nlfl         :      0.00s CPU      0.00s WALL (      90 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (      90 calls)
     rsg          :      0.01s CPU      0.01s WALL (      90 calls)
     rhoset       :      0.01s CPU      0.01s WALL (      90 calls)
     sigset       :      0.00s CPU      0.00s WALL (      90 calls)
     tauset       :      0.00s CPU      0.00s WALL (      90 calls)
     ortho        :      0.02s CPU      0.02s WALL (      90 calls)
     updatc       :      0.00s CPU      0.00s WALL (      90 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (      90 calls)
     nlfq         :      0.01s CPU      0.01s WALL (      90 calls)
     nlsm1        :      0.00s CPU      0.00s WALL (      91 calls)
     nlsm2        :      0.01s CPU      0.01s WALL (      90 calls)
     fft          :      1.52s CPU      1.58s WALL (     901 calls)
     ffts         :      0.66s CPU      0.66s WALL (     360 calls)
     fftw         :      1.88s CPU      1.90s WALL (    2160 calls)
     fft_scatt_xy :      0.91s CPU      0.93s WALL (    3421 calls)
     fft_scatt_yz :      1.31s CPU      1.34s WALL (    3421 calls)
     fft_scatt_tg :      0.01s CPU      0.01s WALL (     540 calls)
     betagx       :      0.56s CPU      0.56s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      1.20s CPU      1.20s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.02s CPU      0.02s WALL (       1 calls)
     from_scratch :      0.15s CPU      0.17s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.00s CPU      0.00s WALL (      91 calls)


     CP           :    13.38s CPU        13.67s WALL


   This run was terminated on:   2: 4:56   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
