
     Program CP v.6.3MaX starts on  6Jun2021 at  2: 4:56 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Message from routine xml_input_dump:
     no longer available
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation      = SCAN ( 0  0  0  0 0 5)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1


   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Local Spin Density calculation
   Number of Electrons=     8
   Spins up   =     4, occupations: 
   1.00 1.00 1.00 1.00
   Spins down =     4, occupations: 
   1.00 1.00 1.00 1.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation      = SCAN ( 0  0  0  0 0 5)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   spin =   1 nbnd =     4
    1.00 1.00 1.00 1.00
   spin =   2 nbnd =     4
    1.00 1.00 1.00 1.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.611973367288878    0.0    0.00      11.235136143237      11.235136143237      11.235136143237      11.847109510526   0.0000   0.0000   0.0000   0.0000
      2    1.763463188865191    0.0    0.00       9.605227375784       9.605227375784       9.605227375784      11.368690564649   0.0000   0.0000   0.0000   0.0000
      3    3.257016713126363    0.0    0.00       7.110901100211       7.110901100211       7.110901100211      10.367917813337   0.0000   0.0000   0.0000   0.0000
      4    4.878844510726762    0.0    0.00       3.865457963260       3.865457963260       3.865457963260       8.744302473987   0.0000   0.0000   0.0000   0.0000
      5    6.377904902999807    0.0    0.00       0.080998065416       0.080998065416       0.080998065416       6.458902968415   0.0000   0.0000   0.0000   0.0000
      6    7.269264558960192    0.0    0.00      -3.633364288574      -3.633364288574      -3.633364288574       3.635900270386   0.0000   0.0000   0.0000   0.0000
      7    7.015931751261872    0.0    0.00      -6.301517188916      -6.301517188916      -6.301517188916       0.714414562346   0.0000   0.0000   0.0000   0.0000
      8    5.649547379297056    0.0    0.00      -7.423718032807      -7.423718032807      -7.423718032807      -1.774170653510   0.0000   0.0000   0.0000   0.0000
      9    3.904177938118456    0.0    0.00      -7.493309122795      -7.493309122795      -7.493309122795      -3.589131184677   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.93 (Slater)  1.55 (Becke)  0.00 (expected)


                total energy =       -7.35751190563 Hartree a.u.
              kinetic energy =        5.64397 Hartree a.u.
        electrostatic energy =      -12.31972 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -1.58163 Hartree a.u.
  n-l pseudopotential energy =        2.83423 Hartree a.u.
 exchange-correlation energy =       -1.93436 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -83.05   -8.49   -3.53    2.04

   Eigenvalues (eV), kp =   1 , spin =  2

  -78.09  -24.62   -5.61   -2.26


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.58831848383930E+00    -0.10190331770270E+01    -0.19760170308916E+00
   H       0.12088309467632E+01    -0.20128099764090E+00    -0.22558421361305E+00
   H      -0.19930372454459E+00     0.12066141367677E+01     0.93651875359100E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.545543125780801    0.0    0.00      -7.357511905625      -7.357511905625      -7.357511905625      -4.811968779844   0.0000   0.0000   0.0000   0.0000
     11    1.843624274905092    0.0    0.00      -7.513639689564      -7.513639689564      -7.513639689564      -5.670015414659   0.0000   0.0000   0.0000   0.0000
     12    1.647203551348969    0.0    0.00      -8.027466195024      -8.027466195024      -8.027466195024      -6.380262643675   0.0000   0.0000   0.0000   0.0000
     13    1.669733922054968    0.0    0.00      -8.735453789407      -8.735453789407      -8.735453789407      -7.065719867352   0.0000   0.0000   0.0000   0.0000
     14    1.688001127331364    0.0    0.00      -9.439306174903      -9.439306174903      -9.439306174903      -7.751305047572   0.0000   0.0000   0.0000   0.0000
     15    1.618170284121678    0.0    0.00     -10.031680270918     -10.031680270918     -10.031680270918      -8.413509986796   0.0000   0.0000   0.0000   0.0000
     16    1.487027470169788    0.0    0.00     -10.514938050880     -10.514938050880     -10.514938050880      -9.027910580710   0.0000   0.0000   0.0000   0.0000
     17    1.353701657423918    0.0    0.00     -10.943227776173     -10.943227776173     -10.943227776173      -9.589526118749   0.0000   0.0000   0.0000   0.0000
     18    1.251802575140328    0.0    0.00     -11.359736451975     -11.359736451975     -11.359736451975     -10.107933876835   0.0000   0.0000   0.0000   0.0000
     19    1.179494890901138    0.0    0.00     -11.774110042152     -11.774110042152     -11.774110042152     -10.594615151251   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.25 (Slater)  1.39 (Becke)  0.00 (expected)


                total energy =      -12.17404333283 Hartree a.u.
              kinetic energy =        7.14561 Hartree a.u.
        electrostatic energy =      -17.87214 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.59876 Hartree a.u.
  n-l pseudopotential energy =        4.03153 Hartree a.u.
 exchange-correlation energy =       -2.88028 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -60.84  -10.27   -0.76   -0.06

   Eigenvalues (eV), kp =   1 , spin =  2

  -62.07  -33.06  -21.53   -3.13


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.60441975656174E+00     0.35733409803082E+00    -0.29151630354571E+00
   H       0.72885761651906E+00    -0.96019101827879E-01    -0.18396692743280E+00
   H      -0.45305111080030E-01     0.87580347764801E+00    -0.82063087865379E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.118620390061853    0.0    0.00     -12.174043332833     -12.174043332833     -12.174043332833     -11.055422942771   0.0000   0.0000   0.0000   0.0000
     21    1.054725893572533    0.0    0.00     -12.545149769231     -12.545149769231     -12.545149769231     -11.490423875658   0.0000   0.0000   0.0000   0.0000
     22    0.985121065682841    0.0    0.00     -12.882314009438     -12.882314009438     -12.882314009438     -11.897192943755   0.0000   0.0000   0.0000   0.0000
     23    0.916830139851054    0.0    0.00     -13.192385148969     -13.192385148969     -13.192385148969     -12.275555009118   0.0000   0.0000   0.0000   0.0000
     24    0.858471943098205    0.0    0.00     -13.487161177658     -13.487161177658     -13.487161177658     -12.628689234560   0.0000   0.0000   0.0000   0.0000
     25    0.814919319017998    0.0    0.00     -13.776658796472     -13.776658796472     -13.776658796472     -12.961739477454   0.0000   0.0000   0.0000   0.0000
     26    0.786626077441460    0.0    0.00     -14.067894331040     -14.067894331040     -14.067894331040     -13.281268253598   0.0000   0.0000   0.0000   0.0000
     27    0.769245222480079    0.0    0.00     -14.361700240251     -14.361700240251     -14.361700240251     -13.592455017771   0.0000   0.0000   0.0000   0.0000
     28    0.755422589322312    0.0    0.00     -14.653329797618     -14.653329797618     -14.653329797618     -13.897907208296   0.0000   0.0000   0.0000   0.0000
     29    0.737727781471658    0.0    0.00     -14.935124778396     -14.935124778396     -14.935124778396     -14.197396996924   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 1.33 (Slater)  1.11 (Becke)  0.00 (expected)


                total energy =      -15.19792260358 Hartree a.u.
              kinetic energy =        9.30123 Hartree a.u.
        electrostatic energy =      -21.12160 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.13850 Hartree a.u.
  n-l pseudopotential energy =        3.32424 Hartree a.u.
 exchange-correlation energy =       -3.56330 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -41.11  -18.26  -15.85    0.24

   Eigenvalues (eV), kp =   1 , spin =  2

  -43.19  -25.78  -22.22   -4.49


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.56596615368896E-01     0.19414105826221E+00     0.76211744032134E+00
   H       0.28950168374167E+00    -0.58258631105359E-01     0.78266871044793E-01
   H      -0.96268375565277E-01     0.26083408853325E+00     0.92862781784563E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.710060930378951    0.0    0.00     -15.197922603577     -15.197922603577     -15.197922603577     -14.487861673198   0.0000   0.0000   0.0000   0.0000
     31    0.668895194268411    0.0    0.00     -15.433190293026     -15.433190293026     -15.433190293026     -14.764295098757   0.0000   0.0000   0.0000   0.0000
     32    0.614333017487662    0.0    0.00     -15.635563838906     -15.635563838906     -15.635563838906     -15.021230821418   0.0000   0.0000   0.0000   0.0000
     33    0.549300207743259    0.0    0.00     -15.803092714851     -15.803092714851     -15.803092714851     -15.253792507108   0.0000   0.0000   0.0000   0.0000
     34    0.478542743536732    0.0    0.00     -15.937324984005     -15.937324984005     -15.937324984005     -15.458782240469   0.0000   0.0000   0.0000   0.0000
     35    0.407595508447774    0.0    0.00     -16.042752186394     -16.042752186394     -16.042752186394     -15.635156677946   0.0000   0.0000   0.0000   0.0000
     36    0.341404210352013    0.0    0.00     -16.125391501040     -16.125391501040     -16.125391501040     -15.783987290688   0.0000   0.0000   0.0000   0.0000
     37    0.283454312259511    0.0    0.00     -16.191482931030     -16.191482931030     -16.191482931030     -15.908028618771   0.0000   0.0000   0.0000   0.0000
     38    0.235502699703448    0.0    0.00     -16.246507406026     -16.246507406026     -16.246507406026     -16.011004706323   0.0000   0.0000   0.0000   0.0000
     39    0.197763663026762    0.0    0.00     -16.294789100096     -16.294789100096     -16.294789100096     -16.097025437069   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.69 (Slater)  0.87 (Becke)  0.00 (expected)


                total energy =      -16.33929565883 Hartree a.u.
              kinetic energy =       11.36520 Hartree a.u.
        electrostatic energy =      -22.66984 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.65694 Hartree a.u.
  n-l pseudopotential energy =        2.56786 Hartree a.u.
 exchange-correlation energy =       -3.94556 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -32.69  -17.85  -14.27   -1.85

   Eigenvalues (eV), kp =   1 , spin =  2

  -34.15  -20.06  -16.70   -8.87


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13639190687288E+00    -0.28357677240002E+00    -0.83347907605539E-01
   H       0.13209441619153E+00    -0.34546628351905E-01     0.49890579814449E-01
   H      -0.31749430954130E-01     0.12678120331558E+00    -0.27533225414128E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.169304989124327    0.0    0.00     -16.339295658829     -16.339295658829     -16.339295658829     -16.169990669705   0.0000   0.0000   0.0000   0.0000
     41    0.148499169023510    0.0    0.00     -16.381776124655     -16.381776124655     -16.381776124655     -16.233276955631   0.0000   0.0000   0.0000   0.0000
     42    0.133435295650792    0.0    0.00     -16.422958143913     -16.422958143913     -16.422958143913     -16.289522848263   0.0000   0.0000   0.0000   0.0000
     43    0.122269279045112    0.0    0.00     -16.462885867243     -16.462885867243     -16.462885867243     -16.340616588197   0.0000   0.0000   0.0000   0.0000
     44    0.113449449152328    0.0    0.00     -16.501212084698     -16.501212084698     -16.501212084698     -16.387762635546   0.0000   0.0000   0.0000   0.0000
     45    0.105822748891424    0.0    0.00     -16.537467729998     -16.537467729998     -16.537467729998     -16.431644981107   0.0000   0.0000   0.0000   0.0000
     46    0.098628630476660    0.0    0.00     -16.571192630699     -16.571192630699     -16.571192630699     -16.472564000223   0.0000   0.0000   0.0000   0.0000
     47    0.091457235691880    0.0    0.00     -16.602051364078     -16.602051364078     -16.602051364078     -16.510594128386   0.0000   0.0000   0.0000   0.0000
     48    0.084182227800048    0.0    0.00     -16.629903025234     -16.629903025234     -16.629903025234     -16.545720797434   0.0000   0.0000   0.0000   0.0000
     49    0.076838545167998    0.0    0.00     -16.654744178366     -16.654744178366     -16.654744178366     -16.577905633198   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.67 (Slater)  0.77 (Becke)  0.00 (expected)


                total energy =      -16.67668618450 Hartree a.u.
              kinetic energy =       11.54988 Hartree a.u.
        electrostatic energy =      -22.85246 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.64376 Hartree a.u.
  n-l pseudopotential energy =        2.29619 Hartree a.u.
 exchange-correlation energy =       -4.02653 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -30.35  -16.84  -12.93   -3.21

   Eigenvalues (eV), kp =   1 , spin =  2

  -32.04  -17.27  -14.10  -11.59


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.82239505437574E-01    -0.10950194401554E+00    -0.21218007593374E-01
   H       0.51723600477259E-01    -0.15038001748997E-01     0.53849773472319E-01
   H      -0.22534949551776E-01     0.88209483393537E-01     0.22728377184826E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.069535212235844    0.0    0.00     -16.676686184500     -16.676686184500     -16.676686184500     -16.607150972264   0.0000   0.0000   0.0000   0.0000
     51    0.062393299586595    0.0    0.00     -16.695895299365     -16.695895299365     -16.695895299365     -16.633501999778   0.0000   0.0000   0.0000   0.0000
     52    0.055519771463288    0.0    0.00     -16.712560541533     -16.712560541533     -16.712560541533     -16.657040770069   0.0000   0.0000   0.0000   0.0000
     53    0.049036268904175    0.0    0.00     -16.726933185593     -16.726933185593     -16.726933185593     -16.677896916689   0.0000   0.0000   0.0000   0.0000
     54    0.043075281522412    0.0    0.00     -16.739337081147     -16.739337081147     -16.739337081147     -16.696261799624   0.0000   0.0000   0.0000   0.0000
     55    0.037737410538416    0.0    0.00     -16.750105888339     -16.750105888339     -16.750105888339     -16.712368477801   0.0000   0.0000   0.0000   0.0000
     56    0.033069857287486    0.0    0.00     -16.759547715234     -16.759547715234     -16.759547715234     -16.726477857947   0.0000   0.0000   0.0000   0.0000
     57    0.029076457535691    0.0    0.00     -16.767936503547     -16.767936503547     -16.767936503547     -16.738860046011   0.0000   0.0000   0.0000   0.0000
     58    0.025734216947032    0.0    0.00     -16.775516094088     -16.775516094088     -16.775516094088     -16.749781877141   0.0000   0.0000   0.0000   0.0000
     59    0.022986708453845    0.0    0.00     -16.782480905848     -16.782480905848     -16.782480905848     -16.759494197394   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.68 (Slater)  0.70 (Becke)  0.00 (expected)


                total energy =      -16.78896922808 Hartree a.u.
              kinetic energy =       11.45048 Hartree a.u.
        electrostatic energy =      -22.85550 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60974 Hartree a.u.
  n-l pseudopotential energy =        2.27345 Hartree a.u.
 exchange-correlation energy =       -4.04766 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -29.90  -15.96  -12.34   -4.19

   Eigenvalues (eV), kp =   1 , spin =  2

  -31.32  -16.06  -12.79  -11.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.48597991820163E-01    -0.73422926154595E-01     0.32003507761977E-01
   H       0.35470993922003E-01    -0.82216071349527E-02     0.27218647321744E-01
   H      -0.14461318920918E-01     0.63835436781884E-01     0.26596560523543E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.020753097134457    0.0    0.00     -16.788969228075     -16.788969228075     -16.788969228075     -16.768216130941   0.0000   0.0000   0.0000   0.0000
     61    0.018950700080130    0.0    0.00     -16.795086274153     -16.795086274153     -16.795086274153     -16.776135574073   0.0000   0.0000   0.0000   0.0000
     62    0.017506405671618    0.0    0.00     -16.800915853916     -16.800915853916     -16.800915853916     -16.783409448244   0.0000   0.0000   0.0000   0.0000
     63    0.016358539159080    0.0    0.00     -16.806526440442     -16.806526440442     -16.806526440442     -16.790167901283   0.0000   0.0000   0.0000   0.0000
     64    0.015454878559959    0.0    0.00     -16.811973775859     -16.811973775859     -16.811973775859     -16.796518897299   0.0000   0.0000   0.0000   0.0000
     65    0.014750486792847    0.0    0.00     -16.817300933510     -16.817300933510     -16.817300933510     -16.802550446717   0.0000   0.0000   0.0000   0.0000
     66    0.014207934749618    0.0    0.00     -16.822542318434     -16.822542318434     -16.822542318434     -16.808334383685   0.0000   0.0000   0.0000   0.0000
     67    0.013796520492564    0.0    0.00     -16.827725630147     -16.827725630147     -16.827725630147     -16.813929109654   0.0000   0.0000   0.0000   0.0000
     68    0.013489497886998    0.0    0.00     -16.832871414691     -16.832871414691     -16.832871414691     -16.819381916804   0.0000   0.0000   0.0000   0.0000
     69    0.013261501397027    0.0    0.00     -16.837990921715     -16.837990921715     -16.837990921715     -16.824729420318   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.54 (Slater)  0.56 (Becke)  0.00 (expected)


                total energy =      -16.84308617543 Hartree a.u.
              kinetic energy =       11.54569 Hartree a.u.
        electrostatic energy =      -22.91783 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62159 Hartree a.u.
  n-l pseudopotential energy =        2.23246 Hartree a.u.
 exchange-correlation energy =       -4.08181 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -29.10  -15.19  -11.52   -4.98

   Eigenvalues (eV), kp =   1 , spin =  2

  -30.20  -15.17  -11.66  -10.97


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.37864188258002E-01    -0.61586665878924E-01    -0.36627320957688E-02
   H       0.28370306608470E-01    -0.27997657551952E-02     0.14948502570264E-01
   H      -0.58902722968373E-02     0.43197480080711E-01     0.18014317354092E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.013088305047006    0.0    0.00     -16.843086175430     -16.843086175430     -16.843086175430     -16.829997870383   0.0000   0.0000   0.0000   0.0000
     71    0.012949263822012    0.0    0.00     -16.848154111991     -16.848154111991     -16.848154111991     -16.835204848169   0.0000   0.0000   0.0000   0.0000
     72    0.012826970617700    0.0    0.00     -16.853187444973     -16.853187444973     -16.853187444973     -16.840360474355   0.0000   0.0000   0.0000   0.0000
     73    0.012706191257812    0.0    0.00     -16.858174448336     -16.858174448336     -16.858174448336     -16.845468257078   0.0000   0.0000   0.0000   0.0000
     74    0.012574784144403    0.0    0.00     -16.863100591355     -16.863100591355     -16.863100591355     -16.850525807210   0.0000   0.0000   0.0000   0.0000
     75    0.012425961069031    0.0    0.00     -16.867952887186     -16.867952887186     -16.867952887186     -16.855526926117   0.0000   0.0000   0.0000   0.0000
     76    0.012258048158039    0.0    0.00     -16.872722918939     -16.872722918939     -16.872722918939     -16.860464870781   0.0000   0.0000   0.0000   0.0000
     77    0.012068759965802    0.0    0.00     -16.877400776651     -16.877400776651     -16.877400776651     -16.865332016685   0.0000   0.0000   0.0000   0.0000
     78    0.011852796831740    0.0    0.00     -16.881971233451     -16.881971233451     -16.881971233451     -16.870118436619   0.0000   0.0000   0.0000   0.0000
     79    0.011605866960082    0.0    0.00     -16.886417668976     -16.886417668976     -16.886417668976     -16.874811802016   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.32 (Slater)  0.39 (Becke)  0.00 (expected)


                total energy =      -16.89072845397 Hartree a.u.
              kinetic energy =       11.63795 Hartree a.u.
        electrostatic energy =      -22.96441 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62380 Hartree a.u.
  n-l pseudopotential energy =        2.18028 Hartree a.u.
 exchange-correlation energy =       -4.12075 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -28.14  -14.30  -10.58   -5.68

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.94  -14.28  -10.69   -9.38


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.25084346059292E-01    -0.38910550580651E-01     0.51229414123044E-03
   H       0.17459606204751E-01     0.18032746902464E-03     0.11729416434665E-01
   H      -0.11452184952375E-02     0.24260229175553E-01     0.13936858898985E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.011328426392871    0.0    0.00     -16.890728453966     -16.890728453966     -16.890728453966     -16.879400027573   0.0000   0.0000   0.0000   0.0000
     81    0.011021873422713    0.0    0.00     -16.894893589731     -16.894893589731     -16.894893589731     -16.883871716309   0.0000   0.0000   0.0000   0.0000
     82    0.010686045931887    0.0    0.00     -16.898900712496     -16.898900712496     -16.898900712496     -16.888214666564   0.0000   0.0000   0.0000   0.0000
     83    0.010323798156310    0.0    0.00     -16.902740736995     -16.902740736995     -16.902740736995     -16.892416938839   0.0000   0.0000   0.0000   0.0000
     84    0.009942196321545    0.0    0.00     -16.906412423698     -16.906412423698     -16.906412423698     -16.896470227377   0.0000   0.0000   0.0000   0.0000
     85    0.009547916232762    0.0    0.00     -16.909916014398     -16.909916014398     -16.909916014398     -16.900368098165   0.0000   0.0000   0.0000   0.0000
     86    0.009146316531151    0.0    0.00     -16.913252649015     -16.913252649015     -16.913252649015     -16.904106332484   0.0000   0.0000   0.0000   0.0000
     87    0.008743292145449    0.0    0.00     -16.916426621627     -16.916426621627     -16.916426621627     -16.907683329482   0.0000   0.0000   0.0000   0.0000
     88    0.008343204809261    0.0    0.00     -16.919443177852     -16.919443177852     -16.919443177852     -16.911099973043   0.0000   0.0000   0.0000   0.0000
     89    0.007946967496326    0.0    0.00     -16.922304530263     -16.922304530263     -16.922304530263     -16.914357562767   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.14 (Slater)  0.24 (Becke)  0.00 (expected)


                total energy =      -16.92501093141 Hartree a.u.
              kinetic energy =       11.68217 Hartree a.u.
        electrostatic energy =      -22.98473 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62144 Hartree a.u.
  n-l pseudopotential energy =        2.14828 Hartree a.u.
 exchange-correlation energy =       -4.14929 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.60  -13.59   -9.90   -6.27

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.03  -13.61   -9.97   -8.17


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20900471811128E-01    -0.26349873852205E-01     0.96704207779840E-02
   H       0.13149389418674E-01     0.20948969693315E-02     0.74584952931263E-02
   H       0.15429729183284E-02     0.15130493635762E-01     0.91044199267480E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.007553667989148    0.0    0.00     -16.925010931408     -16.925010931408     -16.925010931408     -16.917457263419   0.0000   0.0000   0.0000   0.0000
     91    0.007161869263704    0.0    0.00     -16.927562170608     -16.927562170608     -16.927562170608     -16.920400301344   0.0000   0.0000   0.0000   0.0000
     92    0.006768786542377    0.0    0.00     -16.929955367868     -16.929955367868     -16.929955367868     -16.923186581325   0.0000   0.0000   0.0000   0.0000
     93    0.006371520167929    0.0    0.00     -16.932186270533     -16.932186270533     -16.932186270533     -16.925814750365   0.0000   0.0000   0.0000   0.0000
     94    0.005968878008245    0.0    0.00     -16.934251606575     -16.934251606575     -16.934251606575     -16.928282728567   0.0000   0.0000   0.0000   0.0000
     95    0.005561775345377    0.0    0.00     -16.936150220574     -16.936150220574     -16.936150220574     -16.930588445229   0.0000   0.0000   0.0000   0.0000
     96    0.005152987192515    0.0    0.00     -16.937883550420     -16.937883550420     -16.937883550420     -16.932730563228   0.0000   0.0000   0.0000   0.0000
     97    0.004746949764350    0.0    0.00     -16.939456208540     -16.939456208540     -16.939456208540     -16.934709258776   0.0000   0.0000   0.0000   0.0000
     98    0.004349238959614    0.0    0.00     -16.940876056845     -16.940876056845     -16.940876056845     -16.936526817886   0.0000   0.0000   0.0000   0.0000
     99    0.003965661236137    0.0    0.00     -16.942153522420     -16.942153522420     -16.942153522420     -16.938187861184   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   100

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.05 (Slater)  0.13 (Becke)  0.00 (expected)


                total energy =      -16.94330041454 Hartree a.u.
              kinetic energy =       11.70406 Hartree a.u.
        electrostatic energy =      -22.99771 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62350 Hartree a.u.
  n-l pseudopotential energy =        2.13977 Hartree a.u.
 exchange-correlation energy =       -4.16592 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.35  -13.21   -9.54   -6.65

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.54  -13.24   -9.58   -7.49


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20666120276550E-01    -0.23051899501410E-01     0.33878207713024E-02
   H       0.12688438805583E-01     0.26075153116602E-02     0.35917651256789E-02
   H       0.24627922433873E-02     0.13276325966739E-01     0.45266897819050E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    100    0.003601176685246    0.0    0.00     -16.943300414535     -16.943300414535     -16.943300414535     -16.939699237850   0.0000   0.0000   0.0000   0.0000
    101    0.003258949939047    0.0    0.00     -16.944328319443     -16.944328319443     -16.944328319443     -16.941069369503   0.0000   0.0000   0.0000   0.0000
    102    0.002940098971910    0.0    0.00     -16.945247533207     -16.945247533207     -16.945247533207     -16.942307434235   0.0000   0.0000   0.0000   0.0000
    103    0.002644267943067    0.0    0.00     -16.946066951392     -16.946066951392     -16.946066951392     -16.943422683449   0.0000   0.0000   0.0000   0.0000
    104    0.002370477172443    0.0    0.00     -16.946794572087     -16.946794572087     -16.946794572087     -16.944424094914   0.0000   0.0000   0.0000   0.0000
    105    0.002117654453570    0.0    0.00     -16.947437913424     -16.947437913424     -16.947437913424     -16.945320258970   0.0000   0.0000   0.0000   0.0000
    106    0.001884793596604    0.0    0.00     -16.948004145869     -16.948004145869     -16.948004145869     -16.946119352272   0.0000   0.0000   0.0000   0.0000
    107    0.001670998993966    0.0    0.00     -16.948500168694     -16.948500168694     -16.948500168694     -16.946829169701   0.0000   0.0000   0.0000   0.0000
    108    0.001475481903428    0.0    0.00     -16.948932663786     -16.948932663786     -16.948932663786     -16.947457181882   0.0000   0.0000   0.0000   0.0000
    109    0.001297510930901    0.0    0.00     -16.949308066532     -16.949308066532     -16.949308066532     -16.948010555601   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   110

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.01 (Slater)  0.07 (Becke)  0.00 (expected)


                total energy =      -16.94963254752 Hartree a.u.
              kinetic energy =       11.70919 Hartree a.u.
        electrostatic energy =      -22.99861 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62226 Hartree a.u.
  n-l pseudopotential energy =        2.13355 Hartree a.u.
 exchange-correlation energy =       -4.17150 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.09   -9.41   -6.87

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.35  -13.11   -9.44   -7.19


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18987756544059E-01    -0.20103238762265E-01     0.40487820325115E-02
   H       0.13338634579292E-01     0.27141231791143E-02     0.17296876678220E-02
   H       0.26782256186011E-02     0.13532294362977E-01     0.20993514956174E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    110    0.001136377420547    0.0    0.00     -16.949632547520     -16.949632547520     -16.949632547520     -16.948496170099   0.0000   0.0000   0.0000   0.0000
    111    0.000991369499624    0.0    0.00     -16.949911993721     -16.949911993721     -16.949911993721     -16.948920624221   0.0000   0.0000   0.0000   0.0000
    112    0.000861728338317    0.0    0.00     -16.950151956578     -16.950151956578     -16.950151956578     -16.949290228240   0.0000   0.0000   0.0000   0.0000
    113    0.000746590002893    0.0    0.00     -16.950357555414     -16.950357555414     -16.950357555414     -16.949610965412   0.0000   0.0000   0.0000   0.0000
    114    0.000644962881044    0.0    0.00     -16.950533404608     -16.950533404608     -16.950533404608     -16.949888441727   0.0000   0.0000   0.0000   0.0000
    115    0.000555752889403    0.0    0.00     -16.950683594572     -16.950683594572     -16.950683594572     -16.950127841683   0.0000   0.0000   0.0000   0.0000
    116    0.000477808886826    0.0    0.00     -16.950811707223     -16.950811707223     -16.950811707223     -16.950333898337   0.0000   0.0000   0.0000   0.0000
    117    0.000409980301583    0.0    0.00     -16.950920859192     -16.950920859192     -16.950920859192     -16.950510878890   0.0000   0.0000   0.0000   0.0000
    118    0.000351162953898    0.0    0.00     -16.951013764566     -16.951013764566     -16.951013764566     -16.950662601612   0.0000   0.0000   0.0000   0.0000
    119    0.000300321228928    0.0    0.00     -16.951092778229     -16.951092778229     -16.951092778229     -16.950792457000   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   120

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.03 (Becke)  0.00 (expected)


                total energy =      -16.95115993607 Hartree a.u.
              kinetic energy =       11.71262 Hartree a.u.
        electrostatic energy =      -22.99969 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62224 Hartree a.u.
  n-l pseudopotential energy =        2.13157 Hartree a.u.
 exchange-correlation energy =       -4.17342 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.25  -13.06   -9.38   -6.95

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.29  -13.06   -9.39   -7.07


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17867024256082E-01    -0.18215736232594E-01     0.39525331084462E-02
   H       0.13222060047721E-01     0.28776161577188E-02     0.84564724479336E-03
   H       0.28739149078482E-02     0.13265260972303E-01     0.92882440705509E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    120    0.000256498746898    0.0    0.00     -16.951159936072     -16.951159936072     -16.951159936072     -16.950903437325   0.0000   0.0000   0.0000   0.0000
    121    0.000218823561928    0.0    0.00     -16.951216993281     -16.951216993281     -16.951216993281     -16.950998169719   0.0000   0.0000   0.0000   0.0000
    122    0.000186507519468    0.0    0.00     -16.951265457353     -16.951265457353     -16.951265457353     -16.951078949834   0.0000   0.0000   0.0000   0.0000
    123    0.000158843048370    0.0    0.00     -16.951306617434     -16.951306617434     -16.951306617434     -16.951147774386   0.0000   0.0000   0.0000   0.0000
    124    0.000135199489595    0.0    0.00     -16.951341572211     -16.951341572211     -16.951341572211     -16.951206372722   0.0000   0.0000   0.0000   0.0000
    125    0.000115019120094    0.0    0.00     -16.951371254617     -16.951371254617     -16.951371254617     -16.951256235497   0.0000   0.0000   0.0000   0.0000
    126    0.000097812746512    0.0    0.00     -16.951396461591     -16.951396461591     -16.951396461591     -16.951298648844   0.0000   0.0000   0.0000   0.0000
    127    0.000083154459087    0.0    0.00     -16.951417866232     -16.951417866232     -16.951417866232     -16.951334711773   0.0000   0.0000   0.0000   0.0000
    128    0.000070675831297    0.0    0.00     -16.951436042369     -16.951436042369     -16.951436042369     -16.951365366538   0.0000   0.0000   0.0000   0.0000
    129    0.000060059615501    0.0    0.00     -16.951451478380     -16.951451478380     -16.951451478380     -16.951391418764   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   130

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.02 (Becke)  0.00 (expected)


                total energy =      -16.95146458991 Hartree a.u.
              kinetic energy =       11.71396 Hartree a.u.
        electrostatic energy =      -23.00040 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62255 Hartree a.u.
  n-l pseudopotential energy =        2.13137 Hartree a.u.
 exchange-correlation energy =       -4.17385 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.25  -13.05   -9.37   -6.99

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.27  -13.05   -9.37   -7.02


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17913957130589E-01    -0.17928042844062E-01     0.13321256247812E-02
   H       0.13120772913427E-01     0.29193055040361E-02     0.28992325317834E-03
   H       0.29196581008095E-02     0.13125379874899E-01     0.28530156098371E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    130    0.000051033347356    0.0    0.00     -16.951464589912     -16.951464589912     -16.951464589912     -16.951413556565   0.0000   0.0000   0.0000   0.0000
    131    0.000043363277513    0.0    0.00     -16.951475730440     -16.951475730440     -16.951475730440     -16.951432367162   0.0000   0.0000   0.0000   0.0000
    132    0.000036848926380    0.0    0.00     -16.951485200145     -16.951485200145     -16.951485200145     -16.951448351219   0.0000   0.0000   0.0000   0.0000
    133    0.000031318391144    0.0    0.00     -16.951493253538     -16.951493253538     -16.951493253538     -16.951461935147   0.0000   0.0000   0.0000   0.0000
    134    0.000026624372192    0.0    0.00     -16.951500106088     -16.951500106088     -16.951500106088     -16.951473481716   0.0000   0.0000   0.0000   0.0000
    135    0.000022640840809    0.0    0.00     -16.951505940033     -16.951505940033     -16.951505940033     -16.951483299193   0.0000   0.0000   0.0000   0.0000
    136    0.000019260210464    0.0    0.00     -16.951510909490     -16.951510909490     -16.951510909490     -16.951491649279   0.0000   0.0000   0.0000   0.0000
    137    0.000016390847232    0.0    0.00     -16.951515144822     -16.951515144822     -16.951515144822     -16.951498753975   0.0000   0.0000   0.0000   0.0000
    138    0.000013954860718    0.0    0.00     -16.951518756361     -16.951518756361     -16.951518756361     -16.951504801500   0.0000   0.0000   0.0000   0.0000
    139    0.000011886147370    0.0    0.00     -16.951521837532     -16.951521837532     -16.951521837532     -16.951509951385   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   140

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.01 (Becke)  0.00 (expected)


                total energy =      -16.95152446749 Hartree a.u.
              kinetic energy =       11.71398 Hartree a.u.
        electrostatic energy =      -23.00042 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62256 Hartree a.u.
  n-l pseudopotential energy =        2.13133 Hartree a.u.
 exchange-correlation energy =       -4.17385 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18051256538982E-01    -0.18052725316496E-01    -0.51358281580645E-04
   H       0.13110789536741E-01     0.29100903808552E-02     0.42779322096868E-04
   H       0.29102450322051E-02     0.13108969869399E-01     0.32640715870179E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    140    0.000010128664567    0.0    0.00     -16.951524467487     -16.951524467487     -16.951524467487     -16.951514338823   0.0000   0.0000   0.0000   0.0000
    141    0.000008634929459    0.0    0.00     -16.951526713309     -16.951526713309     -16.951526713309     -16.951518078380   0.0000   0.0000   0.0000   0.0000
    142    0.000007364732028    0.0    0.00     -16.951528631881     -16.951528631881     -16.951528631881     -16.951521267149   0.0000   0.0000   0.0000   0.0000
    143    0.000006284039284    0.0    0.00     -16.951530271468     -16.951530271468     -16.951530271468     -16.951523987428   0.0000   0.0000   0.0000   0.0000
    144    0.000005364063269    0.0    0.00     -16.951531673064     -16.951531673064     -16.951531673064     -16.951526309000   0.0000   0.0000   0.0000   0.0000
    145    0.000004580466315    0.0    0.00     -16.951532871528     -16.951532871528     -16.951532871528     -16.951528291062   0.0000   0.0000   0.0000   0.0000
    146    0.000003912679538    0.0    0.00     -16.951533896545     -16.951533896545     -16.951533896545     -16.951529983865   0.0000   0.0000   0.0000   0.0000
    147    0.000003343316336    0.0    0.00     -16.951534773418     -16.951534773418     -16.951534773418     -16.951531430101   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.334332D-05  0.1D-03  0.876873D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:   148

 Spin contamination: s(s+1)= 0.00 (Slater)  0.00 (Becke)  0.00 (expected)


                total energy =      -16.95153552374 Hartree a.u.
              kinetic energy =       11.71392 Hartree a.u.
        electrostatic energy =      -23.00041 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62255 Hartree a.u.
  n-l pseudopotential energy =        2.13136 Hartree a.u.
 exchange-correlation energy =       -4.17385 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18051635439388E-01    -0.18051741011930E-01    -0.89235506758945E-04
   H       0.13112977166119E-01     0.29097243899844E-02    -0.52230603841599E-05
   H       0.29093599792792E-02     0.13112137270407E-01    -0.10021729078370E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
    148    0.000002857668670    0.0    0.00     -16.951535523736     -16.951535523736     -16.951535523736     -16.951532666068   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.285767D-05  0.1D-03  0.750319D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.01s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.48877       0.48877 (AU)
   ekin          :       10.73715      10.73715 (AU)
   epot          :      -28.14322     -28.14322 (AU)
   total energy  :      -15.01287     -15.01287 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :      -15.01287     -15.01287 (AU)
   econs         :      -15.01287     -15.01287 (AU)
   pressure      :        0.00000       0.00000 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      0.69s CPU      0.70s WALL (       1 calls)
     main_loop    :     22.48s CPU     23.83s WALL (     148 calls)
     cpr_total    :     22.48s CPU     23.84s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     22.48s CPU     23.84s WALL (     148 calls)
     move_electro :     22.29s CPU     23.63s WALL (     148 calls)

     Called by move_electrons:
     rhoofr       :      4.64s CPU      4.71s WALL (     149 calls)
     vofrho       :     12.95s CPU     14.21s WALL (     149 calls)
     dforce       :      4.74s CPU      4.77s WALL (     596 calls)
     calphi       :      0.02s CPU      0.02s WALL (     149 calls)
     nlfl         :      0.00s CPU      0.00s WALL (     149 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (     298 calls)
     rsg          :      0.01s CPU      0.01s WALL (     298 calls)
     rhoset       :      0.02s CPU      0.02s WALL (     298 calls)
     sigset       :      0.01s CPU      0.01s WALL (     298 calls)
     tauset       :      0.01s CPU      0.01s WALL (     298 calls)
     ortho        :      0.06s CPU      0.06s WALL (     149 calls)
     updatc       :      0.01s CPU      0.01s WALL (     149 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (     149 calls)
     nlfq         :      0.03s CPU      0.03s WALL (     149 calls)
     nlsm1        :      0.01s CPU      0.01s WALL (     150 calls)
     nlsm2        :      0.02s CPU      0.02s WALL (     149 calls)
     fft          :      3.72s CPU      3.81s WALL (    2236 calls)
     ffts         :      1.00s CPU      1.00s WALL (     596 calls)
     fftw         :      5.76s CPU      5.81s WALL (    7152 calls)
     fft_scatt_xy :      2.55s CPU      2.59s WALL (    9984 calls)
     fft_scatt_yz :      2.97s CPU      3.01s WALL (    9984 calls)
     fft_scatt_tg :      0.04s CPU      0.04s WALL (    1788 calls)
     betagx       :      0.21s CPU      0.21s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      0.52s CPU      0.52s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.00s CPU      0.00s WALL (       1 calls)
     from_scratch :      0.15s CPU      0.17s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.01s CPU      0.01s WALL (     150 calls)


     CP           :    23.22s CPU        24.59s WALL


   This run was terminated on:   2: 5:21   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
