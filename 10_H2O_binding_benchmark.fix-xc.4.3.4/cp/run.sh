#!/bin/bash
cd ./S2/
mpirun -n 2 ../cp.x < ./input > output
cd ..

cd ./S2.A/
mpirun -n 2 ../cp.x < ./input > output
cd ..

cd ./S2.B/
mpirun -n 2 ../cp.x < ./input > output
cd ..
