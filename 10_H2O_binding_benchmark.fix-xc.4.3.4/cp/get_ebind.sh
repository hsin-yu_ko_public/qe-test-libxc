eha_dimer=`grep -B2 'MAIN:.* EKINC.* (thr).* DETOT.* (thr).* MAXFORCE.* (thr)' S2/output| tail -n 3 | head -n1 | awk '{print $5}'`
eha_monomerA=`grep -B2 'MAIN:.* EKINC.* (thr).* DETOT.* (thr).* MAXFORCE.* (thr)' S2.A/output| tail -n 3 | head -n1 | awk '{print $5}'`
eha_monomerB=`grep -B2 'MAIN:.* EKINC.* (thr).* DETOT.* (thr).* MAXFORCE.* (thr)' S2.B/output| tail -n 3 | head -n1 | awk '{print $5}'`
echo "$eha_dimer $eha_monomerA $eha_monomerB" | awk '{print "Ebind=",($1-$2-$3)*627.5096080306, "kcal/mol", "literature value: -5.39 kcal/mol"}' | tee Ebind.out
echo "ref: JCP 144, 044114 (2016)" | tee -a Ebind.out
