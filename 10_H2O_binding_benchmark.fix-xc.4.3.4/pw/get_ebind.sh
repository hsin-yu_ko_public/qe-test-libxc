ery_dimer=`grep '!' S2/output | awk '{print $5}'`
ery_monomerA=`grep '!' S2.A/output | awk '{print $5}'`
ery_monomerB=`grep '!' S2.B/output | awk '{print $5}'`
echo "$ery_dimer $ery_monomerA $ery_monomerB" | awk '{print "Ebind=",($1-$2-$3)*627.5096080306/2.0, "kcal/mol", "literature value: -5.39 kcal/mol"}' | tee Ebind.out
echo "ref: JCP 144, 044114 (2016)" | tee -a Ebind.out
