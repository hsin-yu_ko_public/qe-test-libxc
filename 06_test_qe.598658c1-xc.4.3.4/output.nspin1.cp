
     Program CP v.6.7GPU starts on  6Jun2021 at  2:11:27 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
         "P. Giannozzi et al., J. Chem. Phys. 152 154105 (2020);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
     Message from routine scan_end:
     No INFO block end statement, possibly corrupted file
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Number of Electrons=     8, of States =     4
   Occupation numbers :
   2.00 2.00 2.00 2.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   internal stress tensor calculated
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249

     Using Pencil Decomposition


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |
   Using Pencil Decomposition

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

     Subspace diagonalization in iterative solution of the eigenvalue problem:
     a serial algorithm will be used


   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   nbnd =     4
    2.00 2.00 2.00 2.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)       91.24618      0

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.976566965109257    0.0    0.00      13.177390871541      13.177390871541      13.177390871541      14.153957836651   0.0000   0.0000   0.0000   0.0000
      2    2.768909244405163    0.0    0.00      10.600058166398      10.600058166398      10.600058166398      13.368967410803   0.0000   0.0000   0.0000   0.0000
      3    5.066052758181264    0.0    0.00       6.792030275459       6.792030275459       6.792030275459      11.858083033641   0.0000   0.0000   0.0000   0.0000
      4    7.567941515095341    0.0    0.00       1.992403122983       1.992403122983       1.992403122983       9.560344638078   0.0000   0.0000   0.0000   0.0000
      5    8.977433068182705    0.0    0.00      -2.487286622742      -2.487286622742      -2.487286622742       6.490146445441   0.0000   0.0000   0.0000   0.0000
      6    7.554421251647222    0.0    0.00      -3.877474002482      -3.877474002482      -3.877474002482       3.676947249165   0.0000   0.0000   0.0000   0.0000
      7    4.658184310394582    0.0    0.00      -3.068974415938      -3.068974415938      -3.068974415938       1.589209894457   0.0000   0.0000   0.0000   0.0000
      8    2.924499481938872    0.0    0.00      -2.607860454594      -2.607860454594      -2.607860454594       0.316639027345   0.0000   0.0000   0.0000   0.0000
      9    2.653259524457532    0.0    0.00      -3.056457198511      -3.056457198511      -3.056457198511      -0.403197674053   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)      -46.37614     10
     Pressure of Nuclei (GPa)             0.00000     10
     Pressure Total (GPa)           -46.37614     10


                total energy =       -3.91581154271 Hartree a.u.
              kinetic energy =        8.00419 Hartree a.u.
        electrostatic energy =      -15.96150 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.36136 Hartree a.u.
  n-l pseudopotential energy =        3.94856 Hartree a.u.
 exchange-correlation energy =        2.45430 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -78.97  -17.43   -7.60   -1.26


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
      -34.01782505        -3.78803099        -1.13911100
       -3.78803099       -41.32248882        -0.61837294
       -1.13911100        -0.61837294       -63.78811125
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.79060086373037E+00     0.89196972743263E+00     0.21854172685068E+00
   H       0.96502993051912E+00    -0.18157631185234E+00     0.40982315498421E-02
   H      -0.23142632745509E+00     0.95623541655594E+00     0.28179857059205E+00



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    2.921745780306869    0.0    0.00      -3.915811542706      -3.915811542706      -3.915811542706      -0.994065762399   0.0000   0.0000   0.0000   0.0000
     11    2.951109137111763    0.0    0.00      -4.650147178183      -4.650147178183      -4.650147178183      -1.699038041071   0.0000   0.0000   0.0000   0.0000
     12    2.604414114663909    0.0    0.00      -5.150633379894      -5.150633379894      -5.150633379894      -2.546219265230   0.0000   0.0000   0.0000   0.0000
     13    2.166325509887246    0.0    0.00      -5.534136849890      -5.534136849890      -5.534136849890      -3.367811340003   0.0000   0.0000   0.0000   0.0000
     14    1.842508292997843    0.0    0.00      -5.898611139290      -5.898611139290      -5.898611139290      -4.056102846292   0.0000   0.0000   0.0000   0.0000
     15    1.637426583811155    0.0    0.00      -6.254115896137      -6.254115896137      -6.254115896137      -4.616689312326   0.0000   0.0000   0.0000   0.0000
     16    1.483825867030851    0.0    0.00      -6.574091051032      -6.574091051032      -6.574091051032      -5.090265184001   0.0000   0.0000   0.0000   0.0000
     17    1.340521529252746    0.0    0.00      -6.849322721721      -6.849322721721      -6.849322721721      -5.508801192468   0.0000   0.0000   0.0000   0.0000
     18    1.206185282668607    0.0    0.00      -7.082827649034      -7.082827649034      -7.082827649034      -5.876642366365   0.0000   0.0000   0.0000   0.0000
     19    1.092407502604325    0.0    0.00      -7.271322230101      -7.271322230101      -7.271322230101      -6.178914727497   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -108.27975     20
     Pressure of Nuclei (GPa)             0.00000     20
     Pressure Total (GPa)          -108.27975     20


                total energy =       -7.40959645675 Hartree a.u.
              kinetic energy =        9.87237 Hartree a.u.
        electrostatic energy =      -20.72792 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.07474 Hartree a.u.
  n-l pseudopotential energy =        3.13050 Hartree a.u.
 exchange-correlation energy =        3.39020 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -43.03  -26.72   -8.54   -3.59


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -100.68347453       -12.77715284         1.93396136
      -12.77715284      -105.32439495         3.45620540
        1.93396136         3.45620540      -118.83139385
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18597853645819E+00     0.17980839480207E+00     0.39778613434951E+00
   H       0.46628870771826E+00    -0.49226215907913E+00    -0.85162821792677E-01
   H      -0.27132083848555E+00     0.24632311985514E+00    -0.44014981170299E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.001999542670544    0.0    0.00      -7.409596456754      -7.409596456754      -7.409596456754      -6.407596914084   0.0000   0.0000   0.0000   0.0000
     21    0.927768452630518    0.0    0.00      -7.499429474371      -7.499429474371      -7.499429474371      -6.571661021741   0.0000   0.0000   0.0000   0.0000
     22    0.859467293871489    0.0    0.00      -7.555039144312      -7.555039144312      -7.555039144312      -6.695571850441   0.0000   0.0000   0.0000   0.0000
     23    0.789431278506857    0.0    0.00      -7.598073880181      -7.598073880181      -7.598073880181      -6.808642601675   0.0000   0.0000   0.0000   0.0000
     24    0.714663554494174    0.0    0.00      -7.648931928738      -7.648931928738      -7.648931928738      -6.934268374244   0.0000   0.0000   0.0000   0.0000
     25    0.635832221019282    0.0    0.00      -7.719496398871      -7.719496398871      -7.719496398871      -7.083664177852   0.0000   0.0000   0.0000   0.0000
     26    0.555535685142799    0.0    0.00      -7.811444063914      -7.811444063914      -7.811444063914      -7.255908378772   0.0000   0.0000   0.0000   0.0000
     27    0.477391393522829    0.0    0.00      -7.918782950689      -7.918782950689      -7.918782950689      -7.441391557166   0.0000   0.0000   0.0000   0.0000
     28    0.405934152411218    0.0    0.00      -8.030872652156      -8.030872652156      -8.030872652156      -7.624938499745   0.0000   0.0000   0.0000   0.0000
     29    0.344750138982155    0.0    0.00      -8.138485364099      -8.138485364099      -8.138485364099      -7.793735225117   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -132.93643     30
     Pressure of Nuclei (GPa)             0.00000     30
     Pressure Total (GPa)          -132.93643     30


                total energy =       -8.23464585041 Hartree a.u.
              kinetic energy =       12.11283 Hartree a.u.
        electrostatic energy =      -22.71589 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.65252 Hartree a.u.
  n-l pseudopotential energy =        2.12735 Hartree a.u.
 exchange-correlation energy =        3.89359 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -32.47  -19.20  -12.84   -4.42


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -127.03181308        -6.97875947        -0.90468288
       -6.97875947      -131.34550901        -0.19481661
       -0.90468288        -0.19481661      -140.43195391
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.52262967661254E+00    -0.33971097966693E+00    -0.11747775574641E+00
   H       0.21328404162335E+00    -0.14714284479953E+00    -0.20435689688402E-01
   H      -0.10963352731274E+00     0.13679990819105E+00    -0.49499021078394E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.295909754380650    0.0    0.00      -8.234645850408      -8.234645850408      -8.234645850408      -7.938736096028   0.0000   0.0000   0.0000   0.0000
     31    0.260234515353002    0.0    0.00      -8.315049322056      -8.315049322056      -8.315049322056      -8.054814806703   0.0000   0.0000   0.0000   0.0000
     32    0.236837775910967    0.0    0.00      -8.378029166897      -8.378029166897      -8.378029166897      -8.141191390986   0.0000   0.0000   0.0000   0.0000
     33    0.223915705885948    0.0    0.00      -8.422822156684      -8.422822156684      -8.422822156684      -8.198906450798   0.0000   0.0000   0.0000   0.0000
     34    0.218871047211869    0.0    0.00      -8.450263568871      -8.450263568871      -8.450263568871      -8.231392521659   0.0000   0.0000   0.0000   0.0000
     35    0.218114100914605    0.0    0.00      -8.463205723339      -8.463205723339      -8.463205723339      -8.245091622424   0.0000   0.0000   0.0000   0.0000
     36    0.217959397707295    0.0    0.00      -8.466800555256      -8.466800555256      -8.466800555256      -8.248841157549   0.0000   0.0000   0.0000   0.0000
     37    0.215542222852695    0.0    0.00      -8.466743280435      -8.466743280435      -8.466743280435      -8.251201057582   0.0000   0.0000   0.0000   0.0000
     38    0.208870605692782    0.0    0.00      -8.468900264749      -8.468900264749      -8.468900264749      -8.260029659056   0.0000   0.0000   0.0000   0.0000
     39    0.196790870167323    0.0    0.00      -8.477299979827      -8.477299979827      -8.477299979827      -8.280509109660   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -148.87643     40
     Pressure of Nuclei (GPa)             0.00000     40
     Pressure Total (GPa)          -148.87643     40


                total energy =       -8.49332801255 Hartree a.u.
              kinetic energy =       12.04304 Hartree a.u.
        electrostatic energy =      -23.02989 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63042 Hartree a.u.
  n-l pseudopotential energy =        1.97135 Hartree a.u.
 exchange-correlation energy =        4.15258 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.67  -13.64  -10.08   -7.93


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.57473702        -0.38095857        -0.00020577
       -0.38095857      -148.48211202        -0.21729946
       -0.00020577        -0.21729946      -148.57242632
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13855903321731E+00    -0.99152916118306E-01     0.86082674114996E-02
   H      -0.59789550667675E-01    -0.17329193341612E-01    -0.61324951271475E-02
   H      -0.23616853134569E-01    -0.45405645418286E-01    -0.30851643375776E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.179324424508036    0.0    0.00      -8.493328012553      -8.493328012553      -8.493328012553      -8.314003588045   0.0000   0.0000   0.0000   0.0000
     41    0.157527830596749    0.0    0.00      -8.515591855485      -8.515591855485      -8.515591855485      -8.358064024888   0.0000   0.0000   0.0000   0.0000
     42    0.133099023075669    0.0    0.00      -8.541299580081      -8.541299580081      -8.541299580081      -8.408200557005   0.0000   0.0000   0.0000   0.0000
     43    0.108063940035057    0.0    0.00      -8.567616822389      -8.567616822389      -8.567616822389      -8.459552882354   0.0000   0.0000   0.0000   0.0000
     44    0.084465744981373    0.0    0.00      -8.591955875784      -8.591955875784      -8.591955875784      -8.507490130803   0.0000   0.0000   0.0000   0.0000
     45    0.063962620910485    0.0    0.00      -8.612571110940      -8.612571110940      -8.612571110940      -8.548608490029   0.0000   0.0000   0.0000   0.0000
     46    0.047373680849961    0.0    0.00      -8.627809839184      -8.627809839184      -8.627809839184      -8.580436158334   0.0000   0.0000   0.0000   0.0000
     47    0.034692306629180    0.0    0.00      -8.637380836161      -8.637380836161      -8.637380836161      -8.602688529532   0.0000   0.0000   0.0000   0.0000
     48    0.025363668913313    0.0    0.00      -8.641226128337      -8.641226128337      -8.641226128337      -8.615862459423   0.0000   0.0000   0.0000   0.0000
     49    0.018616085857168    0.0    0.00      -8.640259665633      -8.640259665633      -8.640259665633      -8.621643579776   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -149.01674     50
     Pressure of Nuclei (GPa)             0.00000     50
     Pressure Total (GPa)          -149.01674     50


                total energy =       -8.63596920519 Hartree a.u.
              kinetic energy =       11.61676 Hartree a.u.
        electrostatic energy =      -22.98751 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.63674 Hartree a.u.
  n-l pseudopotential energy =        2.22039 Hartree a.u.
 exchange-correlation energy =        4.15114 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.80  -13.29  -10.07   -7.32


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -147.75234502        -0.04814252         0.07377813
       -0.04814252      -148.36854265         0.06143892
        0.07377813         0.06143892      -150.92933331
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O       0.66026988692742E-01     0.35902016604248E-01     0.85716802975390E-02
   H       0.48634940735117E-01    -0.96291858059193E-02    -0.10575270128258E-02
   H       0.54919833667315E-03     0.40958932379326E-01    -0.23166865466320E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.013735787740613    0.0    0.00      -8.635969205194      -8.635969205194      -8.635969205194      -8.622233417454   0.0000   0.0000   0.0000   0.0000
     51    0.010196938203195    0.0    0.00      -8.629890573475      -8.629890573475      -8.629890573475      -8.619693635272   0.0000   0.0000   0.0000   0.0000
     52    0.007661052542696    0.0    0.00      -8.623642071497      -8.623642071497      -8.623642071497      -8.615981018954   0.0000   0.0000   0.0000   0.0000
     53    0.005907615187303    0.0    0.00      -8.618545180560      -8.618545180560      -8.618545180560      -8.612637565373   0.0000   0.0000   0.0000   0.0000
     54    0.004759852280475    0.0    0.00      -8.615341000942      -8.615341000942      -8.615341000942      -8.610581148662   0.0000   0.0000   0.0000   0.0000
     55    0.004047031712017    0.0    0.00      -8.614160130642      -8.614160130642      -8.614160130642      -8.610113098930   0.0000   0.0000   0.0000   0.0000
     56    0.003606001708215    0.0    0.00      -8.614628042964      -8.614628042964      -8.614628042964      -8.611022041255   0.0000   0.0000   0.0000   0.0000
     57    0.003300876890973    0.0    0.00      -8.615929156510      -8.615929156510      -8.615929156510      -8.612628279619   0.0000   0.0000   0.0000   0.0000
     58    0.003039220110402    0.0    0.00      -8.617240160895      -8.617240160895      -8.617240160895      -8.614200940785   0.0000   0.0000   0.0000   0.0000
     59    0.002774311844607    0.0    0.00      -8.618054768343      -8.618054768343      -8.618054768343      -8.615280456499   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.24346     60
     Pressure of Nuclei (GPa)             0.00000     60
     Pressure Total (GPa)          -150.24346     60


                total energy =       -8.61782723457 Hartree a.u.
              kinetic energy =       11.67542 Hartree a.u.
        electrostatic energy =      -22.97994 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60909 Hartree a.u.
  n-l pseudopotential energy =        2.13088 Hartree a.u.
 exchange-correlation energy =        4.16489 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.38  -13.22   -9.33   -7.11


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72350026        -0.41147292        -0.03974767
       -0.41147292      -149.55568882         0.01211963
       -0.03974767         0.01211963      -151.45117710
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.44925597216259E-01    -0.29728856500333E-01    -0.44161945153395E-02
   H       0.14064672658039E-01     0.65110781954861E-02    -0.24906888588789E-03
   H       0.40066271116628E-02     0.14728895271321E-01    -0.22263444222817E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.002493891257380    0.0    0.00      -8.617827234565      -8.617827234565      -8.617827234565      -8.615333343308   0.0000   0.0000   0.0000   0.0000
     61    0.002203258929227    0.0    0.00      -8.616496823625      -8.616496823625      -8.616496823625      -8.614293564695   0.0000   0.0000   0.0000   0.0000
     62    0.001911638903851    0.0    0.00      -8.614087524999      -8.614087524999      -8.614087524999      -8.612175886096   0.0000   0.0000   0.0000   0.0000
     63    0.001626118718694    0.0    0.00      -8.610924953910      -8.610924953910      -8.610924953910      -8.609298835192   0.0000   0.0000   0.0000   0.0000
     64    0.001351964340860    0.0    0.00      -8.607527558669      -8.607527558669      -8.607527558669      -8.606175594328   0.0000   0.0000   0.0000   0.0000
     65    0.001095004574345    0.0    0.00      -8.604271458341      -8.604271458341      -8.604271458341      -8.603176453766   0.0000   0.0000   0.0000   0.0000
     66    0.000862479837365    0.0    0.00      -8.601558496135      -8.601558496135      -8.601558496135      -8.600696016298   0.0000   0.0000   0.0000   0.0000
     67    0.000661549396299    0.0    0.00      -8.599663437277      -8.599663437277      -8.599663437277      -8.599001887881   0.0000   0.0000   0.0000   0.0000
     68    0.000496713598501    0.0    0.00      -8.598687522964      -8.598687522964      -8.598687522964      -8.598190809366   0.0000   0.0000   0.0000   0.0000
     69    0.000368038261228    0.0    0.00      -8.598583351761      -8.598583351761      -8.598583351761      -8.598215313499   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30510     70
     Pressure of Nuclei (GPa)             0.00000     70
     Pressure Total (GPa)          -150.30510     70


                total energy =       -8.59925136835 Hartree a.u.
              kinetic energy =       11.73442 Hartree a.u.
        electrostatic energy =      -23.01057 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62878 Hartree a.u.
  n-l pseudopotential energy =        2.13030 Hartree a.u.
 exchange-correlation energy =        4.17537 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.21  -13.02   -9.35   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.73156610        -0.21611727         0.01663623
       -0.21611727      -149.75585163        -0.00585370
        0.01663623        -0.00585370      -151.42789169
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.88488143355629E-02    -0.12622302421829E-01     0.16959776862269E-02
   H       0.11645030853614E-01     0.29981633469339E-02     0.34599775665477E-04
   H       0.27323463215581E-02     0.11853278849151E-01     0.22630662642151E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.000271447399381    0.0    0.00      -8.599251368351      -8.599251368351      -8.599251368351      -8.598979920951   0.0000   0.0000   0.0000   0.0000
     71    0.000200647918694    0.0    0.00      -8.600414480820      -8.600414480820      -8.600414480820      -8.600213832901   0.0000   0.0000   0.0000   0.0000
     72    0.000149342702200    0.0    0.00      -8.601821860609      -8.601821860609      -8.601821860609      -8.601672517907   0.0000   0.0000   0.0000   0.0000
     73    0.000112642566948    0.0    0.00      -8.603244776509      -8.603244776509      -8.603244776509      -8.603132133942   0.0000   0.0000   0.0000   0.0000
     74    0.000087241884737    0.0    0.00      -8.604485596776      -8.604485596776      -8.604485596776      -8.604398354891   0.0000   0.0000   0.0000   0.0000
     75    0.000070796302455    0.0    0.00      -8.605396196210      -8.605396196210      -8.605396196210      -8.605325399907   0.0000   0.0000   0.0000   0.0000
     76    0.000061176842203    0.0    0.00      -8.605976733531      -8.605976733531      -8.605976733531      -8.605915556688   0.0000   0.0000   0.0000   0.0000
     77    0.000056108433520    0.0    0.00      -8.606192054872      -8.606192054872      -8.606192054872      -8.606135946439   0.0000   0.0000   0.0000   0.0000
     78    0.000053317827987    0.0    0.00      -8.606129144336      -8.606129144336      -8.606129144336      -8.606075826508   0.0000   0.0000   0.0000   0.0000
     79    0.000050913004145    0.0    0.00      -8.605877304102      -8.605877304102      -8.605877304102      -8.605826391097   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.32663     80
     Pressure of Nuclei (GPa)             0.00000     80
     Pressure Total (GPa)          -150.32663     80


                total energy =       -8.60555020434 Hartree a.u.
              kinetic energy =       11.71086 Hartree a.u.
        electrostatic energy =      -22.99819 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62103 Hartree a.u.
  n-l pseudopotential energy =        2.13055 Hartree a.u.
 exchange-correlation energy =        4.17226 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.04   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.73490350        -0.23608215        -0.00197750
       -0.23608215      -149.73893216         0.00321172
       -0.00197750         0.00321172      -151.50606648
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.21359359769107E-01    -0.20940420906589E-01     0.54611145095145E-05
   H       0.13118109765348E-01     0.24998920582875E-02     0.14880110724548E-03
   H       0.28141516924611E-02     0.12986731870772E-01     0.12865121556207E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.000047699599680    0.0    0.00      -8.605550204338      -8.605550204338      -8.605550204338      -8.605502504738   0.0000   0.0000   0.0000   0.0000
     81    0.000043255702927    0.0    0.00      -8.605204498332      -8.605204498332      -8.605204498332      -8.605161242629   0.0000   0.0000   0.0000   0.0000
     82    0.000037760416840    0.0    0.00      -8.604929709565      -8.604929709565      -8.604929709565      -8.604891949148   0.0000   0.0000   0.0000   0.0000
     83    0.000031702144135    0.0    0.00      -8.604764673185      -8.604764673185      -8.604764673185      -8.604732971041   0.0000   0.0000   0.0000   0.0000
     84    0.000025616630241    0.0    0.00      -8.604717426507      -8.604717426507      -8.604717426507      -8.604691809877   0.0000   0.0000   0.0000   0.0000
     85    0.000019938421808    0.0    0.00      -8.604758585423      -8.604758585423      -8.604758585423      -8.604738647001   0.0000   0.0000   0.0000   0.0000
     86    0.000014952184766    0.0    0.00      -8.604876268515      -8.604876268515      -8.604876268515      -8.604861316331   0.0000   0.0000   0.0000   0.0000
     87    0.000010812209245    0.0    0.00      -8.605044918818      -8.605044918818      -8.605044918818      -8.605034106609   0.0000   0.0000   0.0000   0.0000
     88    0.000007574999046    0.0    0.00      -8.605219383632      -8.605219383632      -8.605219383632      -8.605211808633   0.0000   0.0000   0.0000   0.0000
     89    0.000005214373192    0.0    0.00      -8.605369641065      -8.605369641065      -8.605369641065      -8.605364426692   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30485     90
     Pressure of Nuclei (GPa)             0.00000     90
     Pressure Total (GPa)          -150.30485     90


                total energy =       -8.60545483710 Hartree a.u.
              kinetic energy =       11.71461 Hartree a.u.
        electrostatic energy =      -23.00048 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62264 Hartree a.u.
  n-l pseudopotential energy =        2.13069 Hartree a.u.
 exchange-correlation energy =        4.17236 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72229472        -0.24086231        -0.00195114
       -0.24086231      -149.71747892        -0.00183387
       -0.00195114        -0.00183387      -151.47476984
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.17149236415704E-01    -0.17128945939870E-01    -0.19072292160875E-03
   H       0.13057394345412E-01     0.29304379200390E-02    -0.35295357473556E-04
   H       0.28744715852488E-02     0.13091709290696E-01    -0.61317642762903E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.000003633511632    0.0    0.00      -8.605454837103      -8.605454837103      -8.605454837103      -8.605451203591   0.0000   0.0000   0.0000   0.0000
     91    0.000002683358997    0.0    0.00      -8.605498108476      -8.605498108476      -8.605498108476      -8.605495425117   0.0000   0.0000   0.0000   0.0000
     92    0.000002185821789    0.0    0.00      -8.605494445835      -8.605494445835      -8.605494445835      -8.605492260013   0.0000   0.0000   0.0000   0.0000
     93    0.000001963102675    0.0    0.00      -8.605450644238      -8.605450644238      -8.605450644238      -8.605448681136   0.0000   0.0000   0.0000   0.0000
     94    0.000001868544955    0.0    0.00      -8.605381216674      -8.605381216674      -8.605381216674      -8.605379348129   0.0000   0.0000   0.0000   0.0000
     95    0.000001798011974    0.0    0.00      -8.605310324685      -8.605310324685      -8.605310324685      -8.605308526673   0.0000   0.0000   0.0000   0.0000
     96    0.000001695531335    0.0    0.00      -8.605245854260      -8.605245854260      -8.605245854260      -8.605244158729   0.0000   0.0000   0.0000   0.0000
     97    0.000001545651693    0.0    0.00      -8.605199932302      -8.605199932302      -8.605199932302      -8.605198386650   0.0000   0.0000   0.0000   0.0000
     98    0.000001354383041    0.0    0.00      -8.605169878000      -8.605169878000      -8.605169878000      -8.605168523617   0.0000   0.0000   0.0000   0.0000
     99    0.000001140042426    0.0    0.00      -8.605163036502      -8.605163036502      -8.605163036502      -8.605161896460   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   100

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30470    100
     Pressure of Nuclei (GPa)             0.00000    100
     Pressure Total (GPa)          -150.30470    100


                total energy =       -8.60517075929 Hartree a.u.
              kinetic energy =       11.71349 Hartree a.u.
        electrostatic energy =      -23.00047 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62258 Hartree a.u.
  n-l pseudopotential energy =        2.13188 Hartree a.u.
 exchange-correlation energy =        4.17252 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.71612243        -0.24340785         0.00105308
       -0.24340785      -149.71780907         0.00072524
        0.00105308         0.00072524      -151.48015764
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18089450569722E-01    -0.18115686967735E-01     0.46475861580809E-04
   H       0.13155651627803E-01     0.29509095383979E-02    -0.16936934009582E-04
   H       0.29459970920042E-02     0.13176766696140E-01    -0.14804273245455E-05



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    100    0.000000924581017    0.0    0.00      -8.605170759290      -8.605170759290      -8.605170759290      -8.605169834709   0.0000   0.0000   0.0000   0.0000
    101    0.000000725086819    0.0    0.00      -8.605191909591      -8.605191909591      -8.605191909591      -8.605191184504   0.0000   0.0000   0.0000   0.0000
    102    0.000000553048851    0.0    0.00      -8.605212994538      -8.605212994538      -8.605212994538      -8.605212441489   0.0000   0.0000   0.0000   0.0000
    103    0.000000414814464    0.0    0.00      -8.605238356837      -8.605238356837      -8.605238356837      -8.605237942022   0.0000   0.0000   0.0000   0.0000
    104    0.000000311123034    0.0    0.00      -8.605245436860      -8.605245436860      -8.605245436860      -8.605245125737   0.0000   0.0000   0.0000   0.0000
    105    0.000000238757130    0.0    0.00      -8.605240070042      -8.605240070042      -8.605240070042      -8.605239831285   0.0000   0.0000   0.0000   0.0000
    106    0.000000191037787    0.0    0.00      -8.605225629975      -8.605225629975      -8.605225629975      -8.605225438937   0.0000   0.0000   0.0000   0.0000
    107    0.000000159920758    0.0    0.00      -8.605205557201      -8.605205557201      -8.605205557201      -8.605205397280   0.0000   0.0000   0.0000   0.0000
    108    0.000000138604746    0.0    0.00      -8.605177637273      -8.605177637273      -8.605177637273      -8.605177498669   0.0000   0.0000   0.0000   0.0000
    109    0.000000121339867    0.0    0.00      -8.605146680815      -8.605146680815      -8.605146680815      -8.605146559475   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   110

   from rhoofr: total integrated electronic density
   in g-space =      8.000000   in r-space =     8.000000
     Total Electronic Pressure (GPa)     -150.30772    110
     Pressure of Nuclei (GPa)             0.00000    110
     Pressure Total (GPa)          -150.30772    110


                total energy =       -8.60512028835 Hartree a.u.
              kinetic energy =       11.71399 Hartree a.u.
        electrostatic energy =      -23.00044 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62259 Hartree a.u.
  n-l pseudopotential energy =        2.13137 Hartree a.u.
 exchange-correlation energy =        4.17254 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72180174        -0.24096794        -0.00049074
       -0.24096794      -149.72174483         0.00025041
       -0.00049074         0.00025041      -151.47960458
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18078292545205E-01    -0.18059698621882E-01    -0.22542549016295E-04
   H       0.13141182256256E-01     0.29074888014557E-02     0.21370695966216E-05
   H       0.29023080545099E-02     0.13118929146719E-01     0.10698117928240E-06



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    110    0.000000104957804    0.0    0.00      -8.605120288349      -8.605120288349      -8.605120288349      -8.605120183391   0.0000   0.0000   0.0000   0.0000
    111    0.000000089013047    0.0    0.00      -8.605098713374      -8.605098713374      -8.605098713374      -8.605098624361   0.0000   0.0000   0.0000   0.0000
    112    0.000000073552979    0.0    0.00      -8.605083635112      -8.605083635112      -8.605083635112      -8.605083561559   0.0000   0.0000   0.0000   0.0000
    113    0.000000059539499    0.0    0.00      -8.605082404448      -8.605082404448      -8.605082404448      -8.605082344908   0.0000   0.0000   0.0000   0.0000
    114    0.000000047298770    0.0    0.00      -8.605083177678      -8.605083177678      -8.605083177678      -8.605083130379   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.472988D-07  0.1D-03  0.773231D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:   115
     Pressure of Nuclei (GPa)             0.00000    115
     Pressure Total (GPa)          -150.30825    115


                total energy =       -8.60509487989 Hartree a.u.
              kinetic energy =       11.71416 Hartree a.u.
        electrostatic energy =      -23.00050 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62260 Hartree a.u.
  n-l pseudopotential energy =        2.13129 Hartree a.u.
 exchange-correlation energy =        4.17255 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   Total stress (GPa)
     -149.72217544        -0.24084414        -0.00001083
       -0.24084414      -149.72252474         0.00021188
       -0.00001083         0.00021188      -151.48006133
   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18066669658795E-01    -0.18034595153269E-01     0.34167528947444E-04
   H       0.13102798794283E-01     0.28972591355487E-02    -0.64659842347350E-06
   H       0.29005751640095E-02     0.13099096724495E-01     0.71133574458269E-05



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
    115    0.000000036614669    0.0    0.00      -8.605094879891      -8.605094879891      -8.605094879891      -8.605094843276   0.0000   0.0000   0.0000   0.0000

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.01s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.62892       0.62892 (AU)
   ekin          :       11.04821      11.04821 (AU)
   epot          :      -20.74579     -20.74579 (AU)
   total energy  :       -7.45355      -7.45355 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :       -7.45355      -7.45355 (AU)
   econs         :       -7.45355      -7.45355 (AU)
   pressure      :     -129.72443    -129.72443 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      1.35s CPU      1.36s WALL (       1 calls)
     main_loop    :     15.23s CPU     15.75s WALL (     115 calls)
     cpr_total    :     15.24s CPU     15.75s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     15.24s CPU     15.75s WALL (     115 calls)
     move_electro :     15.13s CPU     15.63s WALL (     115 calls)

     Called by move_electrons:
     rhoofr       :      5.05s CPU      5.09s WALL (     116 calls)
     vofrho       :      8.31s CPU      8.77s WALL (     116 calls)
     dforce       :      1.87s CPU      1.88s WALL (     232 calls)
     calphi       :      0.01s CPU      0.01s WALL (     116 calls)
     nlfl         :      0.00s CPU      0.00s WALL (     116 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (     116 calls)
     rsg          :      0.01s CPU      0.01s WALL (     116 calls)
     rhoset       :      0.01s CPU      0.01s WALL (     116 calls)
     sigset       :      0.01s CPU      0.01s WALL (     116 calls)
     tauset       :      0.01s CPU      0.01s WALL (     116 calls)
     ortho        :      0.03s CPU      0.03s WALL (     116 calls)
     updatc       :      0.00s CPU      0.00s WALL (     116 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (     116 calls)
     nlfq         :      0.01s CPU      0.01s WALL (     116 calls)
     nlsm1        :      0.00s CPU      0.00s WALL (     117 calls)
     nlsm2        :      0.01s CPU      0.01s WALL (     116 calls)
     fft          :      1.99s CPU      2.09s WALL (    1161 calls)
     ffts         :      0.83s CPU      0.84s WALL (     464 calls)
     fftw         :      2.36s CPU      2.38s WALL (    2784 calls)
     fft_scatt_xy :      1.20s CPU      1.22s WALL (    4409 calls)
     fft_scatt_yz :      1.61s CPU      1.64s WALL (    4409 calls)
     betagx       :      0.56s CPU      0.56s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      1.20s CPU      1.20s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.00s CPU      0.00s WALL (       1 calls)
     from_scratch :      0.14s CPU      0.15s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.00s CPU      0.00s WALL (     117 calls)
     exch_corr    :      5.98s CPU      6.37s WALL (     116 calls)


     CP           :     16.67s CPU     17.21s WALL


   This run was terminated on:   2:11:44   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
