
     Program CP v.6.7MaX starts on  6Jun2021 at  2:10:46 

     This program is part of the open-source Quantum ESPRESSO suite
     for quantum simulation of materials; please cite
         "P. Giannozzi et al., J. Phys.:Condens. Matter 21 395502 (2009);
         "P. Giannozzi et al., J. Phys.:Condens. Matter 29 465901 (2017);
          URL http://www.quantum-espresso.org", 
     in publications or presentations arising from this work. More details at
     http://www.quantum-espresso.org/quote

     Parallel version (MPI), running on     2 processors

     MPI processes distributed on     1 nodes
     R & G space division:  proc/nbgrp/npool/nimage =       2
     Waiting for input...
     Reading input from standard input

   Job Title:  Water Molecule


   Atomic Pseudopotentials Parameters
   ----------------------------------

   Reading pseudopotential for specie #  1 from file :
   ../pseudo/O_HSCV_PBE-1.0.UPF
     Message from routine scan_end:
     No INFO block end statement, possibly corrupted file
   file type is UPF v.1

   Reading pseudopotential for specie #  2 from file :
   ../pseudo/H_HSCV_PBE-1.0.UPF
   file type is UPF v.1

     IMPORTANT: XC functional enforced from input :
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)
     Any further DFT definition will be discarded
     Please, verify this is what you really want



   Main Simulation Parameters (from input)
   ---------------------------------------
   Restart Mode       =      -1   from_scratch   
   Number of MD Steps =     400
   Print out every            1 MD Steps
   Reads from unit    =      50
   Writes to unit     =      50
   MD Simulation time step            =       5.00
   Electronic fictitious mass (emass) =     400.00
   emass cut-off                      =       2.50

   Simulation Cell Parameters (from input)
   external pressure       =            0.00 [KBar]
   wmass (calculated)      =         2493.41 [AU]
   ibrav =    1
   alat  =    12.00000000
   a1    =    12.00000000    0.00000000    0.00000000
   a2    =     0.00000000   12.00000000    0.00000000
   a3    =     0.00000000    0.00000000   12.00000000

   b1    =     0.08333333    0.00000000    0.00000000
   b2    =     0.00000000    0.08333333    0.00000000
   b3    =     0.00000000    0.00000000    0.08333333
   omega =    1728.00000000

   Energy Cut-offs
   ---------------
   Ecutwfc =   40.0 Ry,      Ecutrho =  160.0 Ry,      Ecuts =  160.0 Ry
   Gcutwfc =   12.1     ,    Gcutrho =   24.2          Gcuts =   24.2
   NOTA BENE: refg, mmx =   0.050000  3840
   Eigenvalues calculated without the kinetic term contribution
   Orthog. with lagrange multipliers : eps =   0.10E-08,  max = 300
   verlet algorithm for electron dynamics
   with friction frice =  0.1000 , grease =  1.0000
   Electron dynamics : the temperature is not controlled
   initial random displacement of el. coordinates with  amplitude=  0.020000

   Electronic states
   -----------------
   Local Spin Density calculation
   Number of Electrons=     8
   Spins up   =     4, occupations: 
   1.00 1.00 1.00 1.00
   Spins down =     4, occupations: 
   1.00 1.00 1.00 1.00


   Exchange and correlations functionals
   -------------------------------------
     Exchange-correlation= SCAN
                           (   0   0   0   0   0 263 267)


   Ions Simulation Parameters
   --------------------------
   Ions are not allowed to move
   Ionic position (from input)
   sorted by specie, and converted to real a.u. coordinates
   Species   1 atoms =    1 mass =     29166.22 (a.u.),        16.00 (amu) rcmax =   0.50 (a.u.)
        0.009900     0.009900     0.000000
   Species   2 atoms =    2 mass =      1822.89 (a.u.),         1.00 (amu) rcmax =   0.50 (a.u.)
        1.832500    -0.224300    -0.000100
       -0.224300     1.832500     0.000200
   Ionic position read from input file


   Cell Dynamics Parameters (from STDIN)
   -------------------------------------
   Starting cell generated from CELLDM
   Constant VOLUME Molecular dynamics
   cell parameters are not allowed to move

   Verbosity: iverbosity =  1



   Simulation dimensions initialization
   ------------------------------------

   unit vectors of full simulation cell
   in real space:                         in reciprocal space (units 2pi/alat):
   1    12.0000    0.0000    0.0000              1.0000    0.0000    0.0000
   2     0.0000   12.0000    0.0000              0.0000    1.0000    0.0000
   3     0.0000    0.0000   12.0000              0.0000    0.0000    1.0000

     Parallelization info
     --------------------
     sticks:   dense  smooth     PW     G-vecs:    dense   smooth      PW
     Min         913     913    228                29445    29445    3624
     Max         916     916    229                29448    29448    3625
     Sum        1829    1829    457                58893    58893    7249


   Real Mesh
   ---------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Smooth Real Mesh
   ----------------
   Global Dimensions   Local  Dimensions   Processor Grid
   .X.   .Y.   .Z.     .X.   .Y.   .Z.     .X.   .Y.   .Z.
    50    50    50      50    50    25       1     1     2
   Array leading dimensions ( nr1x, nr2x, nr3x )   =     50    50    50
   Local number of cell to store the grid ( nrxx ) =      62500
   Number of x-y planes for each processors: 
  |  50,  25  |  50,  25  |

   Reciprocal Space Mesh
   ---------------------
   Large Mesh
     Global(ngm_g)    MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Smooth Mesh
     Global(ngms_g)   MinLocal       MaxLocal      Average
          29447          14723          14724       14723.50
   Wave function Mesh
     Global(ngw_g)    MinLocal       MaxLocal      Average
           3625           1812           1813        1812.50


   System geometry initialization
   ------------------------------
   ibrav =    1       cell parameters read from input file

   Matrix Multiplication Performances
   ortho mmul, time for parallel driver      =   0.00000 with    1 procs

   Constraints matrixes will be distributed block like on
   ortho sub-group =    1*   1 procs



   Pseudopotentials initialization
   -------------------------------


   Common initialization

   Specie:     1
   1  indv=  1   ang. mom=  0

                        dion 
   0.2201

   Specie:     2

                        dion 

   Cell parameters from input file are used in electron mass preconditioning
   init_tpiba2=    0.27415568

   Short Legend and Physical Units in the Output
   ---------------------------------------------
   NFI    [int]          - step index
   EKINC  [HARTREE A.U.] - kinetic energy of the fictitious electronic dynamics
   TEMPH  [K]            - Temperature of the fictitious cell dynamics
   TEMP   [K]            - Ionic temperature
   ETOT   [HARTREE A.U.] - Scf total energy (Kohn-Sham hamiltonian)
   ENTHAL [HARTREE A.U.] - Enthalpy ( ETOT + P * V )
   ECONS  [HARTREE A.U.] - Enthalpy + kinetic energy of ions and cell
   ECONT  [HARTREE A.U.] - Constant of motion for the CP lagrangian



   Wave Initialization: random initial wave-functions
   Occupation number from init
   spin =   1 nbnd =     4
    1.00 1.00 1.00 1.00
   spin =   2 nbnd =     4
    1.00 1.00 1.00 1.00

   formf: eself=    30.31961
   formf:     vps(g=0)=  -0.0021938     rhops(g=0)=  -0.0034722
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021834     rhops(g=0)=  -0.0034132
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf:     vps(g=0)=  -0.0021764     rhops(g=0)=  -0.0033552
   formf: sum_g vps(g)=  -2.9366943 sum_g rhops(g)=  -4.3110817
   formf:     vps(g=0)=  -0.0004605     rhops(g=0)=  -0.0005787
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004565     rhops(g=0)=  -0.0005689
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf:     vps(g=0)=  -0.0004530     rhops(g=0)=  -0.0005592
   formf: sum_g vps(g)=  -1.7925287 sum_g rhops(g)=  -0.7185136
   Delta V(G=0):   0.003633Ry,    0.098863eV

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
      1    0.560053903515981    0.0    0.00      11.266732542839      11.266732542839      11.266732542839      11.826786446355   0.0000   0.0000   0.0000   0.0000
      2    1.603795329834228    0.0    0.00       9.782787968088       9.782787968088       9.782787968088      11.386583297922   0.0000   0.0000   0.0000   0.0000
      3    2.934881008655175    0.0    0.00       7.541785868386       7.541785868386       7.541785868386      10.476666877041   0.0000   0.0000   0.0000   0.0000
      4    4.363763604595404    0.0    0.00       4.659291331403       4.659291331403       4.659291331403       9.023054935999   0.0000   0.0000   0.0000   0.0000
      5    5.742671137566530    0.0    0.00       1.252945830714       1.252945830714       1.252945830714       6.995616968280   0.0000   0.0000   0.0000   0.0000
      6    6.777819125801004    0.0    0.00      -2.341966537819      -2.341966537819      -2.341966537819       4.435852587982   0.0000   0.0000   0.0000   0.0000
      7    6.993279224874684    0.0    0.00      -5.384698244901      -5.384698244901      -5.384698244901       1.608580979974   0.0000   0.0000   0.0000   0.0000
      8    6.145396606772117    0.0    0.00      -7.170616365300      -7.170616365300      -7.170616365300      -1.025219758528   0.0000   0.0000   0.0000   0.0000
      9    4.605201299361214    0.0    0.00      -7.714930695997      -7.714930695997      -7.714930695997      -3.109729396636   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    10

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.99 (Slater)  1.36 (Becke)  0.00 (expected)


                total energy =       -7.65761275333 Hartree a.u.
              kinetic energy =        5.55432 Hartree a.u.
        electrostatic energy =      -12.29809 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -1.38444 Hartree a.u.
  n-l pseudopotential energy =        2.40118 Hartree a.u.
 exchange-correlation energy =       -1.93059 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -96.23  -17.35   -7.06    1.67

   Eigenvalues (eV), kp =   1 , spin =  2

  -77.08  -17.55   -2.56   -0.87


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.81954118527423E+00    -0.68782266196484E+00     0.15710864593851E+00
   H       0.11117511108083E+01    -0.21768624414091E+00     0.13598081977776E+00
   H      -0.23238139331103E+00     0.12191179630680E+01     0.59729543730481E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     10    3.087977342290294    0.0    0.00      -7.657612753331      -7.657612753331      -7.657612753331      -4.569635411040   0.0000   0.0000   0.0000   0.0000
     11    2.090706639478175    0.0    0.00      -7.653127904017      -7.653127904017      -7.653127904017      -5.562421264539   0.0000   0.0000   0.0000   0.0000
     12    1.669699752976173    0.0    0.00      -7.980670021399      -7.980670021399      -7.980670021399      -6.310970268423   0.0000   0.0000   0.0000   0.0000
     13    1.604862474834041    0.0    0.00      -8.588113643379      -8.588113643379      -8.588113643379      -6.983251168544   0.0000   0.0000   0.0000   0.0000
     14    1.641100090218020    0.0    0.00      -9.291806706382      -9.291806706382      -9.291806706382      -7.650706616164   0.0000   0.0000   0.0000   0.0000
     15    1.626044416058256    0.0    0.00      -9.937845125725      -9.937845125725      -9.937845125725      -8.311800709667   0.0000   0.0000   0.0000   0.0000
     16    1.532516959637527    0.0    0.00     -10.473514205113     -10.473514205113     -10.473514205113      -8.940997245476   0.0000   0.0000   0.0000   0.0000
     17    1.404324589242584    0.0    0.00     -10.926005772550     -10.926005772550     -10.926005772550      -9.521681183307   0.0000   0.0000   0.0000   0.0000
     18    1.290555635523725    0.0    0.00     -11.345791169707     -11.345791169707     -11.345791169707     -10.055235534183   0.0000   0.0000   0.0000   0.0000
     19    1.210834316572222    0.0    0.00     -11.764690781684     -11.764690781684     -11.764690781684     -10.553856465112   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    20

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 2.23 (Slater)  1.34 (Becke)  0.00 (expected)


                total energy =      -12.18326517457 Hartree a.u.
              kinetic energy =        7.41624 Hartree a.u.
        electrostatic energy =      -17.95194 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -2.59210 Hartree a.u.
  n-l pseudopotential energy =        3.85961 Hartree a.u.
 exchange-correlation energy =       -2.91507 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -60.56  -27.49   -4.62   -0.73

   Eigenvalues (eV), kp =   1 , spin =  2

  -61.26  -28.51   -8.70   -1.13


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.13643104439448E+00     0.11686801648157E+01     0.60356411621463E+00
   H       0.67511960448378E+00    -0.26119952302845E+00     0.23738317903908E+00
   H      -0.23975582980213E+00     0.63638349180390E+00     0.37394986697906E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     20    1.155150903855707    0.0    0.00     -12.183265174569     -12.183265174569     -12.183265174569     -11.028114270714   0.0000   0.0000   0.0000   0.0000
     21    1.102649129376200    0.0    0.00     -12.584033398493     -12.584033398493     -12.584033398493     -11.481384269117   0.0000   0.0000   0.0000   0.0000
     22    1.038814521749112    0.0    0.00     -12.949236821796     -12.949236821796     -12.949236821796     -11.910422300047   0.0000   0.0000   0.0000   0.0000
     23    0.961966534027147    0.0    0.00     -13.271857222560     -13.271857222560     -13.271857222560     -12.309890688533   0.0000   0.0000   0.0000   0.0000
     24    0.879837471912689    0.0    0.00     -13.556425785122     -13.556425785122     -13.556425785122     -12.676588313209   0.0000   0.0000   0.0000   0.0000
     25    0.802429835708459    0.0    0.00     -13.813793109095     -13.813793109095     -13.813793109095     -13.011363273387   0.0000   0.0000   0.0000   0.0000
     26    0.736251393970477    0.0    0.00     -14.053972798700     -14.053972798700     -14.053972798700     -13.317721404729   0.0000   0.0000   0.0000   0.0000
     27    0.682817956108054    0.0    0.00     -14.283662606572     -14.283662606572     -14.283662606572     -13.600844650463   0.0000   0.0000   0.0000   0.0000
     28    0.639349232201006    0.0    0.00     -14.504519866638     -14.504519866638     -14.504519866638     -13.865170634437   0.0000   0.0000   0.0000   0.0000
     29    0.600781102169146    0.0    0.00     -14.714080148335     -14.714080148335     -14.714080148335     -14.113299046166   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    30

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 1.87 (Slater)  1.17 (Becke)  0.00 (expected)


                total energy =      -14.90931149029 Hartree a.u.
              kinetic energy =        9.13819 Hartree a.u.
        electrostatic energy =      -20.83208 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.09571 Hartree a.u.
  n-l pseudopotential energy =        3.31224 Hartree a.u.
 exchange-correlation energy =       -3.43195 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -44.26  -26.48   -6.62   -4.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -44.91  -27.37  -20.89   -1.45


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.39488149395700E+00     0.44809155836428E+00    -0.89627700848380E-01
   H       0.36654008618687E+00    -0.15484447180789E+00    -0.65615904494768E-01
   H      -0.16972499438239E+00     0.23848631833102E+00    -0.37290619455203E-01



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     30    0.563151053556440    0.0    0.00     -14.909311490294     -14.909311490294     -14.909311490294     -14.346160436738   0.0000   0.0000   0.0000   0.0000
     31    0.524712414347733    0.0    0.00     -15.088303879384     -15.088303879384     -15.088303879384     -14.563591465036   0.0000   0.0000   0.0000   0.0000
     32    0.485693642359508    0.0    0.00     -15.251015058735     -15.251015058735     -15.251015058735     -14.765321416376   0.0000   0.0000   0.0000   0.0000
     33    0.447469579986444    0.0    0.00     -15.399010278323     -15.399010278323     -15.399010278323     -14.951540698337   0.0000   0.0000   0.0000   0.0000
     34    0.411307040940135    0.0    0.00     -15.534186750299     -15.534186750299     -15.534186750299     -15.122879709359   0.0000   0.0000   0.0000   0.0000
     35    0.377863703515656    0.0    0.00     -15.658218925428     -15.658218925428     -15.658218925428     -15.280355221913   0.0000   0.0000   0.0000   0.0000
     36    0.347310573352062    0.0    0.00     -15.772408219881     -15.772408219881     -15.772408219881     -15.425097646529   0.0000   0.0000   0.0000   0.0000
     37    0.319410961079526    0.0    0.00     -15.877652333982     -15.877652333982     -15.877652333982     -15.558241372903   0.0000   0.0000   0.0000   0.0000
     38    0.293539990557567    0.0    0.00     -15.974281259544     -15.974281259544     -15.974281259544     -15.680741268986   0.0000   0.0000   0.0000   0.0000
     39    0.268892119723277    0.0    0.00     -16.062036351151     -16.062036351151     -16.062036351151     -15.793144231427   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    40

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 1.43 (Slater)  1.02 (Becke)  0.00 (expected)


                total energy =      -16.14084699602 Hartree a.u.
              kinetic energy =       11.25620 Hartree a.u.
        electrostatic energy =      -22.48083 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.55091 Hartree a.u.
  n-l pseudopotential energy =        2.50380 Hartree a.u.
 exchange-correlation energy =       -3.86910 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -34.89  -20.57  -14.99   -6.16

   Eigenvalues (eV), kp =   1 , spin =  2

  -34.02  -21.26  -16.89   -2.53


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.29462799033610E+00    -0.22545224442212E+00     0.48091678816521E-01
   H       0.14456168521082E+00    -0.19227190239878E-01    -0.26926780391308E-02
   H      -0.66908270769293E-01     0.84458527900630E-01     0.59486264736530E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     40    0.245040253040761    0.0    0.00     -16.140846996019     -16.140846996019     -16.140846996019     -15.895806742978   0.0000   0.0000   0.0000   0.0000
     41    0.222051975485995    0.0    0.00     -16.211129054885     -16.211129054885     -16.211129054885     -15.989077079399   0.0000   0.0000   0.0000   0.0000
     42    0.200260636414080    0.0    0.00     -16.273614257657     -16.273614257657     -16.273614257657     -16.073353621243   0.0000   0.0000   0.0000   0.0000
     43    0.180087843586012    0.0    0.00     -16.329310009896     -16.329310009896     -16.329310009896     -16.149222166310   0.0000   0.0000   0.0000   0.0000
     44    0.161886174969035    0.0    0.00     -16.379293303287     -16.379293303287     -16.379293303287     -16.217407128317   0.0000   0.0000   0.0000   0.0000
     45    0.145913976177232    0.0    0.00     -16.424687471114     -16.424687471114     -16.424687471114     -16.278773494936   0.0000   0.0000   0.0000   0.0000
     46    0.132207187255587    0.0    0.00     -16.466437662295     -16.466437662295     -16.466437662295     -16.334230475039   0.0000   0.0000   0.0000   0.0000
     47    0.120624163346128    0.0    0.00     -16.505288841328     -16.505288841328     -16.505288841328     -16.384664677981   0.0000   0.0000   0.0000   0.0000
     48    0.110879665949161    0.0    0.00     -16.541754953469     -16.541754953469     -16.541754953469     -16.430875287520   0.0000   0.0000   0.0000   0.0000
     49    0.102571706320871    0.0    0.00     -16.576082032017     -16.576082032017     -16.576082032017     -16.473510325696   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    50

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.98 (Slater)  0.79 (Becke)  0.00 (expected)


                total energy =      -16.60831857373 Hartree a.u.
              kinetic energy =       11.69044 Hartree a.u.
        electrostatic energy =      -22.80921 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.60948 Hartree a.u.
  n-l pseudopotential energy =        2.12652 Hartree a.u.
 exchange-correlation energy =       -4.00659 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -31.97  -17.38  -13.46  -10.07

   Eigenvalues (eV), kp =   1 , spin =  2

  -30.66  -17.57  -14.04   -3.81


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20492342871807E+00    -0.19342136816852E+00    -0.13444448460916E-01
   H       0.53134116068591E-01    -0.29978488623810E-01    -0.18269618725932E-01
   H      -0.23571132772106E-01     0.31582883894401E-02    -0.22950201022148E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     50    0.095267359276914    0.0    0.00     -16.608318573729     -16.608318573729     -16.608318573729     -16.513051214452   0.0000   0.0000   0.0000   0.0000
     51    0.088590939597793    0.0    0.00     -16.638398134575     -16.638398134575     -16.638398134575     -16.549807194978   0.0000   0.0000   0.0000   0.0000
     52    0.082262795904464    0.0    0.00     -16.666233774792     -16.666233774792     -16.666233774792     -16.583970978887   0.0000   0.0000   0.0000   0.0000
     53    0.076131563512025    0.0    0.00     -16.691760424139     -16.691760424139     -16.691760424139     -16.615628860627   0.0000   0.0000   0.0000   0.0000
     54    0.070158308518287    0.0    0.00     -16.715033133619     -16.715033133619     -16.715033133619     -16.644874825101   0.0000   0.0000   0.0000   0.0000
     55    0.064314790997662    0.0    0.00     -16.736068875196     -16.736068875196     -16.736068875196     -16.671754084198   0.0000   0.0000   0.0000   0.0000
     56    0.058596725449413    0.0    0.00     -16.754910829151     -16.754910829151     -16.754910829151     -16.696314103701   0.0000   0.0000   0.0000   0.0000
     57    0.053048202199488    0.0    0.00     -16.771663969769     -16.771663969769     -16.771663969769     -16.718615767569   0.0000   0.0000   0.0000   0.0000
     58    0.047736005465074    0.0    0.00     -16.786474289298     -16.786474289298     -16.786474289298     -16.738738283833   0.0000   0.0000   0.0000   0.0000
     59    0.042739527811985    0.0    0.00     -16.799535076471     -16.799535076471     -16.799535076471     -16.756795548659   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    60

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.61 (Slater)  0.59 (Becke)  0.00 (expected)


                total energy =      -16.81105812113 Hartree a.u.
              kinetic energy =       11.65800 Hartree a.u.
        electrostatic energy =      -22.92191 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62644 Hartree a.u.
  n-l pseudopotential energy =        2.16535 Hartree a.u.
 exchange-correlation energy =       -4.08605 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -30.22  -15.20  -12.16  -10.88

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.99  -15.25  -11.80   -4.67


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.10000032259022E+00    -0.14683753301810E-01     0.40804493788973E-01
   H       0.43793788380205E-01    -0.10850958102943E-01    -0.26647017480125E-01
   H      -0.60886783018224E-02    -0.10527078543462E-02    -0.73802067058610E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     60    0.038129034741563    0.0    0.00     -16.811058121127     -16.811058121127     -16.811058121127     -16.772929086386   0.0000   0.0000   0.0000   0.0000
     61    0.033952418242914    0.0    0.00     -16.821254830171     -16.821254830171     -16.821254830171     -16.787302411928   0.0000   0.0000   0.0000   0.0000
     62    0.030241595283046    0.0    0.00     -16.830338646833     -16.830338646833     -16.830338646833     -16.800097051550   0.0000   0.0000   0.0000   0.0000
     63    0.027015152271912    0.0    0.00     -16.838521392538     -16.838521392538     -16.838521392538     -16.811506240267   0.0000   0.0000   0.0000   0.0000
     64    0.024265298902182    0.0    0.00     -16.845990327151     -16.845990327151     -16.845990327151     -16.821725028249   0.0000   0.0000   0.0000   0.0000
     65    0.021954727548503    0.0    0.00     -16.852891090510     -16.852891090510     -16.852891090510     -16.830936362962   0.0000   0.0000   0.0000   0.0000
     66    0.020032473338589    0.0    0.00     -16.859339202147     -16.859339202147     -16.859339202147     -16.839306728808   0.0000   0.0000   0.0000   0.0000
     67    0.018443293404242    0.0    0.00     -16.865423851797     -16.865423851797     -16.865423851797     -16.846980558393   0.0000   0.0000   0.0000   0.0000
     68    0.017128559547848    0.0    0.00     -16.871208357869     -16.871208357869     -16.871208357869     -16.854079798321   0.0000   0.0000   0.0000   0.0000
     69    0.016028476572849    0.0    0.00     -16.876730053941     -16.876730053941     -16.876730053941     -16.860701577368   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    70

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.34 (Slater)  0.41 (Becke)  0.00 (expected)


                total energy =      -16.88200766934 Hartree a.u.
              kinetic energy =       11.61890 Hartree a.u.
        electrostatic energy =      -22.94788 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.61931 Hartree a.u.
  n-l pseudopotential energy =        2.18112 Hartree a.u.
 exchange-correlation energy =       -4.11483 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -29.14  -14.32  -11.02   -9.55

   Eigenvalues (eV), kp =   1 , spin =  2

  -28.27  -14.30  -10.71   -5.61


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.34131807655983E-01    -0.34628770030805E-02    -0.10897833692511E-01
   H       0.30528963274770E-01    -0.35386923988083E-02    -0.20295887701369E-01
   H      -0.92005055241232E-03     0.17909922308979E-01    -0.99019732188350E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     70    0.015088608917131    0.0    0.00     -16.882007669336     -16.882007669336     -16.882007669336     -16.866919060419   0.0000   0.0000   0.0000   0.0000
     71    0.014268691433445    0.0    0.00     -16.887055061081     -16.887055061081     -16.887055061081     -16.872786369648   0.0000   0.0000   0.0000   0.0000
     72    0.013539978992206    0.0    0.00     -16.891886175648     -16.891886175648     -16.891886175648     -16.878346196655   0.0000   0.0000   0.0000   0.0000
     73    0.012874093670060    0.0    0.00     -16.896503029144     -16.896503029144     -16.896503029144     -16.883628935474   0.0000   0.0000   0.0000   0.0000
     74    0.012245468155744    0.0    0.00     -16.900898091972     -16.900898091972     -16.900898091972     -16.888652623817   0.0000   0.0000   0.0000   0.0000
     75    0.011640567601605    0.0    0.00     -16.905069429640     -16.905069429640     -16.905069429640     -16.893428862038   0.0000   0.0000   0.0000   0.0000
     76    0.011054265527161    0.0    0.00     -16.909021527330     -16.909021527330     -16.909021527330     -16.897967261803   0.0000   0.0000   0.0000   0.0000
     77    0.010481900563127    0.0    0.00     -16.912755839039     -16.912755839039     -16.912755839039     -16.902273938476   0.0000   0.0000   0.0000   0.0000
     78    0.009919136621469    0.0    0.00     -16.916272489134     -16.916272489134     -16.916272489134     -16.906353352513   0.0000   0.0000   0.0000   0.0000
     79    0.009365169464589    0.0    0.00     -16.919574094436     -16.919574094436     -16.919574094436     -16.910208924972   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    80

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.15 (Slater)  0.24 (Becke)  0.00 (expected)


                total energy =      -16.92266647889 Hartree a.u.
              kinetic energy =       11.67537 Hartree a.u.
        electrostatic energy =      -22.98207 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62044 Hartree a.u.
  n-l pseudopotential energy =        2.15216 Hartree a.u.
 exchange-correlation energy =       -4.14768 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -28.09  -13.66  -10.01   -8.26

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.63  -13.57   -9.91   -6.24


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.29744049864128E-01    -0.22245786532260E-01    -0.12889634719848E-01
   H       0.15510312452214E-01    -0.12120896648282E-03    -0.11532067547600E-01
   H       0.27152189311605E-02     0.12488316644924E-01    -0.68722693693699E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     80    0.008821381269999    0.0    0.00     -16.922666478887     -16.922666478887     -16.922666478887     -16.913845097617   0.0000   0.0000   0.0000   0.0000
     81    0.008287516487174    0.0    0.00     -16.925553452145     -16.925553452145     -16.925553452145     -16.917265935657   0.0000   0.0000   0.0000   0.0000
     82    0.007762020497199    0.0    0.00     -16.928236990170     -16.928236990170     -16.928236990170     -16.920474969673   0.0000   0.0000   0.0000   0.0000
     83    0.007243574426893    0.0    0.00     -16.930718807504     -16.930718807504     -16.930718807504     -16.923475233077   0.0000   0.0000   0.0000   0.0000
     84    0.006731458784321    0.0    0.00     -16.933000773373     -16.933000773373     -16.933000773373     -16.926269314589   0.0000   0.0000   0.0000   0.0000
     85    0.006225990714172    0.0    0.00     -16.935085591096     -16.935085591096     -16.935085591096     -16.928859600382   0.0000   0.0000   0.0000   0.0000
     86    0.005729415811521    0.0    0.00     -16.936978464782     -16.936978464782     -16.936978464782     -16.931249048971   0.0000   0.0000   0.0000   0.0000
     87    0.005246020348047    0.0    0.00     -16.938688097491     -16.938688097491     -16.938688097491     -16.933442077143   0.0000   0.0000   0.0000   0.0000
     88    0.004781187744827    0.0    0.00     -16.940226367933     -16.940226367933     -16.940226367933     -16.935445180188   0.0000   0.0000   0.0000   0.0000
     89    0.004339929324630    0.0    0.00     -16.941606921830     -16.941606921830     -16.941606921830     -16.937266992506   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:    90

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.05 (Slater)  0.13 (Becke)  0.00 (expected)


                total energy =      -16.94284336905 Hartree a.u.
              kinetic energy =       11.70590 Hartree a.u.
        electrostatic energy =      -22.99705 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62301 Hartree a.u.
  n-l pseudopotential energy =        2.13687 Hartree a.u.
 exchange-correlation energy =       -4.16555 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.55  -13.27   -9.56   -7.53

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.34  -13.21   -9.56   -6.63


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.24819576135987E-01    -0.19029855513039E-01    -0.36892244786911E-02
   H       0.13394931631424E-01     0.22104222446040E-02    -0.53263052716947E-02
   H       0.30016465619907E-02     0.12044966939436E-01    -0.30867109186465E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
     90    0.003925593819242    0.0    0.00     -16.942843369046     -16.942843369046     -16.942843369046     -16.938917775227   0.0000   0.0000   0.0000   0.0000
     91    0.003539544229381    0.0    0.00     -16.943948155846     -16.943948155846     -16.943948155846     -16.940408611617   0.0000   0.0000   0.0000   0.0000
     92    0.003181717821135    0.0    0.00     -16.944932511798     -16.944932511798     -16.944932511798     -16.941750793977   0.0000   0.0000   0.0000   0.0000
     93    0.002851278130116    0.0    0.00     -16.945806690891     -16.945806690891     -16.945806690891     -16.942955412761   0.0000   0.0000   0.0000   0.0000
     94    0.002547043603458    0.0    0.00     -16.946580220023     -16.946580220023     -16.946580220023     -16.944033176419   0.0000   0.0000   0.0000   0.0000
     95    0.002267724499061    0.0    0.00     -16.947262017787     -16.947262017787     -16.947262017787     -16.944994293288   0.0000   0.0000   0.0000   0.0000
     96    0.002012032372282    0.0    0.00     -16.947860493946     -16.947860493946     -16.947860493946     -16.945848461574   0.0000   0.0000   0.0000   0.0000
     97    0.001778699133022    0.0    0.00     -16.948383593985     -16.948383593985     -16.948383593985     -16.946604894852   0.0000   0.0000   0.0000   0.0000
     98    0.001566499249566    0.0    0.00     -16.948838797523     -16.948838797523     -16.948838797523     -16.947272298274   0.0000   0.0000   0.0000   0.0000
     99    0.001374279915415    0.0    0.00     -16.949233191350     -16.949233191350     -16.949233191350     -16.947858911434   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   100

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.01 (Slater)  0.06 (Becke)  0.00 (expected)


                total energy =      -16.94957349059 Hartree a.u.
              kinetic energy =       11.71064 Hartree a.u.
        electrostatic energy =      -22.99920 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62261 Hartree a.u.
  n-l pseudopotential energy =        2.13322 Hartree a.u.
 exchange-correlation energy =       -4.17161 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.35  -13.11   -9.42   -7.21

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.09   -9.43   -6.85


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.20482648443305E-01    -0.17693955466835E-01    -0.43073320998086E-02
   H       0.13563996549070E-01     0.27063967802801E-02    -0.24744624017218E-02
   H       0.27651816917841E-02     0.13114305128074E-01    -0.14946642940039E-02



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    100    0.001200970039647    0.0    0.00     -16.949573490585     -16.949573490585     -16.949573490585     -16.948372520546   0.0000   0.0000   0.0000   0.0000
    101    0.001045543023790    0.0    0.00     -16.949866017684     -16.949866017684     -16.949866017684     -16.948820474660   0.0000   0.0000   0.0000   0.0000
    102    0.000906953018050    0.0    0.00     -16.950116675030     -16.950116675030     -16.950116675030     -16.949209722012   0.0000   0.0000   0.0000   0.0000
    103    0.000784109140075    0.0    0.00     -16.950330880376     -16.950330880376     -16.950330880376     -16.949546771236   0.0000   0.0000   0.0000   0.0000
    104    0.000675860101768    0.0    0.00     -16.950513583930     -16.950513583930     -16.950513583930     -16.949837723828   0.0000   0.0000   0.0000   0.0000
    105    0.000580997152609    0.0    0.00     -16.950669166008     -16.950669166008     -16.950669166008     -16.950088168855   0.0000   0.0000   0.0000   0.0000
    106    0.000498275790734    0.0    0.00     -16.950801472906     -16.950801472906     -16.950801472906     -16.950303197115   0.0000   0.0000   0.0000   0.0000
    107    0.000426448570195    0.0    0.00     -16.950913862959     -16.950913862959     -16.950913862959     -16.950487414389   0.0000   0.0000   0.0000   0.0000
    108    0.000364308333851    0.0    0.00     -16.951009244428     -16.951009244428     -16.951009244428     -16.950644936094   0.0000   0.0000   0.0000   0.0000
    109    0.000310722057703    0.0    0.00     -16.951090114156     -16.951090114156     -16.951090114156     -16.950779392098   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   110

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.03 (Becke)  0.00 (expected)


                total energy =      -16.95115863542 Hartree a.u.
              kinetic energy =       11.71215 Hartree a.u.
        electrostatic energy =      -22.99952 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62216 Hartree a.u.
  n-l pseudopotential energy =        2.13173 Hartree a.u.
 exchange-correlation energy =       -4.17337 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.29  -13.06   -9.38   -7.07

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.25  -13.06   -9.38   -6.95


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18470847703306E-01    -0.17478166709242E-01    -0.40269885248075E-02
   H       0.13346977240458E-01     0.29081966810745E-02    -0.10223203695117E-02
   H       0.28726282469292E-02     0.13180564824924E-01    -0.82131226037690E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    110    0.000264643945013    0.0    0.00     -16.951158635421     -16.951158635421     -16.951158635421     -16.950893991476   0.0000   0.0000   0.0000   0.0000
    111    0.000225121018733    0.0    0.00     -16.951216653405     -16.951216653405     -16.951216653405     -16.950991532386   0.0000   0.0000   0.0000   0.0000
    112    0.000191298571337    0.0    0.00     -16.951265765473     -16.951265765473     -16.951265765473     -16.951074466902   0.0000   0.0000   0.0000   0.0000
    113    0.000162414598351    0.0    0.00     -16.951307328184     -16.951307328184     -16.951307328184     -16.951144913585   0.0000   0.0000   0.0000   0.0000
    114    0.000137793361794    0.0    0.00     -16.951342493540     -16.951342493540     -16.951342493540     -16.951204700178   0.0000   0.0000   0.0000   0.0000
    115    0.000116838974946    0.0    0.00     -16.951372252917     -16.951372252917     -16.951372252917     -16.951255413942   0.0000   0.0000   0.0000   0.0000
    116    0.000099028075587    0.0    0.00     -16.951397433567     -16.951397433567     -16.951397433567     -16.951298405491   0.0000   0.0000   0.0000   0.0000
    117    0.000083904652520    0.0    0.00     -16.951418744277     -16.951418744277     -16.951418744277     -16.951334839625   0.0000   0.0000   0.0000   0.0000
    118    0.000071074379557    0.0    0.00     -16.951436780979     -16.951436780979     -16.951436780979     -16.951365706600   0.0000   0.0000   0.0000   0.0000
    119    0.000060197092174    0.0    0.00     -16.951452046547     -16.951452046547     -16.951452046547     -16.951391849455   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   120

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.02 (Becke)  0.00 (expected)


                total energy =      -16.95146497515 Hartree a.u.
              kinetic energy =       11.71381 Hartree a.u.
        electrostatic energy =      -23.00038 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62255 Hartree a.u.
  n-l pseudopotential energy =        2.13149 Hartree a.u.
 exchange-correlation energy =       -4.17384 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.27  -13.05   -9.37   -7.03

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.25  -13.05   -9.37   -6.99


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18041881376943E-01    -0.17943503133643E-01    -0.13588356526367E-02
   H       0.13168001087449E-01     0.29277355026330E-02    -0.28390854323884E-03
   H       0.29118516312639E-02     0.13127696893047E-01    -0.31331784933739E-03



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    120    0.000050981127014    0.0    0.00     -16.951464975153     -16.951464975153     -16.951464975153     -16.951413994026   0.0000   0.0000   0.0000   0.0000
    121    0.000043177449152    0.0    0.00     -16.951475926602     -16.951475926602     -16.951475926602     -16.951432749153   0.0000   0.0000   0.0000   0.0000
    122    0.000036573298863    0.0    0.00     -16.951485208589     -16.951485208589     -16.951485208589     -16.951448635290   0.0000   0.0000   0.0000   0.0000
    123    0.000030987052825    0.0    0.00     -16.951493078949     -16.951493078949     -16.951493078949     -16.951462091896   0.0000   0.0000   0.0000   0.0000
    124    0.000026263641215    0.0    0.00     -16.951499761560     -16.951499761560     -16.951499761560     -16.951473497919   0.0000   0.0000   0.0000   0.0000
    125    0.000022270355545    0.0    0.00     -16.951505439696     -16.951505439696     -16.951505439696     -16.951483169341   0.0000   0.0000   0.0000   0.0000
    126    0.000018894400117    0.0    0.00     -16.951510265848     -16.951510265848     -16.951510265848     -16.951491371448   0.0000   0.0000   0.0000   0.0000
    127    0.000016039847598    0.0    0.00     -16.951514374147     -16.951514374147     -16.951514374147     -16.951498334300   0.0000   0.0000   0.0000   0.0000
    128    0.000013625090917    0.0    0.00     -16.951517871105     -16.951517871105     -16.951517871105     -16.951504246014   0.0000   0.0000   0.0000   0.0000
    129    0.000011581237728    0.0    0.00     -16.951520849630     -16.951520849630     -16.951520849630     -16.951509268392   0.0000   0.0000   0.0000   0.0000

 * Physical Quantities at step:   130

   from rhoofr: total integrated electronic density
   spin up
   in g-space =      4.000000   in r-space =     4.000000
   spin down
   in g-space =      4.000000   in r-space =     4.000000

 Spin contamination: s(s+1)= 0.00 (Slater)  0.01 (Becke)  0.00 (expected)


                total energy =      -16.95152339023 Hartree a.u.
              kinetic energy =       11.71402 Hartree a.u.
        electrostatic energy =      -23.00045 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62257 Hartree a.u.
  n-l pseudopotential energy =        2.13134 Hartree a.u.
 exchange-correlation energy =       -4.17386 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.00


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18049146055519E-01    -0.18042698051816E-01     0.35015077475135E-04
   H       0.13115588010086E-01     0.29081740599957E-02    -0.26616831846924E-04
   H       0.29100496369109E-02     0.13115748985938E-01    -0.61346370656721E-04



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00

  nfi     ekinc              temph  tempp     etot                 enthal               econs                econt              vnhh    xnhh0   vnhp    xnhp0
    130    0.000009850167104    0.0    0.00     -16.951523390226     -16.951523390226     -16.951523390226     -16.951513540059   0.0000   0.0000   0.0000   0.0000
    131    0.000008382923651    0.0    0.00     -16.951525558044     -16.951525558044     -16.951525558044     -16.951517175120   0.0000   0.0000   0.0000   0.0000
    132    0.000007138338605    0.0    0.00     -16.951527407312     -16.951527407312     -16.951527407312     -16.951520268973   0.0000   0.0000   0.0000   0.0000
    133    0.000006081858652    0.0    0.00     -16.951528986977     -16.951528986977     -16.951528986977     -16.951522905118   0.0000   0.0000   0.0000   0.0000
    134    0.000005184386205    0.0    0.00     -16.951530335508     -16.951530335508     -16.951530335508     -16.951525151122   0.0000   0.0000   0.0000   0.0000
    135    0.000004421442855    0.0    0.00     -16.951531487883     -16.951531487883     -16.951531487883     -16.951527066440   0.0000   0.0000   0.0000   0.0000
    136    0.000003772403748    0.0    0.00     -16.951532472880     -16.951532472880     -16.951532472880     -16.951528700476   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.377240D-05  0.1D-03  0.984997D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

 * Physical Quantities at step:   137

 Spin contamination: s(s+1)= 0.00 (Slater)  0.00 (Becke)  0.00 (expected)


                total energy =      -16.95153331379 Hartree a.u.
              kinetic energy =       11.71397 Hartree a.u.
        electrostatic energy =      -23.00042 Hartree a.u.
                         esr =        0.00155 Hartree a.u.
                       eself =       30.31961 Hartree a.u.
      pseudopotential energy =       -3.62256 Hartree a.u.
  n-l pseudopotential energy =        2.13133 Hartree a.u.
 exchange-correlation energy =       -4.17386 Hartree a.u.
           average potential =        0.00000 Hartree a.u.



   Eigenvalues (eV), kp =   1 , spin =  1

  -27.26  -13.05   -9.37   -7.01

   Eigenvalues (eV), kp =   1 , spin =  2

  -27.26  -13.05   -9.37   -7.01


   CELL_PARAMETERS
   12.00000000    0.00000000    0.00000000
    0.00000000   12.00000000    0.00000000
    0.00000000    0.00000000   12.00000000

   System Density [g/cm^3] :              0.1167302083


   System Volume [A.U.^3] :           1728.0000000000


   Center of mass square displacement (a.u.):   0.000000

   ATOMIC_POSITIONS
   O       0.99000000000000E-02     0.99000000000000E-02     0.00000000000000E+00
   H       0.18325000000000E+01    -0.22430000000000E+00    -0.10000000000000E-03
   H      -0.22430000000000E+00     0.18325000000000E+01     0.20000000000000E-03

   ATOMIC_VELOCITIES
   O       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00
   H       0.00000000000000E+00     0.00000000000000E+00     0.00000000000000E+00

   Forces acting on atoms (au):
   O      -0.18042993475164E-01    -0.18042816651589E-01     0.82765264876611E-04
   H       0.13107744136478E-01     0.29071896675262E-02     0.12492125870738E-04
   H       0.29104559913795E-02     0.13110590456589E-01    -0.52347379087166E-05



   Partial temperatures (for each ionic specie) 
   Species  Temp (K)   Mean Square Displacement (a.u.)
        1   0.00E+00     0.0000E+00
        2   0.00E+00     0.0000E+00
    137    0.000003219890515    0.0    0.00     -16.951533313787     -16.951533313787     -16.951533313787     -16.951530093896   0.0000   0.0000   0.0000   0.0000

   MAIN:          EKINC   (thr)          DETOT   (thr)       MAXFORCE   (thr)
   MAIN:   0.321989D-05  0.1D-03  0.840907D-06  0.1D-05  0.000000D+00  0.1D+11
   MAIN: convergence achieved for system relaxation

   writing restart file (with schema): ./h2o_50.save/
     restart      :      0.01s CPU      0.01s WALL (       1 calls)


   Averaged Physical Quantities
                      accumulated      this run
   ekinc         :        0.52768       0.52768 (AU)
   ekin          :       10.68391      10.68391 (AU)
   epot          :      -27.80659     -27.80659 (AU)
   total energy  :      -14.79287     -14.79287 (AU)
   temperature   :        0.00000       0.00000 (K )
   enthalpy      :      -14.79287     -14.79287 (AU)
   econs         :      -14.79287     -14.79287 (AU)
   pressure      :        0.00000       0.00000 (Gpa)
   volume        :     1728.00000    1728.00000 (AU)



     Called by MAIN_LOOP:
     initialize   :      0.69s CPU      0.71s WALL (       1 calls)
     main_loop    :     20.69s CPU     22.30s WALL (     137 calls)
     cpr_total    :     20.70s CPU     22.31s WALL (       1 calls)

     Called by INIT_RUN:

     Called by CPR:
     cpr_md       :     20.70s CPU     22.31s WALL (     137 calls)
     move_electro :     20.55s CPU     22.14s WALL (     137 calls)

     Called by move_electrons:
     rhoofr       :      4.46s CPU      4.53s WALL (     138 calls)
     vofrho       :     11.56s CPU     13.07s WALL (     138 calls)
     dforce       :      4.58s CPU      4.61s WALL (     552 calls)
     calphi       :      0.02s CPU      0.02s WALL (     138 calls)
     nlfl         :      0.00s CPU      0.00s WALL (     138 calls)

     Called by ortho:
     ortho_iter   :      0.00s CPU      0.00s WALL (     276 calls)
     rsg          :      0.01s CPU      0.01s WALL (     276 calls)
     rhoset       :      0.02s CPU      0.02s WALL (     276 calls)
     sigset       :      0.01s CPU      0.01s WALL (     276 calls)
     tauset       :      0.01s CPU      0.01s WALL (     276 calls)
     ortho        :      0.06s CPU      0.06s WALL (     138 calls)
     updatc       :      0.01s CPU      0.01s WALL (     138 calls)

     Small boxes:

     Low-level routines:
     prefor       :      0.00s CPU      0.00s WALL (     138 calls)
     nlfq         :      0.02s CPU      0.02s WALL (     138 calls)
     nlsm1        :      0.01s CPU      0.01s WALL (     139 calls)
     nlsm2        :      0.02s CPU      0.02s WALL (     138 calls)
     fft          :      3.54s CPU      3.67s WALL (    2071 calls)
     ffts         :      0.97s CPU      0.97s WALL (     552 calls)
     fftw         :      5.55s CPU      5.61s WALL (    6624 calls)
     fft_scatt_xy :      2.40s CPU      2.45s WALL (    9247 calls)
     fft_scatt_yz :      2.88s CPU      2.92s WALL (    9247 calls)
     fft_scatt_tg :      0.03s CPU      0.03s WALL (    1656 calls)
     betagx       :      0.22s CPU      0.22s WALL (       1 calls)
     qradx        :      0.00s CPU      0.00s WALL (       1 calls)
     gram         :      0.00s CPU      0.00s WALL (       1 calls)
     nlinit       :      0.53s CPU      0.53s WALL (       1 calls)
     init_dim     :      0.01s CPU      0.01s WALL (       1 calls)
     newnlinit    :      0.00s CPU      0.00s WALL (       1 calls)
     from_scratch :      0.15s CPU      0.17s WALL (       1 calls)
     strucf       :      0.00s CPU      0.00s WALL (       1 calls)
     calbec       :      0.01s CPU      0.01s WALL (     139 calls)


     CP           :     21.47s CPU     23.10s WALL


   This run was terminated on:   2:11: 9   6Jun2021            

=------------------------------------------------------------------------------=
   JOB DONE.
=------------------------------------------------------------------------------=
